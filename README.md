# BOA v2

## HOW-TO get BOA source code

```bash
# Use SSH if you don't want to fill in your Inria credentials
git clone --recurse-submodules git@gitlab.inria.fr:marionjy/boa.git

# OR use HTTPS
git clone --recurse-submodules https://gitlab.inria.fr/marionjy/boa.git
```

## HOW-TO get latest changes from GitLab

```bash
cd boa
git pull --recurse-submodules
```

## HOW-TO compile BOA

### Method 1: Docker image (recommended)

```bash
cd boa-v2

# Build boa image from Dockerfile
docker build -t boa .

# You can open a bash session in your BOA container with the command bellow.
# You will find boa and binsec binaries in the bin folder
docker run -ti --entrypoint bash boa

# If you need to access data from your host machine you can use a Docker bind mount:
docker run -ti --entrypoint bash -v /path/to/folder/to/share:/app/share boa
# You will find your folder in `/app/share` path in the container

# Also, you can directly run boa without opening a container shell with:
docker run -v /path/to/folder/to/share:/app/share boa -b /app/share/hostname.exe
```

You can continue with "HOW-TO run BOA" section.


### Method 2: Local machine

#### Install compilation and runtime dependencies (macOS case)

```bash
# If you do not have it yet, install Brew (THE macOS package manager)
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# Install needed tools to compile BOA and BinSec
brew install cmake autoconf make pkg-config wget gmp zmq graphviz llvm@11 opam protobuf menhir

# Install OCaml stuff to compile BinSec
opam init # answer yes and yes
eval $(opam env)
opam switch install 4.04.2-binsec 4.04.2
eval $(opam env)
opam install ocamlfind menhir ocamlgraph piqi zarith zmq llvm yojson ounit qcheck seq

# Install Radare2 framework if you don't have it
cd /tmp && \
wget https://github.com/radareorg/radare2/archive/7ed581d2dee432c685420524f1b1eaa723db273d.zip -O r2.zip && \
unzip r2.zip && \
cd radare2-7ed581d2dee432c685420524f1b1eaa723db273d && \
sys/install.sh --install

# Install Capstone disassembler if you don't have it
cd /tmp && \
wget https://github.com/aquynh/capstone/archive/4.0.1.zip -O capstone_4.0.1.zip && \
unzip capstone_4.0.1.zip && \
rm capstone_4.0.1.zip && \
cd capstone-4.0.1 && \
CAPSTONE_ARCHS="x86" CAPSTONE_X86_ATT_DISABLE=yes ./make.sh && \
sudo ./make.sh install

# Install at least one SMT solver (Only Yices is recommended)

## Yices solver
cd /tmp && \
wget https://github.com/SRI-CSL/yices2/archive/afb9bfd5834b3ead4c9d00b5cad86cec643e73c6.zip -O yices2.zip && \
unzip yices2.zip && \
cd yices2-afb9bfd5834b3ead4c9d00b5cad86cec643e73c6 && \
autoconf && \
./configure && \
make && \
sudo make install

## Z3 solver
brew install z3

## CVC4 solver
brew tap cvc4/cvc4 && \
brew install cvc4/cvc4/cvc4

## Boolector solver
cd /tmp && \
git clone https://github.com/boolector/boolector && \
cd boolector && \
./contrib/setup-lingeling.sh && \
./contrib/setup-btor2tools.sh && \
./configure.sh && \
cd build && \
make && \
sudo make install
```

#### Install compilation and runtime dependencies (Linux case)

```bash
# Install needed tools to compile BOA and BinSec
apt install git build-essential autoconf make curl wget opam protobuf-compiler menhir graphviz libgmp-dev libzmq3-dev llvm-10-dev gperf graphviz cmake perl pkg-config

# Install OCaml stuff to compile BinSec
opam init # answer yes
eval `opam config env` # (or eval $(opam env))
opam switch install 4.04.2-binsec 4.04.2 # (or opam switch install 4.04.2-binsec --alias-of 4.04.2)
eval $(opam env)
opam install depext
opam depext conf-zmq conf-llvm conf-gmp
opam install ocamlfind menhir ocamlgraph piqi zarith zmq llvm yojson ounit qcheck seq


# Install Radare2 framework if you don't have it
cd /tmp && \
wget https://github.com/radareorg/radare2/archive/cb7d6b4390a6a38d3d9525337d6749248b206dd3.zip -O r2.zip && \
unzip r2.zip && \
cd radare2-cb7d6b4390a6a38d3d9525337d6749248b206dd3 && \
sys/install.sh --install

# Install Capstone disassembler if you don't have it
cd /tmp && \
wget https://github.com/aquynh/capstone/archive/4.0.1.zip -O capstone_4.0.1.zip && \
unzip capstone_4.0.1.zip && \
rm capstone_4.0.1.zip && \
cd capstone-4.0.1 && \
CAPSTONE_ARCHS="x86" CAPSTONE_X86_ATT_DISABLE=yes ./make.sh && \
sudo ./make.sh install

# Install at least one SMT solver (Only Yices is recommended)

## Yices solver
cd /tmp && \
wget https://github.com/SRI-CSL/yices2/archive/afb9bfd5834b3ead4c9d00b5cad86cec643e73c6.zip -O yices2.zip && \
unzip yices2.zip && \
cd yices2-afb9bfd5834b3ead4c9d00b5cad86cec643e73c6 && \
autoconf && \
./configure && \
make && \
sudo make install

## Z3 solver
apt install z3

## CVC4 solver
apt install cvc4

## Boolector solver
cd /tmp && \
git clone https://github.com/boolector/boolector && \
cd boolector && \
./contrib/setup-lingeling.sh && \
./contrib/setup-btor2tools.sh && \
./configure.sh && \
cd build && \
make && \
make install
```


#### Compile BOA

```bash
cd boa
mkdir build && cd build

# Option 1: Use make generator
cmake ..
make binsec # Will compile BinSec executable in 'thirdparty/binsec' directory
make boa_v2 # Will compile BOA and move executable in 'bin' directory

# Option 2: Use Xcode generator (macOS)
cmake .. -G Xcode # Will generate Xcode project file
open BOA_v2.xcodeproj # Will open BOA project in Xcode
# Notice the 'targets' selector at the top left of the window
# If you need BinSec (needed for BOA disassembly mode), select 'binsec' target and hit CMB-B to build BinSec
# To compile BOA, select 'boa_v2' target and hit CMD-B to build BOA
```

## HOW-TO run BOA

### The simple way

By using the `./script/run_boa.sh` script your binary and your config file will be automatically given to BOA. Also, the script will save the stdout of BOA in a log file.

For example, if you want to disassemble the binary `my_binary`, you need to create a folder named `my_binary` and move the binary in this folder with the config file.
Your folder need to be like that:

```
.
`-- my_binary
    |-- my_binary
    `-- config.ini
```

Then you can disassemble your binary with this command:

```bash
./script/run_boa.sh yes ./examples/my_binary

# Arg1: Print or not print BOA stdout in the shell ('yes' or 'no')
# Arg2: Path to the folder that contains the binary and the config file
# Args3+ will be directly given to BOA, so without modifying the config file you can add or modify any BOA parameter

# When BOA has finished you will find
# the CFG and the log file in the 'my_binary' folder.
```

### The real way


```bash
# Add bin folder to the PATH (so that boa finds BinSec binary)
export PATH=./bin:$PATH

# List all available options with
./bin/boa_v2 -h

# Disassemble `my_binary` with
./bin/boa_v2 -b ./examples/my_binary/my_binary -c ./examples/my_binary/config.ini
```

Note that `./script/run_boa.sh` it's just a call to `./bin/boa_v2` with some magic that:
* Pre-filled values for `-b` and `-c`
* Record stdout of `./bin/boa_v2` in a log file


## Config file

You can configure BOA with the multitude of arguments given by `./bin/boa_v2 -h`. Or you can create a config file `config.ini` and put BOA arguments in this file, then you only need to give this file to BOA with `./bin/boa_v2 -b my_binary -c config.ini`.

You can find below a config file sample:


```ini
; Core
binary = my_binary
disas-mode = boa
start-addr = 0x560

; BOA disassembling mode
loop-lap = 50
solver = yices

; Logging
loglevel-global = 2
loglevel-core = 2
loglevel-bin-parser = 2
loglevel-disassembler = 2
loglevel-rec-disas = 2
loglevel-boa-disas = 1
loglevel-x86-to-smtlib = 2
loglevel-solver = 2
```

You can print a detailed description and default value of each setting with `./bin/boa_v2 -h`.


## Hello World: Your first BOA experience

Of course, you can find multiple examples in the `examples` folder (PE, ELF and handmade examples with NASM).

In this section we will disassemble our first handmade assembly program with BOA.

### Create your assembly program

Create a new file `helloworld.asm` and add your program instructions:


```assembly
bits 32
global main



        section   .text

main:

A       mov eax, 0x90000000
        call C

        add eax, ecx
        jmp eax

C       mov ecx, 0x01234567
        ret
```

Of course, this program is bullshit, we only want to be sure that BOA is able to know :

1. What is the target of the `ret` instruction
2. What is the target of the `jmp eax` instruction

### Assemble your program with NASM

Use the `nasm -f bin helloworld.asm` command to generate the raw binary file (`helloworld`) of your program with NASM.
If you open this file with and hex viewer you can see each machine opcode x86 instruction of your program.
Feel free to use something like https://onlinedisassembler.com/odaweb/ to paste your all your hex code and see what's happen.

### Generate CFG of your program with BOA

Use the `./bin/boa_v2 -b ./helloworld --addrs-not-to-disas 0x91234567` command to build the CFG of your program.

The `addrs-not-to-disas` is only here to tell BOA that the `0x91234567` address does not contain any valid x86 instruction because our program is bullshit.

Now you can find in the root folder of your program `helloworld` new files generated by BOA. Also, feel free to increase BOA verbosity with the `--loglevel-XXXXXX` option to understand how BOA works.
