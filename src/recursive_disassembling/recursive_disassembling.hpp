#ifndef RECURSIVE_DISASSEMBLING_H
#define RECURSIVE_DISASSEMBLING_H

#include <cstdint>

#include "common/basic_block.hpp"
#include "common/cfg_manager.hpp"
#include "common/edge.hpp"
#include "common/instr.hpp"
#include "disassembler/disassembler.hpp"
#include "utils/utils.hpp"

namespace boa
{

extern const char* rec_disas_logger;

struct config_rec_disassembling_t
{
  bool disassemble_call_targets;
};

struct pair_hash
{
  inline std::size_t operator()(const std::pair<addr_t, std::optional<const Instr*>>& v) const
  {
    if(v.second.has_value())
    {
      return v.first * 31 + reinterpret_cast<std::uintptr_t>(*v.second);
    }
    return v.first * 31;
  }
};

class RecursiveDisassembling
{
public:
  // Constructors and destructors
  RecursiveDisassembling(Disassembler* disassembler, Binary& binary,
                         const config_rec_disassembling_t& config_rec_disassembling,
                         const config_global_t& config_global);
  ~RecursiveDisassembling();

  // Other functions
  void disassembleBinary(CFGManager& cfg_manager) const;

private:
  Disassembler* m_disassembler;
  Binary& m_binary;
  std::shared_ptr<spdlog::logger> m_log;
  const config_rec_disassembling_t& m_config_rec_disassembling;
  const config_global_t& m_config_global;
};

} // namespace boa

#endif
