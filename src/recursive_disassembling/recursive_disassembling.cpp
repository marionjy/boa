#include "recursive_disassembling.hpp"

#include <deque>
#include <set>
#include <sstream>
#include <stack>

namespace boa
{

// Constructors and destructors
RecursiveDisassembling::RecursiveDisassembling(Disassembler* disassembler, Binary& binary,
                                               const config_rec_disassembling_t& config_rec_disassembling,
                                               const config_global_t& config_global)
    : m_disassembler(disassembler), m_binary(binary), m_config_rec_disassembling(config_rec_disassembling),
      m_config_global(config_global)
{
  m_log = spdlog::get(utils::rec_disas_logger);
  SPDLOG_LOGGER_TRACE(m_log, "RecursiveDisassembling::RecursiveDisassembling()");
}

RecursiveDisassembling::~RecursiveDisassembling()
{
  SPDLOG_LOGGER_TRACE(m_log, "RecursiveDisassembling::~RecursiveDisassembling()");
}

// Other functions
void RecursiveDisassembling::disassembleBinary(CFGManager& cfg_manager) const
{
  SPDLOG_LOGGER_TRACE(m_log, "RecursiveDisassembling::disassembleBinary()");
  m_log->info("🚀 Disassemble binary with static recursive algorithm");

  // In static mode, only one wave, so create the unique CFG
  CFG* cfg = cfg_manager.createEmptyCfg(m_config_global.start_addrs);

  // Keep an history of each disassembled BB

  // First: next addr to disas
  // Second: corresponding last_instr (in order to create edge)
  std::unordered_set<std::pair<addr_t, std::optional<const Instr*>>, pair_hash> already_disassembled_addrs;
  std::stack<std::pair<addr_t, std::optional<const Instr*>>> addrs_to_disas{};

  // Init
  for(const auto& start_addr : m_config_global.start_addrs)
  {
    addrs_to_disas.push({start_addr, std::nullopt});
  }

  unsigned int cnt = 0;
  auto start_time_500_bbs = std::chrono::steady_clock::now();

  while(!addrs_to_disas.empty())
  {

    // Prevent infinite loop
    if(already_disassembled_addrs.find(addrs_to_disas.top()) != already_disassembled_addrs.end())
    {
      addrs_to_disas.pop();
      continue;
    }
    already_disassembled_addrs.insert(addrs_to_disas.top());

    addr_t addr_to_disas = addrs_to_disas.top().first;
    std::optional<const Instr*> last_instr_addr = addrs_to_disas.top().second;
    addrs_to_disas.pop();

    m_log->info("🔶 #{} (instrs: {}) (todo: {}) Disassemble BB at {:#x}", cnt, cfg->getDisassembledInstrs().size(),
                addrs_to_disas.size(), addr_to_disas);

    if(cnt % 500 == 0 && cnt != 0)
    {
      auto time = utils::milli2hr_min_sec_milli(
          std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start_time_500_bbs)
              .count());
      start_time_500_bbs = std::chrono::steady_clock::now();

      m_log->warn("🕑 #{} (instrs: {}) (todo: {}) Elapsed time for the last 500 BBs (HH:mm:ss,ms): "
                  "{:02d}:{:02d}:{:02d},{:03d}",
                  cnt, cfg->getDisassembledInstrs().size(), addrs_to_disas.size(), time.hr, time.min, time.sec,
                  time.milli);
    }

    cnt++;

    const BasicBlock* bb = nullptr;
    m_disassembler->disassembleBasicBlock(bb, addr_to_disas, 0, last_instr_addr, EdgeFoundMethod::STATIC_DISAS, cfg);

    if(bb != nullptr)
    {
      const Instr* last_instr = bb->getLastInstr();
      const addr_t& last_instr_next_addr = last_instr->getNextAddr();
      const std::optional<addr_t>& last_instr_jmp_addr = last_instr->getJmpAddr();
      const InstrType& last_instr_type = last_instr->getType();
      const InstrSubType& last_instr_sub_type = last_instr->getSubType();

      // Disas returnsite of CALL (Static and Dyn) instructions --> yes
      if(last_instr_type == InstrType::CALL)
      {
        addrs_to_disas.push({last_instr_next_addr, std::nullopt});
        SPDLOG_LOGGER_DEBUG(m_log, "\tDisassemble returnsite address, next addr to disas: call returnsite ({:#x})",
                            last_instr->getNextAddr());
      }

      // SEQ
      if(last_instr_type == InstrType::SEQ)
      {
        SPDLOG_LOGGER_DEBUG(m_log, "Last instr: SEQ, next addr to disas: next addr ({:#x})", last_instr_next_addr);
        addrs_to_disas.push({last_instr_next_addr, last_instr});
      }

      // JCC
      else if(last_instr_type == InstrType::JCC)
      {
        addrs_to_disas.push({last_instr_next_addr, last_instr});
        addrs_to_disas.push({*last_instr_jmp_addr, last_instr});

        SPDLOG_LOGGER_DEBUG(m_log, "Last instr: JCC, next addrs to disas: next addr ({:#x}) and jump target ({:#x})",
                            last_instr_next_addr, *last_instr_jmp_addr);
      }

      // DYN CALL
      else if(last_instr_type == InstrType::CALL && last_instr_sub_type == InstrSubType::DYN_CALL)
      {

        if(!m_config_global.no_disassemble_call_targets)
        {
          bool found_next_addr_to_disas = false;

          // If this DYN_CALL is a call dword [0xXXXXXXXX] call we can maybe find the target
          if(last_instr->getPtrAddr().has_value())
          {
            addr_t ptr_addr = *last_instr->getPtrAddr();

            // Check if this CALL call an external lib function
            auto import_entry = m_binary.getImportAtAddr(ptr_addr);
            if(import_entry != nullptr)
            {
              found_next_addr_to_disas = true;
              addrs_to_disas.push({import_entry->function_addr, last_instr}); // last_instr --> func_lib_addr

              SPDLOG_LOGGER_DEBUG(m_log,
                                  "Last instr: DYN_CALL to library function, next addr to disas: jump target ({:#x})",
                                  import_entry->function_addr);
            }

            // Else, we check if the mem case value at ptr_address is known
            else
            {
              auto target_addr = m_binary.getWord(ptr_addr);
              if(target_addr.has_value())
              {
                found_next_addr_to_disas = true;
                addrs_to_disas.push({*target_addr, last_instr_addr});
                SPDLOG_LOGGER_DEBUG(m_log,
                                    "Last instr: call dword [0xXXXXXXXX], next addr to disas: target_addr ({:#x})",
                                    *target_addr);
              }
            }
          }
        }
      }

      // STATIC CALL
      else if(last_instr_type == InstrType::CALL && last_instr_sub_type == InstrSubType::STATIC_CALL)
      {
        if(!m_config_global.no_disassemble_call_targets)
        {
          addrs_to_disas.push({*last_instr_jmp_addr, last_instr});
          SPDLOG_LOGGER_DEBUG(m_log, "Last instr: STATIC_CALL, next addr to disas: jump target ({:#x})",
                              *last_instr_jmp_addr);
        }
      }

      // DYN JMP
      else if(last_instr_type == InstrType::JMP && last_instr_sub_type == InstrSubType::DYN_JMP)
      {
        bool found_next_addr_to_disas = false;

        // If this DYN_JMP is a jmp dword [0xXXXXXXXX] jump we can maybe find the target
        if(last_instr->getPtrAddr().has_value())
        {
          addr_t ptr_addr = *last_instr->getPtrAddr();

          // Check if this JMP call an external lib function
          auto import_entry = m_binary.getImportAtAddr(ptr_addr);
          if(import_entry != nullptr)
          {
            found_next_addr_to_disas = true;
            addrs_to_disas.push({import_entry->function_addr, last_instr}); // last_instr --> func_lib_addr
            SPDLOG_LOGGER_DEBUG(m_log,
                                "Last instr: DYN_JMP to library function, next addr to disas: jump target ({:#x})",
                                import_entry->function_addr);
          }

          // decryptmsg case (jmp dword[0x80940b6])
          else
          {
            auto target_addr = m_binary.getWord(ptr_addr);
            if(target_addr.has_value())
            {
              found_next_addr_to_disas = true;
              addrs_to_disas.push({*target_addr, last_instr});
              SPDLOG_LOGGER_DEBUG(m_log, "Last instr: jmp dword [0xXXXXXXXX], next addr to disas: jump target ({:#x})",
                                  *target_addr);
            }
          }
        }

        if(!found_next_addr_to_disas)
        {
          SPDLOG_LOGGER_DEBUG(m_log, "Last instr: DYN_JMP, next addr to disas: ???");
        }
      }

      // STATIC JMP
      else if(last_instr_type == InstrType::JMP && last_instr_sub_type == InstrSubType::STATIC_JMP)
      {
        addrs_to_disas.push({*last_instr_jmp_addr, last_instr});
        SPDLOG_LOGGER_DEBUG(m_log, "Last instr: STATIC_JMP, next addr to disas: jump target ({:#x})",
                            *last_instr_jmp_addr);
      }

      // RET
      else if(last_instr_type == InstrType::RET)
      {
        SPDLOG_LOGGER_DEBUG(m_log, "Last instr: RET, next addr to disas: ???");
      }

      // TRAP
      else if(last_instr_type == InstrType::TRAP)
      {
        static const std::vector<uint8_t> int_80 = {0xcd, 0x80};
        if(last_instr->getBytes() == int_80)
        {
          //TODO: Peut etre pas à faire ici car dans le cas d'un exit (EAX = 0x1) il ne faut pas désassembler la suite
          SPDLOG_LOGGER_DEBUG(m_log, "Last instr: int 0x80, next addr to disas: ({:#x})", last_instr_next_addr);
          addrs_to_disas.push({last_instr_next_addr, last_instr});
        }
        else
        {
          SPDLOG_LOGGER_DEBUG(m_log, "Last instr: TRAP, next addr to disas: none");
        }
        
      }

      // FAKE_LIB_FUNC
      else if(last_instr_type == InstrType::FAKE_LIB_FUNC)
      {
        SPDLOG_LOGGER_DEBUG(m_log,
                            "Last instr: FAKE_LIB_FUNC, next addr to disas should normally already be in the stack");
      }

      // Other to implement
      else
      {
        throw std::runtime_error("Don't know what is the next address to disassemble with the instruction " +
                                 last_instr->getOpcode());
      }
    }
  }
}

} // namespace boa
