#ifndef BOA_TYPES_H
#define BOA_TYPES_H

#include <cstdint>
#include <deque>
#include <map>
#include <set>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace boa
{

// MARK:- Permissions
enum class Permission
{
  READ,
  WRITE,
  EXECUTE
};
std::string perms2s(const std::set<Permission>& perms);

// MARK:- Memory address
using addr_t = std::uint64_t;
using waddr_t = std::pair<unsigned int, addr_t>;

std::string addr2s(addr_t addr);
std::string waddr2s(waddr_t waddr);
std::string mem_case2s(addr_t addr);
std::string addrs2s(const std::unordered_set<addr_t>& values, const std::string& sep = "; ");
std::string addrs2s(const std::set<addr_t>& values, const std::string& sep = "; ");
std::string addrs2s(const std::deque<addr_t>& values, const std::string& sep = "; ");
std::string addrs2s(const std::vector<addr_t>& values, const std::string& sep = "; ");
std::string waddrs2s(const std::set<waddr_t>& values, const std::string& sep = "; ");
std::string waddrs2s(const std::deque<waddr_t>& values, const std::string& sep = "; ");

// MARK:- Byte
using byte_t = std::uint8_t;
std::string byte2s(byte_t v, bool with_0x_prefix = true);
std::string bytes2s(const std::unordered_set<byte_t>& values);
std::string bytes2s(const std::set<byte_t>& values);

// MARK:- Registers
enum class Register
{
  EAX,
  EBX,
  ECX,
  EDX,
  EDI,
  ESI,
  ESP,
  EBP,
  OF,
  SF,
  ZF,
  PF,
  CF,
  AF,
  DF,
  TF,
  FS,
  DR0,
  DR1,
  DR2,
  DR3,
  DR6,
  DR7
};
const std::string& reg2s(Register name);
addr_t reg2size(Register name);
const std::pair<std::string, std::string>& reg2smt(Register name);
bool isAValidReg(const std::string& s);
Register s2reg(const std::string& s);
const std::unordered_map<Register, std::string>& getAllCpuRegs();

// MARK:- Exceptions
enum class Exception
{
  ACCESS_VIOLATION,
  INTEGER_DIVIDE_BY_ZERO,
  BREAKPOINT,
  SINGLE_STEP,
  ILLEGAL_INSTRUCTION
};
uint32_t exc2code(Exception name);
std::string exc2s(Exception name);

} // namespace boa

#endif
