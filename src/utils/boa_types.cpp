#include "boa_types.hpp"

#include <algorithm>
#include <iomanip>
#include <sstream>
#include <string>

#include "utils/smtlib_utils.hpp"

namespace boa
{

// MARK:- Permissions
std::string perms2s(const std::set<Permission>& perms)
{
  static const std::map<std::set<Permission>, std::string> perms2s_map = {
      {{}, "----"},
      {{Permission::READ}, "-r--"},
      {{Permission::READ, Permission::WRITE}, "-rw-"},
      {{Permission::READ, Permission::EXECUTE}, "-r-x"},
      {{Permission::READ, Permission::WRITE, Permission::EXECUTE}, "-rwx"}};

  if(perms2s_map.find(perms) != perms2s_map.end())
  {
    return perms2s_map.at(perms);
  }
  return "????";
}

// MARK:- Memory addresses
std::string addr2s(addr_t addr)
{
  std::stringstream s;
  s << "0x" << std::hex << addr;
  return s.str();
}

std::string waddr2s(waddr_t waddr)
{
  std::stringstream s;
  s << waddr.first << "_0x" << std::hex << waddr.second;
  return s.str();
}

std::string mem_case2s(addr_t addr)
{
  return "@[" + addr2s(addr) + "]";
}

std::string addrs2s(const std::unordered_set<addr_t>& values, const std::string& sep)
{
  if(values.empty())
    return "";
  std::string s = "";
  for(auto v : values)
    s = s + addr2s(v) + sep;
  return s.substr(0, s.length() - sep.length());
}

std::string addrs2s(const std::set<addr_t>& values, const std::string& sep)
{
  if(values.empty())
    return "";
  std::string s = "";
  for(auto v : values)
    s = s + addr2s(v) + sep;
  return s.substr(0, s.length() - sep.length());
}

std::string addrs2s(const std::deque<addr_t>& values, const std::string& sep)
{
  if(values.empty())
    return "";
  std::string s = "";
  for(auto v : values)
    s = s + addr2s(v) + sep;
  return s.substr(0, s.length() - sep.length());
}

std::string addrs2s(const std::vector<addr_t>& values, const std::string& sep)
{
  if(values.empty())
    return "";
  std::string s = "";
  for(auto v : values)
    s = s + addr2s(v) + sep;
  return s.substr(0, s.length() - sep.length());
}

std::string waddrs2s(const std::set<waddr_t>& values, const std::string& sep)
{
  if(values.empty())
    return "";
  std::string s = "";
  for(auto v : values)
    s = s + waddr2s(v) + sep;
  return s.substr(0, s.length() - sep.length());
}

std::string waddrs2s(const std::deque<waddr_t>& values, const std::string& sep)
{
  if(values.empty())
    return "";
  std::string s = "";
  for(auto const& v : values)
    s = s + waddr2s(v) + sep;
  return s.substr(0, s.length() - sep.length());
}

// MARK:- Byte
std::string byte2s(byte_t v, bool with_0x_prefix)
{
  std::stringstream stream;
  if(with_0x_prefix)
  {
    stream << "0x";
  }
  stream << std::setfill('0') << std::setw(2) << std::hex << static_cast<int>(v);
  return stream.str();
}

std::string bytes2s(const std::unordered_set<byte_t>& values)
{
  if(values.empty())
    return "";
  std::string s = "";
  for(auto const& v : values)
    s = s + byte2s(v, true) + "; ";
  return s.substr(0, s.length() - 2);
}

std::string bytes2s(const std::set<byte_t>& values)
{
  if(values.empty())
    return "";
  std::string s = "";
  for(auto const& v : values)
    s = s + byte2s(v, true) + "; ";
  return s.substr(0, s.length() - 2);
}

// MARK:- Registers
std::unordered_map<std::string, Register> s2reg_map = {
    {"eax", Register::EAX},    {"ebx", Register::EBX}, {"ecx", Register::ECX}, {"edx", Register::EDX},
    {"edi", Register::EDI},    {"esi", Register::ESI}, {"esp", Register::ESP}, {"ebp", Register::EBP},
    {"OF", Register::OF},      {"SF", Register::SF},   {"ZF", Register::ZF},   {"PF", Register::PF},
    {"CF", Register::CF},      {"AF", Register::AF},   {"DF", Register::DF},   {"TF", Register::TF},
    {"fs_base", Register::FS}, {"dr0", Register::DR0}, {"dr1", Register::DR1}, {"dr2", Register::DR2},
    {"dr3", Register::DR3},    {"dr6", Register::DR6}, {"dr7", Register::DR7}};

std::unordered_map<Register, std::string> reg2s_map = {
    {Register::EAX, "eax"},    {Register::EBX, "ebx"}, {Register::ECX, "ecx"}, {Register::EDX, "edx"},
    {Register::EDI, "edi"},    {Register::ESI, "esi"}, {Register::ESP, "esp"}, {Register::EBP, "ebp"},
    {Register::OF, "OF"},      {Register::SF, "SF"},   {Register::ZF, "ZF"},   {Register::PF, "PF"},
    {Register::CF, "CF"},      {Register::AF, "AF"},   {Register::DF, "DF"},   {Register::TF, "TF"},
    {Register::FS, "fs_base"}, {Register::DR0, "dr0"}, {Register::DR1, "dr1"}, {Register::DR2, "dr2"},
    {Register::DR3, "dr3"},    {Register::DR6, "dr6"}, {Register::DR7, "dr7"}};

std::unordered_map<Register, addr_t> reg2size_map = {
    {Register::EAX, 32},    {Register::EBX, 32}, {Register::ECX, 32}, {Register::EDX, 32},
    {Register::EDI, 32},    {Register::ESI, 32}, {Register::ESP, 32}, {Register::EBP, 32},
    {Register::OF, 1},      {Register::SF, 1},   {Register::ZF, 1},   {Register::PF, 1},
    {Register::CF, 1},      {Register::AF, 1},   {Register::DF, 1},   {Register::TF, 1},
    {Register::FS, 32}, {Register::DR0, 32}, {Register::DR1, 32}, {Register::DR2, 32},
    {Register::DR3, 32},    {Register::DR6, 32}, {Register::DR7, 32}};

std::unordered_map<Register, std::pair<std::string, std::string>> reg2smt_map = {
  {Register::EAX, {"eax", smtlib_utils::smt_sort_bv(32)}},
  {Register::EBX, {"ebx", smtlib_utils::smt_sort_bv(32)}},
  {Register::ECX, {"ecx", smtlib_utils::smt_sort_bv(32)}},
  {Register::EDX, {"edx", smtlib_utils::smt_sort_bv(32)}},
  {Register::EDI, {"edi", smtlib_utils::smt_sort_bv(32)}},
  {Register::ESI, {"esi", smtlib_utils::smt_sort_bv(32)}},
  {Register::ESP, {"esp", smtlib_utils::smt_sort_bv(32)}},
  {Register::EBP, {"ebp", smtlib_utils::smt_sort_bv(32)}},
  {Register::OF, {"OF", smtlib_utils::smt_sort_bv(1)}},
  {Register::SF, {"SF", smtlib_utils::smt_sort_bv(1)}},
  {Register::ZF, {"ZF", smtlib_utils::smt_sort_bv(1)}},
  {Register::PF, {"PF", smtlib_utils::smt_sort_bv(1)}},
  {Register::CF, {"CF", smtlib_utils::smt_sort_bv(1)}},
  {Register::AF, {"AF", smtlib_utils::smt_sort_bv(1)}},
  {Register::DF, {"DF", smtlib_utils::smt_sort_bv(1)}},
  {Register::TF, {"TF", smtlib_utils::smt_sort_bv(1)}},
  {Register::FS, {"fs_base", smtlib_utils::smt_sort_bv(32)}},
  {Register::DR0, {"dr0", smtlib_utils::smt_sort_bv(32)}},
  {Register::DR1, {"dr1", smtlib_utils::smt_sort_bv(32)}},
  {Register::DR2, {"dr2", smtlib_utils::smt_sort_bv(32)}},
  {Register::DR3, {"dr3", smtlib_utils::smt_sort_bv(32)}},
  {Register::DR6, {"dr6", smtlib_utils::smt_sort_bv(32)}},
  {Register::DR7, {"dr7", smtlib_utils::smt_sort_bv(32)}}};

const std::string& reg2s(Register name)
{
  auto r = reg2s_map.find(name);
  if(r == reg2s_map.end())
  {
    throw std::runtime_error("Unknown register given to 'reg2s'");
  }
  return r->second;
}

addr_t reg2size(Register name)
{
  auto r = reg2size_map.find(name);
  if(r == reg2size_map.end())
  {
    throw std::runtime_error("Unknown register given to 'reg2size_map'");
  }
  return r->second;
}

const std::pair<std::string, std::string>& reg2smt(Register name)
{
  auto r = reg2smt_map.find(name);
  if(r == reg2smt_map.end())
  {
    throw std::runtime_error("Unknown register given to 'reg2smt_map'");
  }
  return r->second;
}

bool isAValidReg(const std::string& s)
{
  if(s2reg_map.find(s) == s2reg_map.end())
  {
    return false;
  }
  return true;
}

Register s2reg(const std::string& s)
{
  if(!isAValidReg(s))
  {
    throw std::runtime_error("Unknown register: " + s);
  }
  return s2reg_map.at(s);
}

const std::unordered_map<Register, std::string>& getAllCpuRegs()
{
  return reg2s_map;
}

// MARK:- Exceptions
uint32_t exc2code(Exception name)
{
  switch(name)
  {
  case Exception::ACCESS_VIOLATION:
    return 0xc0000005U;
  case Exception::INTEGER_DIVIDE_BY_ZERO:
    return 0xc0000094U;
  case Exception::BREAKPOINT:
    return 0x80000003U;
  case Exception::SINGLE_STEP:
    return 0x80000004U;
  case Exception::ILLEGAL_INSTRUCTION:
    return 0xc000001dU;
  }
  return 0x00000000U;
}

std::string exc2s(Exception name)
{
  switch(name)
  {
  case Exception::ACCESS_VIOLATION:
    return "ACCESS_VIOLATION";
  case Exception::INTEGER_DIVIDE_BY_ZERO:
    return "INTEGER_DIVIDE_BY_ZERO";
  case Exception::BREAKPOINT:
    return "BREAKPOINT";
  case Exception::SINGLE_STEP:
    return "SINGLE_STEP";
  case Exception::ILLEGAL_INSTRUCTION:
    return "ILLEGAL_INSTRUCTION";
  }
  return "unknown exception";
}

} // namespace boa
