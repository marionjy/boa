#ifndef UTILS_H
#define UTILS_H

#define SPDLOG_SHORT_LEVEL_NAMES                                                                                       \
  {                                                                                                                    \
    "🔦", "🔧", "ℹ️", "⚠️", "🚫", "C", "O"                                                                        \
  }
#define SPDLOG_ACTIVE_LEVEL SPDLOG_LEVEL_TRACE
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/spdlog.h"

#include <cstring>
#include <deque>
#include <memory>
#include <mutex>
#include <set>
#include <string>
#include <thread>
#include <unordered_set>
#include <vector>

namespace boa
{

using addr_t = uint64_t;
using UPtrString = std::unique_ptr<std::string>;
using SetString = std::set<std::string>;
using UPtrSetString = std::unique_ptr<SetString>;

using UnorderedSetString = std::unordered_set<std::string>;
using UPtrUnorderedSetString = std::unique_ptr<UnorderedSetString>;

enum class DisassemblyMode
{
  STATIC,
  BOA
};

enum class DisassemblerEngine
{
  CAPSTONE
};

struct config_global_t
{
  // From CLI or config file
  std::string bin_filepath;
  std::vector<addr_t> start_addrs;
  std::unordered_set<addr_t> addrs_not_to_disas;
  DisassemblyMode disas_mode;
  DisassemblerEngine disassembler;
  bool print_csv_result;
  bool no_disassemble_call_targets;

  // Computed later
  std::string bin_folderpath;
  std::string bin_filename;

  // Debug
  bool test_mode;
  bool all_bbs_to_smtlib;
  bool dump_bbs_to_smtlib;

  // Analysis
  bool opaque_predicate_analysis;
  unsigned int op_analysis_nb_thread;
};

struct my_time_format_t
{
  long long hr;
  long long min;
  long long sec;
  long long milli;
};

namespace utils
{
extern const char* core_logger;
extern const char* bin_parser_logger;
extern const char* disassembler_logger;
extern const char* rec_disas_logger;
extern const char* boa_disas_logger;
extern const char* x86_to_smtlib_logger;
extern const char* solver_logger;
extern const char* tainting_logger;

void pushMessage(const std::string& m);
const std::vector<std::string>& getMessagesStack();

std::string DisassemblyMode_to_string(DisassemblyMode disas_mode);
std::string uint32_to_string(const uint32_t& v);
std::string uint322s(const std::vector<uint32_t>& values, const std::string& sep = "; ");
std::string get_folder_path(const std::string& s);
std::string get_filename(const std::string& s);
void create_text_file(const std::string& content, const std::string& filepath);
std::pair<int, std::string> exec_shell_command(std::string cmd);
void dot_to_pdf(const std::string& dot_filepath, const std::string& pdf_filepath);
void find_and_replace(std::string* p_source, std::string const& find, std::string const& replace);
FILE* string_to_file_pointer(char* s);
std::vector<std::string> string_splitter(std::string const& s, std::string const& delim);
my_time_format_t milli2hr_min_sec_milli(long long milli);
std::tuple<uint8_t, uint8_t, uint8_t, uint8_t> dword2bytes(uint32_t v);
std::tuple<uint8_t, uint8_t> word2bytes(uint16_t v);

} // namespace utils

} // namespace boa

#endif
