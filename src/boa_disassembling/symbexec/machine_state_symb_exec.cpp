#include "machine_state_symb_exec.hpp"

#include <filesystem>
#include <nlohmann/json.hpp>
#include <set>
#include <sstream>

namespace boa
{

// Map of all external lib func
std::map<std::string, std::pair<CallConv, unsigned int>> MachineStateSymbExec::s_external_lib_func{};

int MachineStateSymbExec::s_write_at_unknown_location_cnt = 0;

void MachineStateSymbExec::ParseWindowsDllsJson(const std::string& windows_dlls_json_filepath)
{
  // Need to parse the JSON file
  if(!std::filesystem::exists(windows_dlls_json_filepath))
  {
    throw std::runtime_error("Can not find " + windows_dlls_json_filepath + " file");
  }
  std::ifstream windows_dlls_json_stream(windows_dlls_json_filepath);
  nlohmann::json json;
  windows_dlls_json_stream >> json;
  for(nlohmann::json::iterator it = json.begin(); it != json.end(); ++it)
  {
    std::string key = it.key();
    std::string value = it.value();
    if(s_external_lib_func.find(key) != s_external_lib_func.end())
    {
      throw std::out_of_range("Key " + key + " is duplicate in the json file");
    }
    std::vector<std::string> splitted_value = utils::string_splitter(value, "@");
    CallConv conv;
    if(splitted_value.at(0) == "stdcall")
    {
      conv = CallConv::STDCALL;
    }
    else if(splitted_value.at(0) == "cdecl")
    {
      conv = CallConv::CDECL;
    }
    else if(splitted_value.at(0) == "varargs")
    {
      conv = CallConv::VARARGS;
    }
    else if(splitted_value.at(0) == "thiscall")
    {
      conv = CallConv::THISCALL;
    }
    else
    {
      throw std::out_of_range("Unkown call conv  " + splitted_value.at(0));
    }

    int number_arg = stoi(splitted_value.at(1));

    s_external_lib_func.insert({key, {conv, number_arg}});
  }
}

void MachineStateSymbExec::ExternalLibFuncHook(MachineState* machine_state, Binary* binary, const Instr* instr,
                                               const std::string& windows_dlls_json_filepath,
                                               std::shared_ptr<spdlog::logger> log, bool disable_lib_fun_hooks, bool load_dlls)
{

  auto esp_value = machine_state->getRegValue(Register::ESP);
  if(!esp_value.has_value())
  {
    // If don't know the ESP value it's not worth it to try to simulate something...
    return;
  }

  const auto& func_lib_name = instr->getOpcode();

  // First, retrieve calling convention and args numbers
  CallConv call_conv;
  unsigned int number_of_args;

  if(binary->getFormat() == BinaryFormat::PE)
  {
    if(s_external_lib_func.empty())
    {
      SPDLOG_LOGGER_DEBUG(log, "Parse JSON DLL file: {}", windows_dlls_json_filepath);
      ParseWindowsDllsJson(windows_dlls_json_filepath);
    }
    if(s_external_lib_func.find(func_lib_name) == s_external_lib_func.end())
    {
      throw std::out_of_range("Need to add entry for lib function " + func_lib_name + " in the json file");
    }
    std::pair<CallConv, unsigned int> call_conv_arg = s_external_lib_func.at(func_lib_name);
    call_conv = call_conv_arg.first;
    number_of_args = call_conv_arg.second;

    // If CDECL or VARARGS --> caller clean stack (donc on ne pop pas les arguements, ça sera fait par le prochain
    // BB)
    if(call_conv_arg.first == CallConv::THISCALL)
    {
      // On dit que thiscall en Microsoft Visual c'est comme le stdcall (callee clean stack)??
      throw std::runtime_error("Need to implement THISCALL conv");
    }
  }
  else if(binary->getFormat() == BinaryFormat::ELF)
  {
    call_conv = CallConv::CDECL;
    number_of_args = 0;
  }
  else
  {
    throw std::runtime_error("Need to implement smtlib fake lib for this binary format");
  }

  std::optional<uint32_t> hooked_eax = std::nullopt;

  log->debug("Number of expected args: {}", number_of_args);

  // Try to retrieve arguments from the stack
  std::optional<std::vector<uint32_t>> args = std::make_optional<std::vector<uint32_t>>();
  for(unsigned int i = 1; i < number_of_args + 1; i++)
  {
    auto arg_value = machine_state->getMemDword(*esp_value + i * 0x04, true);
    if(arg_value.has_value())
    {
      args->push_back(*arg_value);
    }
    else
    {
      args = std::nullopt;
      log->info("\tFailed to recover args values");
      break;
    }
  }
  if(args.has_value())
  {
    // Print info (function arguments)
    std::string args_s = "(" + utils::uint322s(*args, ", ") + ")";
    log->info("\tArgs values: {}{}", func_lib_name, args_s);
    utils::pushMessage("Call external lib " + instr->toStringLight() + args_s);

    if(disable_lib_fun_hooks)
    {
      log->info("Do not simulate this external library function call");
    }
    else
    {
      log->info("Try to simulate this external library function call");
      if((func_lib_name == "kernel32.dll_GetModuleHandleA" || func_lib_name == "kernel32.dll_GetModuleHandleW") &&
         args->at(0) == 0x0)
      {
        // https://docs.microsoft.com/en-us/windows/win32/api/libloaderapi/nf-libloaderapi-getmodulehandlea
        // "If this parameter is NULL, GetModuleHandle returns a handle to the file used to create the calling process
        // (.exe file)." Used by molebox in first wave (0_0x1005b1b)
        // And also by emomet
        hooked_eax = binary->getBaseAddr();
      }

      else if(func_lib_name == "kernel32.dll_LoadLibraryA" || func_lib_name == "kernel32.dll_GetModuleHandleA" ||
              func_lib_name == "kernel32.dll_GetModuleHandleW")
      {
        std::optional<std::string> module;
        if(func_lib_name == "kernel32.dll_GetModuleHandleW")
        {
          module = machine_state->getUnicodeText(args->at(0), true);
        }
        else
        {
          module = machine_state->getAsciiText(args->at(0), true);
        }

        if(module.has_value() && *module != "")
        {
          transform((*module).begin(), (*module).end(), (*module).begin(), ::tolower);
          if(module->find(".dll") == std::string::npos)
          {
            *module += ".dll";
          }
          
          log->warn("Module '{}' loaded by LoadLibraryA or GetModuleHandleA/W", *module);
          utils::pushMessage("[" + waddr2s(instr->getWaddr()) + "] Module " + *module +
                             " dynamically loaded by kernel32.dll_LoadLibraryA or kernel32.dll_GetModuleHandleA/W");

          if (func_lib_name == "kernel32.dll_LoadLibraryA")
          {
            machine_state->linkDll(*module, load_dlls);
          }

          const auto& dlls = machine_state->getLoadedDlls();
          auto bin_dll_entry = dlls.find(*module);
          if (bin_dll_entry != dlls.end())
          {
            hooked_eax = bin_dll_entry->second.handle;
          }
          else
          {
            hooked_eax = 0x0;
          }
          // Aspack charge une première fois kernel32.dll dans la vague 0 et une seconde fois dans la vague 1
        }
      }
      else if(func_lib_name == "kernel32.dll_GetModuleHandleExA" || func_lib_name == "kernel32.dll_GetModuleHandleExW")
      {
        std::optional<std::string> module;
        if(func_lib_name == "kernel32.dll_GetModuleHandleExW")
        {
          module = machine_state->getUnicodeText(args->at(1), true);
        }
        else
        {
          module = machine_state->getAsciiText(args->at(1), true);
        }

        if(module.has_value() && *module != "")
        {
          transform((*module).begin(), (*module).end(), (*module).begin(), ::tolower);

          if(module->find(".dll") == std::string::npos)
          {
            *module += ".dll";
          }
          
          log->warn("Module '{}' loaded by GetModuleHandleExA/W", *module);
          utils::pushMessage("[" + waddr2s(instr->getWaddr()) + "] Module " + *module +
                             " dynamically loaded by kernel32.dll_GetModuleHandleExA/W");

          const auto& dlls = machine_state->getLoadedDlls();
          auto bin_dll_entry = dlls.find(*module);
          if (bin_dll_entry != dlls.end())
          {
            machine_state->setMemCellDwordValue(args->at(2), bin_dll_entry->second.handle);
          }
        }
        hooked_eax = 0x1;
      }
      else if(func_lib_name == "kernel32.dll__lopen")
      {
        // https://docs.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-_lopen
        std::optional<std::string> module = machine_state->getAsciiText(args->at(0), true);
        if(module.has_value() && *module != "")
        {
          log->info("Opened file: {}", *module);

          utils::pushMessage("[" + waddr2s(instr->getWaddr()) + "] Open file " + *module +
                             " with kernel32.dll__lopen");
          hooked_eax = 0xFFFFFFFF;
          // SVK opens file '\\.\TRW' 0_o
        }
      }
      else if(func_lib_name == "kernel32.dll_GetProcAddress")
      {
        // https://docs.microsoft.com/en-us/windows/win32/api/libloaderapi/nf-libloaderapi-getprocaddress

        // First arg
        // A handle to the DLL module that contains the function or variable. The LoadLibrary, LoadLibraryEx,
        // LoadPackagedLibrary, or GetModuleHandle function returns this handle. The GetProcAddress function
        // does not retrieve addresses from modules that were loaded using the LOAD_LIBRARY_AS_DATAFILE flag.
        // For more information, see LoadLibraryEx.
        addr_t module_handle = args->at(0);
        log->info("Handle to the DLL module that contains the function for which we want the address: {:#x}",
                  module_handle);

        // Second arg: The function or variable name, or the function's ordinal value.
        // If this parameter is an ordinal value, it must be in the low-order word; the high-order word must be
        // zero.
        std::optional<std::string> function = std::nullopt;

        if(args->at(1) <= 0xFFFF)
        {
          // Ordinal value case
          function = "Ordinal_" + std::to_string(args->at(1));
        }
        else
        {
          // Function name case
          function = machine_state->getAsciiText(args->at(1), true);
        }

        if(function.has_value())
        {
          log->warn("Retrieves the address of the exported function: '{}'", *function);
          utils::pushMessage("[" + waddr2s(instr->getWaddr()) +
                             "] Usage of kernel32.dll_GetProcAddress to retrieve address of '" + *function +
                             "' function");

          auto dll = machine_state->getLoadedDllAt(module_handle);
          
          if(dll == nullptr)
          {
            throw std::runtime_error("No loaded module at @ " + addr2s(module_handle) + "?!");
          }
          
          // If we have the DLL, use the "real" vaddr
          if(dll->binary != nullptr)
          {
            if(args->at(1) <= 0xFFFF)
            {
              auto export_entry = dll->binary->getExportByOrdinal((int)args->at(1));
              if(export_entry == nullptr)
              {
                throw std::runtime_error("Function at ordinal " + std::to_string((int)args->at(1)) + " should be in exported functions of DLL " + dll->binary->getFilepath());
              }
              hooked_eax = export_entry->vaddr;
            }
            else
            {
              auto export_entry = dll->binary->getExportByName(*function);
              if(export_entry == nullptr)
              {
                log->warn("Function {} not found in exported function of DLL {} --> Return 0x0", *function, dll->binary->getFilepath());
                hooked_eax = 0x0;
              }
              else
              {
                hooked_eax = export_entry->vaddr;
              }
            }
          }
          // Fake DLL case
          else
          {
            if(dll->exported_functions.find(*function) == dll->exported_functions.end())
            {
              auto fake_function_vaddr = dll->handle + dll->exported_functions.size();
              dll->exported_functions.insert({*function, fake_function_vaddr});
            }
            hooked_eax = dll->exported_functions.at(*function);
          }
        }
      }

      else if(func_lib_name == "kernel32.dll_VirtualAlloc")
      {
        /*
         LPVOID VirtualAlloc(
           LPVOID lpAddress, --> The starting address of the region to allocate, If this parameter is NULL, the
         system determines where to allocate the region SIZE_T dwSize, --> The size of the region, in bytes
         DWORD flAllocationType, --> The type of memory allocation. DWORD  flProtect --> The memory protection
         for the region of pages to be allocated
         );

         If the function succeeds, the return value is the base address of the allocated region of pages.

         Source: https://docs.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-virtualalloc
         */

        if(args->at(0) == 0x0)
        {
          hooked_eax = machine_state->getVirtualAllocPtr();
          machine_state->setVirtualAllocPtr(*hooked_eax + args->at(1));
        }
        else
        {
          hooked_eax = args->at(0);
        }

        if(args->at(3) == 0x04)
        {
          machine_state->setMemoryPermsRange(*hooked_eax, *hooked_eax + args->at(1),
                                             {Permission::READ, Permission::WRITE});
        }
        else if(args->at(3) == 0x02)
        {
          machine_state->setMemoryPermsRange(*hooked_eax, *hooked_eax + args->at(1), {Permission::READ});
        }
        else if(args->at(3) == 0x40)
        {
          machine_state->setMemoryPermsRange(*hooked_eax, *hooked_eax + args->at(1),
                                             {Permission::READ, Permission::WRITE, Permission::EXECUTE});
        }
        else
        {
          throw std::runtime_error("Call of kernel32.dll_VirtualAlloc with flProtect == " +
                                   utils::uint32_to_string(args->at(3)) + " --> need to be implemented");
        }
        
        // Zeros?
        for(auto i = 0; i <= args->at(1); i++)
        {
          machine_state->setMemCellValue(*hooked_eax + i, 0x00);
          SPDLOG_LOGGER_DEBUG(log, "\tPut 0x00 in @[{:#x}]_1", *hooked_eax + i);
        }
      }
      
      else if(func_lib_name == "kernel32.dll_VirtualAllocExNuma")
      {
        // kernel32.dll_VirtualAllocExNuma(0xffffffff, 0x0, 0x9, 0x3000, 0x40, 0x0)
        if(args->at(1) == 0x0)
        {
          hooked_eax = machine_state->getVirtualAllocPtr();
          machine_state->setVirtualAllocPtr(*hooked_eax + args->at(2));
        }
        else
        {
          hooked_eax = args->at(1);
        }

        if(args->at(4) == 0x04)
        {
          machine_state->setMemoryPermsRange(*hooked_eax, *hooked_eax + args->at(2),
                                             {Permission::READ, Permission::WRITE});
        }
        else if(args->at(4) == 0x02)
        {
          machine_state->setMemoryPermsRange(*hooked_eax, *hooked_eax + args->at(2), {Permission::READ});
        }
        else if(args->at(4) == 0x40)
        {
          machine_state->setMemoryPermsRange(*hooked_eax, *hooked_eax + args->at(2),
                                             {Permission::READ, Permission::WRITE, Permission::EXECUTE});
        }
        else
        {
          throw std::runtime_error("Call of kernel32.dll_VirtualAllocExNuma with flProtect == " +
                                   utils::uint32_to_string(args->at(4)) + " --> need to be implemented");
        }
        // Zeros?
        for(auto i = 0; i <= args->at(2); i++)
        {
          machine_state->setMemCellValue(*hooked_eax + i, 0x00);
          SPDLOG_LOGGER_DEBUG(log, "\tPut 0x00 in @[{:#x}]_1", *hooked_eax + i);
        }
      }
      
      else if(func_lib_name == "kernel32.dll_HeapCreate")
      {
        /*
         HANDLE HeapCreate(
           DWORD  flOptions,
           SIZE_T dwInitialSize,
           SIZE_T dwMaximumSize
         );

         Source: https://docs.microsoft.com/en-us/windows/win32/api/heapapi/nf-heapapi-heapcreate
         */
        hooked_eax = machine_state->getHeapPtr();
        if(args->at(1) == 0x0)
        {
          machine_state->setHeapPtr(*hooked_eax + 0x1000);
        }
        else
        {
          machine_state->setHeapPtr(*hooked_eax + args->at(1));
        }
      }
      
      else if(func_lib_name == "kernel32.dll_HeapAlloc")
      {
        /*
         DECLSPEC_ALLOCATOR LPVOID HeapAlloc(
           HANDLE hHeap,
           DWORD  dwFlags,
           SIZE_T dwBytes
         );
         
         Source: https://docs.microsoft.com/en-us/windows/win32/api/heapapi/nf-heapapi-heapalloc
         */
        hooked_eax = machine_state->getHeapPtr();
        machine_state->setHeapPtr(*hooked_eax + args->at(2));
      }
      else if(func_lib_name == "kernel32.dll_HeapSize")
      {
        //TODO: Need to return the correct value
        hooked_eax = 0x80;
      }

      else if(func_lib_name == "kernel32.dll_VirtualProtect")
      {
        /*
         BOOL VirtualProtect(
           LPVOID lpAddress, --> starting page of the region of pages whose access protection attributes are to
         be changed. SIZE_T dwSize, --> The size of the region whose access protection attributes are to be
         changed, in bytes DWORD  flNewProtect, --> The memory protection option PDWORD lpflOldProtect --> A
         pointer to a variable that receives the previous access protection value of the first page in the
         specified region of pages. If this parameter is NULL or does not point to a valid variable, the
         function fails.
         );

         If the function succeeds, the return value is nonzero.

         Source: https://docs.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-virtualprotect
         */

        hooked_eax = 0x1;

        // TODO: To do correctly
        machine_state->setMemCellDwordValue(args->at(3), 0x02);

        if(args->at(2) == 0x04)
        {
          machine_state->setMemoryPermsRange(args->at(0), args->at(0) + args->at(1) - 1,
                                             {Permission::READ, Permission::WRITE});
        }
        else if(args->at(2) == 0x02)
        {
          machine_state->setMemoryPermsRange(args->at(0), args->at(0) + args->at(1) - 1, {Permission::READ});
        }
        else if(args->at(2) == 0x40)
        {
          machine_state->setMemoryPermsRange(args->at(0), args->at(0) + args->at(1) - 1,
                                             {Permission::READ, Permission::WRITE, Permission::EXECUTE});
        }
        else if(args->at(2) == 0x20)
        {
          machine_state->setMemoryPermsRange(args->at(0), args->at(0) + args->at(1) - 1,
                                             {Permission::READ, Permission::EXECUTE});
        }
        else
        {
          throw std::runtime_error("Call of kernel32.dll_VirtualProtect with flNewProtect == " +
                                   utils::uint32_to_string(args->at(2)) + " --> need to be implemented");
        }
      }

      else if(func_lib_name == "kernel32.dll_CreateFileA")
      {
        /*
         HANDLE CreateFileA(
           LPCSTR                lpFileName,
           DWORD                 dwDesiredAccess,
           DWORD                 dwShareMode,
           LPSECURITY_ATTRIBUTES lpSecurityAttributes,
           DWORD                 dwCreationDisposition,
           DWORD                 dwFlagsAndAttributes,
           HANDLE                hTemplateFile
         );

         // If the function succeeds, the return value is an open handle to the specified file, device, named
         pipe, or mail slot.
         // If the function fails, the return value is INVALID_HANDLE_VALUE (= -1).

         Source: https://docs.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-createfilea
         */

        if(args->at(2) == 0x0 || args->at(2) == 0x4 || args->at(2) == 0x1 || args->at(2) == 0x2)
        {
          hooked_eax = 0x34;
        }
        else
        {
          hooked_eax = 0xFFFFFFFF;
        }
      }
      else if(func_lib_name == "kernel32.dll_GetFileSize")
      {
        hooked_eax = 0x2e7e;
      }
      else if(func_lib_name == "kernel32.dll_CloseHandle")
      {
        hooked_eax = 0xD8;
      }
      else if(func_lib_name == "kernel32.dll_GetModuleFileNameA")
      {
        /*
         DWORD GetModuleFileNameA(
           HMODULE hModule,
           LPSTR   lpFilename,
           DWORD   nSize
         );
         */
        
        if(args->at(0) == binary->getBaseAddr() || args->at(0) == 0x0)
        {
          // String ("e:\Exec\7mQKun8Qs.exe")
          machine_state->setMemCellValue(args->at(1), 0x65);
          machine_state->setMemCellValue(args->at(1) + 1, 0x3A);
          machine_state->setMemCellValue(args->at(1) + 2, 0x5C);
          machine_state->setMemCellValue(args->at(1) + 3, 0x45);
          machine_state->setMemCellValue(args->at(1) + 4, 0x78);
          machine_state->setMemCellValue(args->at(1) + 5, 0x65);
          machine_state->setMemCellValue(args->at(1) + 6, 0x63);
          machine_state->setMemCellValue(args->at(1) + 7, 0x5C);
          machine_state->setMemCellValue(args->at(1) + 8, 0x37);
          machine_state->setMemCellValue(args->at(1) + 9, 0x6D);
          machine_state->setMemCellValue(args->at(1) + 10, 0x51);
          machine_state->setMemCellValue(args->at(1) + 11, 0x4B);
          machine_state->setMemCellValue(args->at(1) + 12, 0x75);
          machine_state->setMemCellValue(args->at(1) + 13, 0x6E);
          machine_state->setMemCellValue(args->at(1) + 14, 0x38);
          machine_state->setMemCellValue(args->at(1) + 15, 0x51);
          machine_state->setMemCellValue(args->at(1) + 16, 0x73);
          machine_state->setMemCellValue(args->at(1) + 17, 0x2E);
          machine_state->setMemCellValue(args->at(1) + 18, 0x65);
          machine_state->setMemCellValue(args->at(1) + 19, 0x78);
          machine_state->setMemCellValue(args->at(1) + 20, 0x65);
          machine_state->setMemCellValue(args->at(1) + 21, 0x00);
          hooked_eax = 0x15;
        }
      }
      else if(func_lib_name == "shlwapi.dll_PathFindExtensionA")
      {
        unsigned int i = 0;
        hooked_eax = args->at(0) + 3;
        while(true)
        {
          std::optional<uint8_t> byte_value = machine_state->getMemCellValue(args->at(0) + i, true);
          if(byte_value.has_value())
          {
            if(*byte_value == 0x2e) // '.'
            {
              hooked_eax = args->at(0) + i;
            }
            if(*byte_value == 0x00)
            {
              break;
            }
          }
          if(i > 10000)
          {
            break;
          }
          i++;
        }
      }
      else if(func_lib_name == "shlwapi.dll_PathFindFileNameA")
      {
        unsigned int i = 0;
        hooked_eax = args->at(0) + 3;
        while(true)
        {
          std::optional<uint8_t> byte_value = machine_state->getMemCellValue(args->at(0) + i, true);
          if(byte_value.has_value())
          {
            if(*byte_value == 0x5C) // '\'
            {
              hooked_eax = args->at(0) + i + 1;
            }
            if(*byte_value == 0x00)
            {
              break;
            }
          }
          if(i > 10000)
          {
            break;
          }
          i++;
        }
      }
      else if(func_lib_name == "kernel32.dll_lstrcpyA")
      {
        unsigned int i = 0;
        hooked_eax = args->at(0);
        while(true)
        {
          std::optional<uint8_t> byte_value = machine_state->getMemCellValue(args->at(1) + i, true);
          if(byte_value.has_value())
          {
            SPDLOG_LOGGER_DEBUG(log, "\tCopy char {:#x} from @[{:#x}] to @[{:#x}]", *byte_value, args->at(1) + i, args->at(0) + i);
            machine_state->setMemCellValue(args->at(0) + i, *byte_value);
            if(*byte_value == 0x00)
            {
              break;
            }
          }
          else
          {
            machine_state->setMemCellValue(args->at(0) + i, std::nullopt);
            SPDLOG_LOGGER_DEBUG(log, "\tCopy char BOT from @[{:#x}] to @[{:#x}]", args->at(1) + i, args->at(0) + i);
          }
          if(i > 10000)
          {
            break;
          }
          i++;
        }
      }
      else if(func_lib_name == "kernel32.dll_lstrcpynA")
      {
        unsigned int i = 0;
        hooked_eax = args->at(0);
        while(i < args->at(2))
        {
          std::optional<uint8_t> byte_value = machine_state->getMemCellValue(args->at(1) + i, true);
          if(byte_value.has_value())
          {
            machine_state->setMemCellValue(args->at(0) + i, *byte_value);
            SPDLOG_LOGGER_DEBUG(log, "\tCopy char {:#x} from @[{:#x}] to @[{:#x}]", *byte_value, args->at(1) + i, args->at(0) + i);
            /*
            if(*byte_value == 0x00)
            {
              break;
            }
             */
          }
          else
          {
            machine_state->setMemCellValue(args->at(0) + i, std::nullopt);
            SPDLOG_LOGGER_DEBUG(log, "\tCopy char BOT from @[{:#x}] to @[{:#x}]", args->at(1) + i, args->at(0) + i);
          }
          if(i > 10000)
          {
            break;
          }
          i++;
        }
      }

      else if(func_lib_name == "kernel32.dll_LocalAlloc" || func_lib_name == "kernel32.dll_GlobalAlloc")
      {
        /*
         DECLSPEC_ALLOCATOR HLOCAL LocalAlloc(
           UINT   uFlags,
           SIZE_T uBytes
         );

         Source: https://docs.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-localalloc
         Source: https://docs.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-globalalloc
         */
        if(args->at(1) != 0)
        {
          hooked_eax = machine_state->getHeapPtr();
          machine_state->setHeapPtr(*hooked_eax + args->at(1));
          if(args->at(0) == 0x40)
          {
            // Initializes memory contents to zero.
            for(unsigned int i = 0; i < args->at(1); i++)
            {
              machine_state->setMemCellValue(*hooked_eax + i, 0);
            }
          }
        }
      }
      else if(func_lib_name == "kernel32.dll_LocalReAlloc")
      {
        hooked_eax = args->at(0);
      }
      
      else if(func_lib_name == "kernel32.dll_GetVersionExA")
      {
        /*
         NOT_BUILD_WINDOWS_DEPRECATE BOOL GetVersionExA(
           LPOSVERSIONINFOA lpVersionInformation
         );
         
         typedef struct _OSVERSIONINFOA {
           DWORD dwOSVersionInfoSize; --> 0x00
           DWORD dwMajorVersion;      --> 0x04
           DWORD dwMinorVersion;      --> 0x08
           DWORD dwBuildNumber;       --> 0x0C
           DWORD dwPlatformId;        --> 0x10
           CHAR  szCSDVersion[128];
         } OSVERSIONINFOA, *POSVERSIONINFOA, *LPOSVERSIONINFOA;
         
         Source: https://docs.microsoft.com/en-us/windows/win32/api/sysinfoapi/nf-sysinfoapi-getversionexa
         */
        // Simulate Windows 7
        machine_state->setMemCellDwordValue(args->at(0) + 0x04, 0x06);
        machine_state->setMemCellDwordValue(args->at(0) + 0x08, 0x01);
        
        // Simulate a Windows NT platform (VER_PLATFORM_WIN32_NT)
        machine_state->setMemCellDwordValue(args->at(0) + 0x10, 0x02);
        
        machine_state->setMemCellDwordValue(args->at(0) + 0x0c, 0x1db1);
        
        hooked_eax = 0x1;
      }
      else if(func_lib_name == "kernel32.dll_GetVersion")
      {
        hooked_eax = 0x1db10106; // From emomet trace
      }
      
      else if(func_lib_name == "kernel32.dll_InitializeCriticalSectionAndSpinCount")
      {
        /*
         BOOL InitializeCriticalSectionAndSpinCount(
           LPCRITICAL_SECTION lpCriticalSection,
           DWORD              dwSpinCount
         );

         Source: https://docs.microsoft.com/en-us/windows/win32/api/synchapi/nf-synchapi-initializecriticalsectionandspincount
         */
        hooked_eax = 0x1;
      }
      else if(func_lib_name == "kernel32.dll_FlsAlloc")
      {
        // Source: https://docs.microsoft.com/en-us/windows/win32/api/fibersapi/nf-fibersapi-flsalloc
        hooked_eax = 0x4;
      }
      else if(func_lib_name == "kernel32.dll_FlsSetValue")
      {
        // Source: https://docs.microsoft.com/en-us/windows/win32/api/fibersapi/nf-fibersapi-flssetvalue
        hooked_eax = 0x1;
      }
      else if(func_lib_name == "kernel32.dll_GetStdHandle")
      {
        // Source: https://docs.microsoft.com/fr-fr/windows/console/getstdhandle
        hooked_eax = 0x3;
      }
      else if(func_lib_name == "kernel32.dll_FindResourceA")
      {
        hooked_eax = 0x0;
      }
      else if(func_lib_name == "kernel32.dll_GetFileType")
      {
        // Source: https://docs.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-getfiletype
        hooked_eax = 0x2;
      }
      else if(func_lib_name == "advapi32.dll_RegOpenKeyExA")
      {
        hooked_eax = 0x2;
      }
      else if(func_lib_name == "kernel32.dll_GetEnvironmentStringsW")
      {
        // Source: https://docs.microsoft.com/en-us/windows/win32/api/processenv/nf-processenv-getenvironmentstringsw
        hooked_eax = 0x006607a8;
      }
      else if(func_lib_name == "kernel32.dll_WideCharToMultiByte")
      {
        /*
         int WideCharToMultiByte(
           UINT                               CodePage,
           DWORD                              dwFlags,
           _In_NLS_string_(cchWideChar)LPCWCH lpWideCharStr, // Pointer to the Unicode string to convert.
           int                                cchWideChar, // Size, in characters, of the string indicated by lpWideCharStr
           LPSTR                              lpMultiByteStr, // Pointer to a buffer that receives the converted string.
           int                                cbMultiByte, // Size, in bytes, of the buffer indicated by lpMultiByteStr.
           LPCCH                              lpDefaultChar,
           LPBOOL                             lpUsedDefaultChar
         );
         */
        addr_t lpWideCharStr = args->at(2);
        unsigned int cchWideChar = args->at(3);
        addr_t lpMultiByteStr = args->at(4);
        
        bool write_to_dst = false;
        if(lpMultiByteStr != 0)
        {
          log->info("lpMultiByteStr != nullptr --> Convert string");
          write_to_dst = true;
        }
        
        for(unsigned int i = 0; i <= cchWideChar ; i++)
        {
          auto wide_char = machine_state->getMemWord(lpWideCharStr, true);
          lpWideCharStr = lpWideCharStr + 2;
          if(write_to_dst)
          {
            machine_state->setMemCellValue(lpMultiByteStr, wide_char);
            log->debug("Write {:#x} value at @[{:#x}]_1", *wide_char, lpMultiByteStr);
            lpMultiByteStr ++;
          }
        }
        hooked_eax = cchWideChar;
      }
      else if(func_lib_name == "kernel32.dll_MultiByteToWideChar")
      {
        /*
         int MultiByteToWideChar(
           UINT                              CodePage,
           DWORD                             dwFlags,
           _In_NLS_string_(cbMultiByte)LPCCH lpMultiByteStr, // Pointer to the character string to convert.
           int                               cbMultiByte, // Size, in bytes, of the string indicated by the lpMultiByteStr
           LPWSTR                            lpWideCharStr, // Pointer to a buffer that receives the converted string.
           int                               cchWideChar // Size, in characters, of the buffer indicated by lpWideCharStr.
         );
         */
        addr_t lpMultiByteStr = args->at(2);
        unsigned int cbMultiByte = args->at(3);
        addr_t lpWideCharStr = args->at(4);
        unsigned int cchWideChar = args->at(5);
        
        bool write_to_dst = false;
        if(lpWideCharStr != 0)
        {
          log->info("lpWideCharStr != nullptr --> Convert string");
          write_to_dst = true;
        }
        
        for(unsigned int i = 0; i <= cbMultiByte ; i++)
        {
          auto mb_char = machine_state->getMemCellValue(lpMultiByteStr, true);
          lpMultiByteStr ++;
          if(write_to_dst)
          {
            if(mb_char.has_value())
            {
              machine_state->setMemCellWordValue(lpWideCharStr, *mb_char);
              log->debug("Write {:#x} value at @[{:#x}]_2", *mb_char, lpWideCharStr);
            }
            else
            {
              machine_state->setMemCellWordValue(lpWideCharStr, 0x00);
              log->debug("Write 0x00? value at @[{:#x}]_2", lpWideCharStr);
            }
            
            lpWideCharStr = lpWideCharStr + 2;
          }
        }
        
        hooked_eax = cbMultiByte;
      }
      else if(func_lib_name == "kernel32.dll_LCMapStringW")
      {
        hooked_eax = args->at(3);
      }
      else if(func_lib_name == "kernel32.dll_GetACP")
      {
        hooked_eax = 0x4e4;
      }
      else if(func_lib_name == "kernel32.dll_GetCPInfo")
      {
        hooked_eax = 0x1;
      }
      else if(func_lib_name == "kernel32.dll_GetStringTypeW")
      {
        /*
         BOOL GetStringTypeW(
           DWORD                         dwInfoType,
           _In_NLS_string_(cchSrc)LPCWCH lpSrcStr, // Pointer to the Unicode string for which to retrieve the character types.
           int                           cchSrc, // Size, in characters, of the string indicated by lpSrcStr.
           LPWORD                        lpCharType // Pointer to an array of 16-bit values
         );
         */
        addr_t lpSrcStr = args->at(1);
        unsigned int cchSrc = args->at(2);
        addr_t lpCharType = args->at(3);
        for(unsigned int i = 0; i <= cchSrc ; i++)
        {
          machine_state->setMemCellWordValue(lpCharType, 0x220);
          //auto wide_char = machine_state->getMemCellValue(lpSrcStr, true);
          //log->info("@[{:#x}] = {:#x}",lpSrcStr, *c);
          lpCharType = lpCharType + 2;
        }
        hooked_eax = 0x1;
      }
      else if(func_lib_name == "kernel32.dll_GetCurrentProcessId")
      {
        hooked_eax = 0x29c;
      }
      else if(func_lib_name == "kernel32.dll_GetCurrentProcess")
      {
        hooked_eax = 0xffffffff;
      }
      else if(func_lib_name == "kernel32.dll_GetCurrentThreadId")
      {
        hooked_eax = 0xb1c;
      }
      else if(func_lib_name == "kernel32.dll_IsDebuggerPresent")
      {
        hooked_eax = 0x0;
      }
      else if(func_lib_name == "kernel32.dll_TlsAlloc")
      {
        hooked_eax = 0x1b;
        // Tls slot is already init to zero
      }
      else if(func_lib_name == "kernel32.dll_TlsGetValue")
      {
        hooked_eax = machine_state->getTlsSlot();
      }
      else if(func_lib_name == "kernel32.dll_TlsSetValue")
      {
        machine_state->setTlsSlot(args->at(1));
        hooked_eax = 0x1;
      }
      else if(func_lib_name == "kernel32.dll_GlobalLock")
      {
        hooked_eax = 0x660bf0;
      }
      else if(func_lib_name == "kernel32.dll_CreateToolhelp32Snapshot")
      {
        hooked_eax = 0x74c;
      }
      else if(func_lib_name == "kernel32.dll_Process32First")
      {
        hooked_eax = 0x1;
      }
      else if(func_lib_name == "kernel32.dll_Process32Next")
      {
        hooked_eax = 0x0;
      }
      else if(func_lib_name == "kernel32.dll_GetCurrentThread")
      {
        hooked_eax = 0x1;
      }
      /*
      else if(func_lib_name == "msvcrt.dll___set_app_type")
      {
        hooked_eax = 0x1;
      }
      else if(func_lib_name == "msvcrt.dll___p__fmode")
      {
        hooked_eax = 0x77c3185c;
      }
      else if(func_lib_name == "msvcrt.dll___p__commode")
      {
        hooked_eax = 0x77c321fc;
      }
       */
      else if(func_lib_name == "kernel32.dll_CreateFileMappingA")
      {
        hooked_eax = 0x744;
      }
      else if(func_lib_name == "kernel32.dll_MapViewOfFile")
      {
        hooked_eax = 0xf60000;
      }
      else if(func_lib_name == "kernel32.dll_GetCommandLineA")
      {
        hooked_eax = 0x642bd0;
      }
      else if(func_lib_name == "kernel32.dll_GetLastError")
      {
        // Nothing
      }
      else if(func_lib_name == "kernel32.dll_GetStartupInfoA")
      {
        hooked_eax = 0x0; // Random value because return value is void
      }
      else if(func_lib_name == "kernel32.dll_QueryPerformanceCounter")
      {
        hooked_eax = 0x1;
        machine_state->setMemCellDwordValue(args->at(0), 0xbaffe);
      }
      else if(func_lib_name == "kernel32.dll_GetTickCount")
      {
        hooked_eax = 0x5728;
      }
      else if(func_lib_name == "ntdll.dll_LdrFindResource_U")
      {
        machine_state->setMemCellDwordValue(args->at(3), 0x462be8);
        hooked_eax = 0x0;
      }
      else if(func_lib_name == "ntdll.dll_LdrAccessResource")
      {
        machine_state->setMemCellDwordValue(args->at(2), 0x463038);
        machine_state->setMemCellDwordValue(args->at(3), 0x1c933);
      }
      else if(func_lib_name == "rand")
      {
        hooked_eax = rand();
      }
      else if(func_lib_name == "malloc")
      {
        // TODO: Use real malloc size instead of 0x1000
        hooked_eax = machine_state->getHeapPtr();
        machine_state->setHeapPtr(*hooked_eax + 0x1000);
      }
      else
      {
        log->warn("Missing hook of '{}' function", func_lib_name);
      }
    }
  } // args.has_value() end

  // Symbolize ECX and EDX
  // machine_state->setRegValue(Register::ECX, std::nullopt);
  // machine_state->setRegValue(Register::EDX, std::nullopt);

  // Simulate return value in EAX register
  if(hooked_eax.has_value())
  {
    log->info("Hooked EAX value: {:#x}", *hooked_eax);
    machine_state->setRegValue(Register::EAX, *hooked_eax);
  }
  else
  {
    machine_state->setRegValue(Register::EAX, std::nullopt);
  }
  
  // Recover the returnsite address
  if(args.has_value())
  {
    auto returnsite = machine_state->getMemDword(*esp_value, false);
    if(returnsite.has_value())
    {
      machine_state->setInstrPtr(*returnsite);
    }
    else
    {
      machine_state->setInstrPtr(0x99999999);
    }
  }
  else
  {
    if(!machine_state->getReturnsiteStack().empty())
    {
      addr_t returnsite = machine_state->getReturnsiteStack().back();
      SPDLOG_LOGGER_DEBUG(
          log, "Assume that the RET of an external lib function is genuine and jump to the returnsite: {:#x}",
          returnsite);
      machine_state->setInstrPtr(returnsite);
    }
  }
  
  // Simulate the callee stack clean-up and RET POP
  if(call_conv == CallConv::STDCALL)
  {
    machine_state->setRegValue(Register::ESP, *esp_value + number_of_args * 4 + 4);
  }
  else
  {
    machine_state->setRegValue(Register::ESP, *esp_value + 4);
  }
}

void MachineStateSymbExec::ExceptionExitHook(MachineState* machine_state, std::shared_ptr<spdlog::logger> log)
{
  exception_context_t& exception_context = *machine_state->getExceptionContext();
  auto esp_value = machine_state->getRegValue(Register::ESP);
  if(esp_value.has_value())
  {
    auto esp_value_before_ret = *esp_value - 0x4;
    // @[ESP + 0xc] = Pointeur sur le contextRecord = ESP + 0xFC
    auto context_record_struct_addr = esp_value_before_ret + 0xFC;
    machine_state->setMemCellDwordValue(esp_value_before_ret + 0xc, context_record_struct_addr);
    // On restaure les registres
    machine_state->setRegValueFromMemDword(Register::DR0, context_record_struct_addr + 0x04, false);
    machine_state->setRegValueFromMemDword(Register::DR1, context_record_struct_addr + 0x08, false);
    machine_state->setRegValueFromMemDword(Register::DR2, context_record_struct_addr + 0x0c, false);
    machine_state->setRegValueFromMemDword(Register::DR3, context_record_struct_addr + 0x10, false);
    machine_state->setRegValueFromMemDword(Register::DR6, context_record_struct_addr + 0x14, false);
    machine_state->setRegValueFromMemDword(Register::DR7, context_record_struct_addr + 0x18, false);
    machine_state->setRegValueFromMemDword(Register::FS, context_record_struct_addr + 0x90, false);
    machine_state->setRegValueFromMemDword(Register::EDI, context_record_struct_addr + 0x9c, false);
    machine_state->setRegValueFromMemDword(Register::ESI, context_record_struct_addr + 0xA0, false);
    machine_state->setRegValueFromMemDword(Register::EBX, context_record_struct_addr + 0xA4, false);
    machine_state->setRegValueFromMemDword(Register::EDX, context_record_struct_addr + 0xA8, false);
    machine_state->setRegValueFromMemDword(Register::ECX, context_record_struct_addr + 0xAC, false);
    machine_state->setRegValueFromMemDword(Register::EAX, context_record_struct_addr + 0xB0, false);
    machine_state->setRegValueFromMemDword(Register::EBP, context_record_struct_addr + 0xB4, false);
    machine_state->setRegValueFromMemDword(Register::ESP, context_record_struct_addr + 0xC4, false);

    // EFLAGS
    auto eflags = machine_state->getMemDword(context_record_struct_addr + 0xC0, false);
    auto eflags_had_value = machine_state->getMemDword(context_record_struct_addr + 0xBC, false);
    if (eflags.has_value() && eflags_had_value.has_value())
    {
      // CF
      if (*eflags_had_value & 0x00000001U)
        machine_state->setRegValue(Register::CF, *eflags & 0x00000001U);
      else
        machine_state->setRegValue(Register::CF, std::nullopt);
      // PF
      if (*eflags_had_value & (0x00000001U << 2))
        machine_state->setRegValue(Register::PF, !!(*eflags & (0x00000001U << 2)));
      else
        machine_state->setRegValue(Register::PF, std::nullopt);
      // AF
      if (*eflags_had_value & (0x00000001U << 4))
        machine_state->setRegValue(Register::AF, !!(*eflags & (0x00000001U << 4)));
      else
        machine_state->setRegValue(Register::AF, std::nullopt);
      // ZF
      if (*eflags_had_value & (0x00000001U << 6))
        machine_state->setRegValue(Register::ZF, !!(*eflags & (0x00000001U << 6)));
      else
        machine_state->setRegValue(Register::ZF, std::nullopt);
      // SF
      if (*eflags_had_value & (0x00000001U << 7))
        machine_state->setRegValue(Register::SF, !!(*eflags & (0x00000001U << 7)));
      else
        machine_state->setRegValue(Register::SF, std::nullopt);
      // TF
      if (*eflags_had_value & (0x00000001U << 8))
        machine_state->setRegValue(Register::TF, !!(*eflags & (0x00000001U << 8)));
      else
        machine_state->setRegValue(Register::TF, std::nullopt);
      // DF
      if (*eflags_had_value & (0x00000001U << 10))
        machine_state->setRegValue(Register::DF, !!(*eflags & (0x00000001U << 10)));
      else
        machine_state->setRegValue(Register::DF, std::nullopt);
      // OF
      if (*eflags_had_value & (0x00000001U << 11))
        machine_state->setRegValue(Register::OF, !!(*eflags & (0x00000001U << 11)));
      else
        machine_state->setRegValue(Register::OF, std::nullopt);
    }

    exception_context.handler_exit_target = machine_state->getMemDword(context_record_struct_addr + 0xB8, false);
    if(exception_context.handler_exit_target.has_value())
    {
      log->warn("Address to execute now: {:#x}", *exception_context.handler_exit_target);
    }
    else
    {
      log->warn("Address to execute now: ???");
    }
  }
}

void MachineStateSymbExec::ExceptionEntryHook(MachineState* machine_state, std::shared_ptr<spdlog::logger> log)
{
  exception_context_t& exception_context = *machine_state->getExceptionContext();

  auto esp_value = machine_state->getRegValue(Register::ESP);
  if(esp_value.has_value())
  {
    // ESP_après = ESP_avant - 0x3C8
    auto esp_value_handler_entry = *esp_value + 0x3C8;

    // @[ESP] = @ de retour dans le kernel utlisé par le RET du handler (normalement)
    // On va mettre 0x7ccccccc
    machine_state->setMemCellDwordValue(esp_value_handler_entry, exception_context.fake_kernel_return_address);

    // @[ESP + 0x4] = Pointeur sur le Exception Record = ESP + 0xE8
    auto exception_record_struct_addr = esp_value_handler_entry + 0xE8;
    machine_state->setMemCellDwordValue(esp_value_handler_entry + 0x4, exception_record_struct_addr);
    // On peuple la structure ExceptionRecord
    // @[ExceptionRecord] = ExceptionCode
    machine_state->setMemCellDwordValue(exception_record_struct_addr, exc2code(exception_context.exception_type));
    // @[ExceptionRecord + 0x4] = ExceptionFlags
    // @[ExceptionRecord + 0x8] = Pointeur sur un ExceptionRecord?
    // @[ExceptionRecord + 0xc] = ExceptionAddress
    machine_state->setMemCellDwordValue(exception_record_struct_addr + 0xc, exception_context.trigger_instr->getAddr());
    // @[ExceptionRecord + 0x10] = NumberParamters
    // @[ExceptionRecord + 0x14] = ??

    // @[ESP + 0x8] = Establisher Frame
    // D'après les traces, il contient la la valeur de ESP juste avant l'exception...
    machine_state->setMemCellDwordValue(esp_value_handler_entry + 0x8, *esp_value);

    // @[ESP + 0xc] = Pointeur sur le contextRecord = ESP + 0xFC
    auto context_record_struct_addr = esp_value_handler_entry + 0xFC;
    machine_state->setMemCellDwordValue(esp_value_handler_entry + 0xc, context_record_struct_addr);
    // On peuple la structure Context Record
    machine_state->setMemCellDwordValue(context_record_struct_addr + 0x04, machine_state->getRegValue(Register::DR0));
    machine_state->setMemCellDwordValue(context_record_struct_addr + 0x08, machine_state->getRegValue(Register::DR1));
    machine_state->setMemCellDwordValue(context_record_struct_addr + 0x0c, machine_state->getRegValue(Register::DR2));
    machine_state->setMemCellDwordValue(context_record_struct_addr + 0x10, machine_state->getRegValue(Register::DR3));
    machine_state->setMemCellDwordValue(context_record_struct_addr + 0x14, machine_state->getRegValue(Register::DR6));
    machine_state->setMemCellDwordValue(context_record_struct_addr + 0x18, machine_state->getRegValue(Register::DR7));
    machine_state->setMemCellDwordValue(context_record_struct_addr + 0x90, machine_state->getRegValue(Register::FS));
    machine_state->setMemCellDwordValue(context_record_struct_addr + 0x9c, machine_state->getRegValue(Register::EDI));
    machine_state->setMemCellDwordValue(context_record_struct_addr + 0xA0, machine_state->getRegValue(Register::ESI));
    machine_state->setMemCellDwordValue(context_record_struct_addr + 0xA4, machine_state->getRegValue(Register::EBX));
    machine_state->setMemCellDwordValue(context_record_struct_addr + 0xA8, machine_state->getRegValue(Register::EDX));
    machine_state->setMemCellDwordValue(context_record_struct_addr + 0xAC, machine_state->getRegValue(Register::ECX));
    machine_state->setMemCellDwordValue(context_record_struct_addr + 0xB0, machine_state->getRegValue(Register::EAX));
    machine_state->setMemCellDwordValue(context_record_struct_addr + 0xB4, machine_state->getRegValue(Register::EBP));
    addr_t eip = exception_context.trigger_instr->getAddr();
    if(exception_context.trigger_instr->getType() == InstrType::TRAP)
    {
      eip++;
    }
    machine_state->setMemCellDwordValue(context_record_struct_addr + 0xB8, eip);
    machine_state->setMemCellDwordValue(context_record_struct_addr + 0xC4, machine_state->getRegValue(Register::ESP));

    /*
     * EFLAGS: BOA stores flags in multiple registers, as std::optional values.
     * If a std::nullopt value is stored, the corresponding EFLAGS bit is set to
     * zero and a bit field is added to restore this special value later.
     * TODO: move to `utils/` ?
     */
    uint32_t eflags = 0x00000000U;
    uint32_t eflags_had_value = 0xffffffffU;
    // CF
    auto flag = machine_state->getRegValue(Register::CF);
    if (flag.has_value())
      eflags |= *flag & 0x00000001U;
    else
      eflags_had_value &= ~0x00000001U;
    // Reserved
    eflags |= 0x00000002U;
    // PF
    flag = machine_state->getRegValue(Register::PF);
    if (flag.has_value())
      eflags |= (*flag & 0x00000001U) << 2;
    else
      eflags_had_value &= ~(0x00000001U << 2);
    // AF
    flag = machine_state->getRegValue(Register::AF);
    if (flag.has_value())
      eflags |= (*flag & 0x00000001U) << 4;
    else
      eflags_had_value &= ~(0x00000001U << 4);
    // ZF
    flag = machine_state->getRegValue(Register::ZF);
    if (flag.has_value())
      eflags |= (*flag & 0x00000001U) << 6;
    else
      eflags_had_value &= ~(0x00000001U << 6);
    // SF
    flag = machine_state->getRegValue(Register::SF);
    if (flag.has_value())
      eflags |= (*flag & 0x00000001U) << 7;
    else
      eflags_had_value &= ~(0x00000001U << 7);
    // TF
    flag = machine_state->getRegValue(Register::TF);
    if (flag.has_value())
      eflags |= (*flag & 0x00000001U) << 8;
    else
      eflags_had_value &= ~(0x00000001U << 8);
    // DF
    flag = machine_state->getRegValue(Register::DF);
    if (flag.has_value())
      eflags |= (*flag & 0x00000001U << 10);
    else
      eflags_had_value &= ~(0x00000001U << 10);
    // OF
    flag = machine_state->getRegValue(Register::OF);
    if (flag.has_value())
      eflags |= (*flag & 0x00000001U << 11);
    else
      eflags_had_value &= ~(0x00000001U << 11);
    // Store EFLAGS
    machine_state->setMemCellDwordValue(context_record_struct_addr + 0xC0, eflags);
    // Store bit field somewhere... here in SegCS
    machine_state->setMemCellDwordValue(context_record_struct_addr + 0xBC, eflags_had_value);

    // @[ESP + 0x10] = Pointeur sur le DispatcherContext
    // TODO

    // Maintenant il faut trouver l'@ du handler (on prend le premier dans la chaine SEH)
    auto fs_value = machine_state->getRegValue(Register::FS);
    if(fs_value.has_value())
    {
      SPDLOG_LOGGER_DEBUG(log, "FS value: {:#x}", *fs_value);
      // @ de début de la chaine = FS
      // Début de la chaine = @[FS]
      auto seh_chain_entry = machine_state->getMemDword(*fs_value, false);
      if(seh_chain_entry.has_value())
      {
        SPDLOG_LOGGER_DEBUG(log, "Entrée de la chaine SEH: {:#x}", *seh_chain_entry);
        // @ handler = @[début chaine SEH + 4]
        auto handler_addr = machine_state->getMemDword(*seh_chain_entry + 0x4, false);
        if(handler_addr.has_value())
        {
          SPDLOG_LOGGER_DEBUG(log, "@ du premier handler de la chaine: {:#x}", *handler_addr);
          exception_context.handler_entry_addr = *handler_addr;
        }
      }
    }

    // Registers new values
    machine_state->setRegValue(Register::ESP, esp_value_handler_entry);

    // EAX, EBX, EDI, ESI sont xoré
    machine_state->setRegValue(Register::EAX, 0x0);
    machine_state->setRegValue(Register::EBX, 0x0);
    machine_state->setRegValue(Register::EDI, 0x0);
    machine_state->setRegValue(Register::ESI, 0x0);

    // Clear TF flag?? Maybe...
    machine_state->setRegValue(Register::TF, 0x0);
  }
  return;
}

void MachineStateSymbExec::repHack(MachineState* machine_state, const BasicBlock* const bb,
                                   std::shared_ptr<spdlog::logger> log)
{
  const auto& rep_bytes = bb->getLastInstr()->getBytes();

  const static std::vector<uint8_t> rep_movsb_bytes = {0xf3, 0xa4};
  const static std::vector<uint8_t> rep_movsd_bytes = {0xf3, 0xa5};
  const static std::vector<uint8_t> rep_stosb_bytes = {0xf3, 0xaa};
  const static std::vector<uint8_t> rep_stosd_16_bytes = {0x66, 0xf3, 0xab};
  const static std::vector<uint8_t> rep_stosd_bytes = {0xf3, 0xab};
  const static std::vector<uint8_t> rep_scasb_bytes = {0xf2, 0xae};
  const static std::vector<uint8_t> rep_scasd_bytes = {0xf2, 0xaf};

  // TODO: Check how to implement 'repne movsb' with the ZF flag
  const static std::vector<uint8_t> repne_movsb_bytes = {0xf2, 0xa4};

  auto ecx_value = machine_state->getRegValue(Register::ECX);
  auto df_value = machine_state->getRegValue(Register::DF);
  auto edi_value = machine_state->getRegValue(Register::EDI);
  auto esi_value = machine_state->getRegValue(Register::ESI);

  if(ecx_value.has_value() && df_value.has_value())
  {
    log->info("REP instruction with ECX entry value {:#x} and DF entry value {:#x}", *ecx_value, *df_value);

    // rep movsd/movsb case
    if(rep_bytes == rep_movsb_bytes || rep_bytes == rep_movsd_bytes || rep_bytes == repne_movsb_bytes)
    {
      unsigned int step = 4;
      if(rep_bytes == rep_movsb_bytes || rep_bytes == repne_movsb_bytes)
      {
        step = 1;
      }

      if(edi_value.has_value() && esi_value.has_value())
      {
        log->info("EDI entry value: {:#x}, ESI entry value: {:#x}", *edi_value, *esi_value);

        // 0: if (ecx<32> = 0<32>) goto 6 else goto 1
        while(ecx_value != 0)
        {
          // 1: @[edi<32>,4] := @[esi<32>,4]; OR @[edi<32>,1] := @[esi<32>,1];
          for(size_t i = 0; i < step; i++)
          {
            auto read_addr = *esi_value + i;
            auto written_addr = *edi_value + i;

            auto esi_mem_cell_value = machine_state->getMemCellValue(read_addr, true);
            if(esi_mem_cell_value.has_value())
            {
              SPDLOG_LOGGER_DEBUG(log, "✅ @[{:#x}] <-- @[{:#x}] ({:#x})", written_addr, read_addr, *esi_mem_cell_value);
            }
            else
            {
              SPDLOG_LOGGER_DEBUG(log, "❌ @[{:#x}] <-- @[{:#x}] (unknown)", written_addr, read_addr);
            }

            machine_state->setMemCellValue(written_addr, *esi_mem_cell_value);
            machine_state->addWrittenAddrInHistory(written_addr);
          }

          // 2: esi<32> := DF<1> ? (esi<32> - 4<32>) : (4<32> + esi<32>);
          // 3: edi<32> := DF<1> ? (edi<32> - 4<32>) : (4<32> + edi<32>);
          if(df_value == 0)
          {
            *edi_value = *edi_value + step;
            *esi_value = *esi_value + step;
          }
          else
          {
            *edi_value = *edi_value - step;
            *esi_value = *esi_value - step;
          }

          // 4: ecx<32> := (ecx<32> - 1<32>);
          *ecx_value = *ecx_value - 1;
        }

        machine_state->setRegValue(Register::ECX, *ecx_value);
        machine_state->setRegValue(Register::EDI, *edi_value);
        machine_state->setRegValue(Register::ESI, *esi_value);
      }
      else
      {
        log->warn("REP movsd/movsb instruction with unknown ESI or EDI value :-(");

        // ECX --> 0x0
        machine_state->setRegValue(Register::ECX, 0x0);

        // ESI and EDI --> TOP
        machine_state->setRegValue(Register::ESI, std::nullopt);
        machine_state->setRegValue(Register::EDI, std::nullopt);

        // Memory
        s_write_at_unknown_location_cnt++;
      }
    }

    // rep stosd/stosb case
    else if(rep_bytes == rep_stosb_bytes || rep_bytes == rep_stosd_bytes || rep_bytes == rep_stosd_16_bytes)
    {
      unsigned int step = 4;
      if(rep_bytes == rep_stosd_16_bytes)
      {
        step = 2;
      }
      else if(rep_bytes == rep_stosb_bytes)
      {
        step = 1;
      }

      auto edi_value = machine_state->getRegValue(Register::EDI);

      if(edi_value.has_value())
      {
        log->info("EDI entry value: {:#x}", *edi_value);

        // Mem
        std::unordered_set<addr_t> proceced_mem_cases{};
        auto eax_value = machine_state->getRegValue(Register::EAX);

        std::vector<uint8_t> eax_bytes = {0x00, 0x00, 0x00, 0x00};
        if(eax_value.has_value())
        {
          uint32_t v = *eax_value;
          eax_bytes[3] = (v & 0xFF000000) >> 24;
          eax_bytes[2] = (v & 0x00FF0000) >> 16;
          eax_bytes[1] = (v & 0x0000FF00) >> 8;
          eax_bytes[0] = v & 0x000000FF;
        }

        // 0: if (ecx<32> = 0<32>) goto 6 else goto 1
        while(*ecx_value != 0)
        {
          for(size_t i = 0; i < step; i++)
          {
            // 1: @[edi<32>,4] := eax<32>; OR @[edi<32>,1] := eax<32>{0,7};
            auto written_addr = *edi_value + i;

            if(eax_value.has_value())
            {
              SPDLOG_LOGGER_DEBUG(log, "✅ @[{:#x}] <-- {:#x}", written_addr, eax_bytes[i]);
              machine_state->setMemCellValue(written_addr, eax_bytes[i]);
            }
            else
            {
              SPDLOG_LOGGER_DEBUG(log, "❌ @[{:#x}]_exit: unknown", written_addr);
              machine_state->setMemCellValue(written_addr, std::nullopt);
            }

            proceced_mem_cases.insert(written_addr);
            machine_state->addWrittenAddrInHistory(written_addr);
          }

          // 2: edi<32> := DF<1> ? (edi<32> - 4<32>) : (4<32> + edi<32>); OR edi<32> := DF<1> ? (edi<32> -
          // 1<32>) : (1<32> + edi<32>);
          if(df_value == 0)
          {
            *edi_value = *edi_value + step;
          }
          else
          {
            *edi_value = *edi_value - step;
          }

          // 3: ecx<32> := (ecx<32> - 1<32>);
          *ecx_value = *ecx_value - 1;
        }

        machine_state->setRegValue(Register::ECX, *ecx_value);
        machine_state->setRegValue(Register::EDI, *edi_value);
      }
      else
      {
        log->warn("REP stosd/stosb instruction with unknown ESI or EDI value :-(");

        // ECX --> 0x0
        machine_state->setRegValue(Register::ECX, 0x0);

        // EDI --> TOP
        machine_state->setRegValue(Register::EDI, std::nullopt);

        // Memory
        s_write_at_unknown_location_cnt++;
      }
    }

    // repne scasb/scasd case
    else if(rep_bytes == rep_scasb_bytes || rep_bytes == rep_scasd_bytes)
    {
      /*
       while (ecx != 0) {
           ZF = (al == *(BYTE *)edi);
           if (DF == 0)
               edi++;
           else
               edi--;
           ecx--;
           if (ZF) break;
       }
       */
      unsigned int step = 4;
      if(rep_bytes == rep_scasb_bytes)
      {
        step = 1;
      }

      auto edi_value = machine_state->getRegValue(Register::EDI);
      auto eax_value = machine_state->getRegValue(Register::EAX);

      if(edi_value.has_value() && eax_value.has_value())
      {
        log->info("EDI entry value: {:#x}, EAX entry value: {:#x}", *edi_value, *eax_value);

        std::vector<uint8_t> eax_bytes = {0x00, 0x00, 0x00, 0x00};
        eax_bytes[3] = (*eax_value & 0xFF000000) >> 24;
        eax_bytes[2] = (*eax_value & 0x00FF0000) >> 16;
        eax_bytes[1] = (*eax_value & 0x0000FF00) >> 8;
        eax_bytes[0] = *eax_value & 0x000000FF;

        // 0: while (ecx != 0)
        while(*ecx_value != 0)
        {

          // 1: ZF = (al == *(BYTE *)edi);
          int zf = 0;
          if(step == 4)
          {
            auto esi_mem_cell_value = machine_state->getMemDword(*edi_value, true);
            if(esi_mem_cell_value.has_value())
            {
              SPDLOG_LOGGER_DEBUG(log, "Compare EAX ({:#x}) with @[EDI]_4 (@[{:#x}]_4 == {:#x})", *eax_value,
                                  *edi_value, *esi_mem_cell_value);
              if(*eax_value == *esi_mem_cell_value)
              {
                zf = 1;
              }
            }
            else
            {
              SPDLOG_LOGGER_DEBUG(log, "@[EDI]_4 (@[{:#x}]_4 is unknown", *edi_value);
              zf = 1;
            }
          }
          else
          {
            auto esi_mem_cell_value = machine_state->getMemCellValue(*edi_value, true);
            if(esi_mem_cell_value.has_value())
            {
              SPDLOG_LOGGER_DEBUG(log, "Compare AL ({:#x}) with @[EDI]_1 (@[{:#x}]_1 == {:#x})", eax_bytes[0],
                                  *edi_value, *esi_mem_cell_value);
              if(eax_bytes[0] == *esi_mem_cell_value)
              {
                zf = 1;
              }
            }
            else
            {
              SPDLOG_LOGGER_DEBUG(log, "@[EDI]_1 (@[{:#x}]_1 is unknown", *edi_value);
              zf = 1;
            }
          }

          // 2: edi<32> := DF<1> ? (edi<32> - 4<32>) : (4<32> + edi<32>); OR edi<32> := DF<1> ? (edi<32> -
          // 1<32>) : (1<32> + edi<32>);
          if(df_value == 0)
          {
            *edi_value = *edi_value + step;
          }
          else
          {
            *edi_value = *edi_value - step;
          }

          //  ecx<32> := (ecx<32> - 1<32>);
          *ecx_value = *ecx_value - 1;

          // if (ZF) break;
          if(zf == 1)
          {
            break;
          }
        }

        machine_state->setRegValue(Register::ECX, *ecx_value);
        machine_state->setRegValue(Register::EDI, *edi_value);
      }
      else
      {
        log->warn("REPNE scasb/scasd instruction with unknown EAX or EDI value :-(");

        // EDI --> TOP
        machine_state->setRegValue(Register::EDI, std::nullopt);
      }
    }

    else
    {
      throw std::runtime_error("Need to implement REP instr " + bb->getLastInstr()->getOpcode());
    }
  }

  else
  {
    log->warn("REP instruction with unknown ECX or DF value :-(");

    // ECX_end is always 0x00
    machine_state->setRegValue(Register::ECX, 0x0);

    // If REP mov
    if(rep_bytes == rep_movsb_bytes || rep_bytes == rep_movsd_bytes)
    {
      // ESI and EDI --> TOP
      machine_state->setRegValue(Register::ESI, std::nullopt);
      machine_state->setRegValue(Register::EDI, std::nullopt);
    }

    // If REP stos
    else if(rep_bytes == rep_stosb_bytes || rep_bytes == rep_stosd_bytes)
    {
      // EDI --> TOP
      machine_state->setRegValue(Register::EDI, std::nullopt);
    }

    // Memory
    s_write_at_unknown_location_cnt++;
  }
}

void MachineStateSymbExec::get(ExecFormula& exec_formula, std::shared_ptr<spdlog::logger> log,
                               SolverManager& solver_manager, Binary* binary, std::optional<addr_t>& new_bb_ep,
                               const std::string& windows_dlls_json_filepath, bool disable_lib_fun_hooks, bool load_dlls)
{

  // SPDLOG_LOGGER_DEBUG(log, "[+] Compute exit machine state valuation");

  const BasicBlock* bb = exec_formula.getBB();
  const Instr* last_instr = bb->getLastInstr();
  MachineState* machine_state = exec_formula.getEntryMachineState();

  // REP Hack
  // Note: Le REP ne peut pas faire une modification "intra" BB car de toute façon il est isolé dans un BB de taille 1
  static const std::vector<uint8_t> bnd_ret_bytes = {0xf2, 0xc3};
  if(last_instr->getSubType() == InstrSubType::REP && last_instr->getBytes() != bnd_ret_bytes)
  {
    // TODO: A REP can trigger an exception!
    repHack(machine_state, bb, log);
    return;
  }

  // If we have a Fake lib we need to hook it
  if(last_instr->getType() == InstrType::FAKE_LIB_FUNC)
  {
    log->info("Hook external function '{}' execution", last_instr->getOpcode());
    ExternalLibFuncHook(machine_state, binary, last_instr, windows_dlls_json_filepath, log, disable_lib_fun_hooks, load_dlls);
    return;
  }

  // Get conc values from solver
  auto conc_values = exec_formula.getConcValues(solver_manager, binary);
  SPDLOG_LOGGER_DEBUG(log, "Results source: {}", ExecFormula::resultSrcToString(conc_values->result_src));

  // Before modifying the machine state, we need to check for any self-modification OR exception
  // If this is the case, we have to split the BB and exec again the splitted BB with the original BB

  // Note: Une instruction ne peut pas semodifier elle-même pendant son exécution (une fois que le CPU l'a chargée
  // c'est temriné pour elle) Par contre, une instruction peut modifier l'instruction suivante ou bien une autre plus
  // loin dans le BB Finalement, seulement les BBs de taille 2 ou plus sont à considérer Si par contre une instruction
  // du BB modifie les bytes d'une instruction du même BB mais en amont, ce n'est pas grâve car elle a déjà été
  // exécuté. Si ce BB est exécuté à nouveau plus tard, on détectera la self-modif lors du désassemblage

  SPDLOG_LOGGER_TRACE(log, "Check for any intra-BB self-modification or write access violation");

  // First, get bytes addresses of each instr of this BB

  // Key: Byte address
  // Value: Correcponding instruction number
  std::unordered_map<addr_t, unsigned int> bb_instrs_bytes_addrs;

  unsigned int cnt = 0;
  for(const auto& instr : bb->getInstrs())
  {
    addr_t instr_addr = instr->getAddr();
    for(unsigned int i = 0; i < instr->getSize(); i++)
    {
      bb_instrs_bytes_addrs.insert({instr_addr + i, cnt});
    }
    cnt++;
  }

  // Now check the written operations performed by this BB
  cnt = 0;
  for(const auto& instr : bb->getInstrs())
  {

    const auto& instr_tainting_info = instr->getTaintingInfo();
    // Check if one instruciton set TF flag to 1
    if(instr_tainting_info.regs_write.find(Register::TF) != instr_tainting_info.regs_write.end())
    {
      if(cnt != bb->getSize() - 1)
      {
        // Single step exception case (Trap flag)
        auto tf_value = conc_values->exit_regs.at(Register::TF);
        if(tf_value.has_value() && *tf_value == 0x1)
        {
          log->warn("Instruction {} set TF flag to 1 --> EXCEPTION_SINGLE_STEP", instr->toStringLight());
          new_bb_ep = instr->getNextAddr();
          exception_context_t exception_context = {instr.get(), 0x7ccccccc, std::nullopt, std::nullopt, Exception::SINGLE_STEP};
          machine_state->setExceptionContext(exception_context);
          return;
        }
      }
    }

    // Single step exception case (Debug registers)
    auto dr0_value = machine_state->getRegValue(Register::DR0);
    auto dr1_value = machine_state->getRegValue(Register::DR1);
    auto dr2_value = machine_state->getRegValue(Register::DR2);
    auto dr3_value = machine_state->getRegValue(Register::DR3);
    auto dr7_value = machine_state->getRegValue(Register::DR7);
    if ((cnt != bb->getSize() - 1) && dr7_value.has_value())
    {
      // DR0
      if (dr0_value.has_value() && (*dr0_value == instr->getAddr()) &&
          (*dr7_value & 0x00000003U) && !(*dr7_value & 0x00030000U))
      {
        log->warn("Debug control register DR0 breakpoint --> EXCEPTION_SINGLE_STEP");
        new_bb_ep = instr->getNextAddr();
        exception_context_t exception_context = {instr.get(), 0x7cccccccU, std::nullopt, std::nullopt, Exception::SINGLE_STEP};
        machine_state->setExceptionContext(exception_context);
        machine_state->setRegValue(Register::DR6, 0x00000001U);
        return;
      }
      // DR1
      if (dr1_value.has_value() && (*dr1_value == instr->getAddr()) &&
          (*dr7_value & 0x0000000cU) && !(*dr7_value & 0x000c0000U))
      {
        log->warn("Debug control register DR1 breakpoint --> EXCEPTION_SINGLE_STEP");
        new_bb_ep = instr->getNextAddr();
        exception_context_t exception_context = {instr.get(), 0x7cccccccU, std::nullopt, std::nullopt, Exception::SINGLE_STEP};
        machine_state->setExceptionContext(exception_context);
        machine_state->setRegValue(Register::DR6, 0x00000002U);
        return;
      }
      // DR2
      if (dr2_value.has_value() && (*dr2_value == instr->getAddr()) &&
          (*dr7_value & 0x00000030U) && !(*dr7_value & 0x00300000U))
      {
        log->warn("Debug control register DR2 breakpoint --> EXCEPTION_SINGLE_STEP");
        new_bb_ep = instr->getNextAddr();
        exception_context_t exception_context = {instr.get(), 0x7cccccccU, std::nullopt, std::nullopt, Exception::SINGLE_STEP};
        machine_state->setExceptionContext(exception_context);
        machine_state->setRegValue(Register::DR6, 0x00000004U);
        return;
      }
      // DR3
      if (dr3_value.has_value() && (*dr3_value == instr->getAddr()) &&
          (*dr7_value & 0x000000c0U) && !(*dr7_value & 0x00c00000U))
      {
        log->warn("Debug control register DR3 breakpoint --> EXCEPTION_SINGLE_STEP");
        new_bb_ep = instr->getNextAddr();
        exception_context_t exception_context = {instr.get(), 0x7cccccccU, std::nullopt, std::nullopt, Exception::SINGLE_STEP};
        machine_state->setExceptionContext(exception_context);
        machine_state->setRegValue(Register::DR6, 0x00000008U);
        return;
      }
    }

    for(const auto& micro_instr : *instr->getMicroInstrs())
    {
      // Check for DIV exception
      if((cnt != bb->getSize() - 1) && (micro_instr->getType() == MicroInstrType::ASSERT))
      {
        log->info("Check if instruction \"{}\" triggers an exception ({})", instr->toStringLight(), micro_instr->getMnemonic());
        const auto& assert_value = conc_values->asserts.at(micro_instr.get());
        if(assert_value.has_value())
        {
          if(*assert_value == 1)
          {
            log->info("\tThis micro instruction does not trigger an exception");
          }
          else
          {
            log->warn("\tThis micro instruction triggers an exception");
            new_bb_ep = instr->getNextAddr();
            exception_context_t exception_context = {instr.get(), 0x7ccccccc, std::nullopt, std::nullopt,
                                                     Exception::INTEGER_DIVIDE_BY_ZERO};
            machine_state->setExceptionContext(exception_context);
            return;
          }
        }
        else
        {
          log->warn("\tAssert value is unknown, we can not check if this micro instruction triggers an exception");
        }
      }

      if(micro_instr->getMemRead().has_value())
      {
        const auto& mem_read = *micro_instr->getMemRead();
        const auto& mem_read_loc_result = conc_values->mem_read_locs.at(&mem_read);
        if(mem_read_loc_result.has_value())
        {
          for(unsigned int i = 0; i < mem_read.size; i++)
          {
            addr_t final_addr = *mem_read_loc_result + i;

            // Check for access violation
            const auto& mem_case_perms = machine_state->getMemCellPerms(final_addr);

            if(mem_case_perms.find(Permission::READ) == mem_case_perms.end())
            {
              if(cnt != bb->getSize() - 1)
              {
                log->warn("Instruction {} is reading an address ({:#x}) without READ permission --> "
                          "Access violation",
                          instr->toStringLight(), final_addr);
                new_bb_ep = instr->getNextAddr();
                exception_context_t exception_context = {instr.get(), 0x7ccccccc, std::nullopt, std::nullopt,
                                                         Exception::ACCESS_VIOLATION};
                machine_state->setExceptionContext(exception_context);
                return;
              }
            }
          }
        }
      }

      if(micro_instr->getMemWrite().has_value())
      {
        const auto& mem_write = *micro_instr->getMemWrite();
        const auto& mem_write_loc_result = conc_values->mem_write_locs.at(&mem_write);

        if(mem_write_loc_result.has_value())
        {
          // Monitor written addr for self modification detection
          for(unsigned int i = 0; i < mem_write.size; i++)
          {
            addr_t final_addr = *mem_write_loc_result + i;

            // Check for access violation
            const auto& mem_case_perms = machine_state->getMemCellPerms(final_addr);

            if(mem_case_perms.find(Permission::WRITE) == mem_case_perms.end())
            {
              if(cnt != bb->getSize() - 1)
              {
                log->warn("Instruction {} is modifying an address ({:#x}) without WRITE permission --> "
                          "Access violation",
                          instr->toStringLight(), final_addr);
                new_bb_ep = instr->getNextAddr();
                exception_context_t exception_context = {instr.get(), 0x7ccccccc, std::nullopt, std::nullopt,
                                                         Exception::ACCESS_VIOLATION};
                machine_state->setExceptionContext(exception_context);
                return;
              }
            }

            // Check for self-modif
            if(bb_instrs_bytes_addrs.find(final_addr) != bb_instrs_bytes_addrs.end())
            {
              unsigned int modified_instr_nbr = bb_instrs_bytes_addrs.at(final_addr);
              if(modified_instr_nbr > cnt)
              {
                log->warn("Instruction {} is modifying at least one byte of the instruction "
                          "n°{} of the BB itself",
                          instr->toStringLight(), modified_instr_nbr);
                new_bb_ep = bb->getInstrs().at(modified_instr_nbr)->getAddr();
                return;
              }
            }
          }
        }
      }
    }
    cnt++;
  }

  // Print results (mem read and write locs) and modify machine state
  for(const auto& instr : bb->getInstrs())
  {
    for(const auto& micro_instr : *instr->getMicroInstrs())
    {
      if(micro_instr->getMemRead().has_value())
      {
        SPDLOG_LOGGER_DEBUG(log, "Read operation by instr {}: ", instr->toStringLight());
        const auto& mem_read = *micro_instr->getMemRead();
        auto mem_read_loc_result = conc_values->mem_read_locs.at(&mem_read);

        if(mem_read_loc_result.has_value())
        {
          SPDLOG_LOGGER_DEBUG(log, "`--> ✅ Read @: {:#x}_{}", *mem_read_loc_result, mem_read.size);
        }
        else
        {
          SPDLOG_LOGGER_DEBUG(log, "`--> ❌ Read @: unknown");
        }
      }

      if(micro_instr->getMemWrite().has_value())
      {
        SPDLOG_LOGGER_DEBUG(log, "Write operation by instr {}: ", instr->toStringLight());
        const auto& mem_write = *micro_instr->getMemWrite();
        auto mem_write_loc_result = conc_values->mem_write_locs.at(&mem_write);

        if(mem_write_loc_result.has_value())
        {
          SPDLOG_LOGGER_DEBUG(log, "`--> ✅ Write @: {:#x}_{}", *mem_write_loc_result, mem_write.size);

          // Monitor written addr for self modification detection
          for(unsigned int i = 0; i < mem_write.size; i++)
          {
            addr_t final_addr = *mem_write_loc_result + i;
            machine_state->addWrittenAddrInHistory(final_addr);
          }
        }
        else
        {
          SPDLOG_LOGGER_DEBUG(log, "`--> ❌ Write @: unknown");
          s_write_at_unknown_location_cnt++;
          log->warn("At least a memory write operation at unknown location :-(");
        }
      }
    }
  }

  for(const auto& reg_p : conc_values->exit_regs)
  {
    machine_state->setRegValue(reg_p.first, reg_p.second);
    if(reg_p.second.has_value())
    {
      SPDLOG_LOGGER_DEBUG(log, "✅ {}_exit: {:#x}", reg2s(reg_p.first), *reg_p.second);
    }
    else
    {
      SPDLOG_LOGGER_DEBUG(log, "❌ {}_exit: unknown", reg2s(reg_p.first));
    }
  }

  // Print exit mem cases results
  for(auto exit_mem_case_state : conc_values->exit_mem_cases)
  {
    machine_state->setMemCellValue(exit_mem_case_state.first, exit_mem_case_state.second);

    if(exit_mem_case_state.second.has_value())
    {
      SPDLOG_LOGGER_DEBUG(log, "✅ @[{:#x}]_exit: {:#x}", exit_mem_case_state.first, *exit_mem_case_state.second);
    }
    else
    {
      SPDLOG_LOGGER_DEBUG(log, "❌ @[{:#x}]_exit: unknown", exit_mem_case_state.first);
    }
  }

  // RDTSC case
  static const std::vector<uint8_t> rdtsc_bytes = {0x0f, 0x31};
  if(last_instr->getBytes() == rdtsc_bytes)
  {
    static uint32_t tick = 0;
    log->warn("BB ends with RDTSC instr, simulation --> EAX_exit: {:#x}, EDX_exit: {:#x}", 0xdeadbbbe, tick);
    machine_state->setRegValue(Register::EAX, 0xdeadbbbe);
    machine_state->setRegValue(Register::EDX, tick);
    tick = tick + 1000;
  }

  // Int3 case
  if(last_instr->getType() == InstrType::TRAP && binary->getFormat() == BinaryFormat::PE)
  {
    log->warn("Instruction {} is a trap --> EXCEPTION_BREAKPOINT", last_instr->toStringLight());
    exception_context_t exception_context = {last_instr, 0x7ccccccc, std::nullopt, std::nullopt, Exception::BREAKPOINT};
    machine_state->setExceptionContext(exception_context);
  }

  // Check if last instr trigger an exception
  // If it's the case we have to simulate Windows procedure
  if(machine_state->getExceptionContext().has_value())
  {
    auto& exception_context = *machine_state->getExceptionContext();
    if(exception_context.trigger_instr == last_instr)
    {
      log->warn("Last instr ({}) trigger an exception, we need to simulate Windows", last_instr->toStringLight());
      utils::pushMessage("[" + waddr2s(last_instr->getWaddr()) + "] Instruction " + last_instr->toStringLight() +
                         " trigger an exception of type " + exc2s(exception_context.exception_type));
      ExceptionEntryHook(machine_state, log);
    }
  }

  // SPDLOG_LOGGER_DEBUG(log, "[-] Exit machine state valuation computed");

  return;
}

int MachineStateSymbExec::getWriteAtUnknownLocationCnt()
{
  return s_write_at_unknown_location_cnt;
}

} // namespace boa
