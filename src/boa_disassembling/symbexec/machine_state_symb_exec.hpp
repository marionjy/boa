#ifndef MACHINE_STATE_SYMB_EXEC_H
#define MACHINE_STATE_SYMB_EXEC_H

#include <deque>
#include <stdio.h>

#include "boa_disassembling/exec_formula.hpp"
#include "boa_disassembling/machine_state.hpp"
#include "boa_disassembling/solver/solver_manager.hpp"

namespace boa
{

enum class CallConv
{
  STDCALL,
  CDECL,
  VARARGS,
  THISCALL
};

class MachineStateSymbExec
{
public:
  static void ParseWindowsDllsJson(const std::string& windows_dlls_json_filepath);

  static void ExceptionExitHook(MachineState* machine_state, std::shared_ptr<spdlog::logger> log);

  static void get(ExecFormula& exec_formula, std::shared_ptr<spdlog::logger> log, SolverManager& solver_manager,
                  Binary* binary, std::optional<addr_t>& new_bb_ep, const std::string& windows_dlls_json_filepath,
                  bool disable_lib_fun_hooks, bool load_dlls);

  static void ExceptionEntryHook(MachineState* machine_state, std::shared_ptr<spdlog::logger> log);

  static int getWriteAtUnknownLocationCnt();

private:
  static void ExternalLibFuncHook(MachineState* machine_state, Binary* binary, const Instr* instr,
                                  const std::string& windows_dlls_json_filepath, std::shared_ptr<spdlog::logger> log,
                                  bool disable_lib_fun_hooks, bool load_dlls);

  static void repHack(MachineState* machine_state, const BasicBlock* const bb, std::shared_ptr<spdlog::logger> log);

  static int s_write_at_unknown_location_cnt;

  static std::map<std::string, std::pair<CallConv, unsigned int>> s_external_lib_func;
};

} // namespace boa

#endif /* MACHINE_STATE_SYMB_EXEC_H */
