#include "solver_manager.hpp"

#include <assert.h>
#include <fstream>
#include <iostream>
#include <map>
#include <vector>

namespace boa
{

// MARK:- Constructors and destructors
SolverManager::SolverManager(const config_solver_t& config_solver, bool incremental_mode, unsigned int solvers_to_start)
    : m_log(spdlog::get(utils::solver_logger)), m_config_solver(config_solver),
      m_number_of_get_symb_var_valuation_with_solvers(0), m_number_of_get_symb_var_valuation_with_saved_results(0),
      m_check_sat_cnt(0), m_get_value_cnt(0)
{
  // Check if solver is in the PATH
  std::string solver_s_lower = Solver::solverToString(config_solver.smt_solver);
  std::transform(solver_s_lower.begin(), solver_s_lower.end(), solver_s_lower.begin(), tolower);
  if(utils::exec_shell_command("which " + solver_s_lower).first != 0)
  {
    throw std::runtime_error("Solver '" + solver_s_lower +
                             "' is specified in '--solver' argument but this solver is not found in your PATH");
  }

  // signal(SIGCHLD, SIG_IGN);
  for(unsigned int i = 0; i < solvers_to_start; i++)
  {
    // Create Solver instances
    std::unique_ptr<Solver> solver =
        std::make_unique<Solver>(config_solver.smt_solver, i, /*config_solvers.timeout,*/ incremental_mode);

    // Start solver
    solver->startProcess();
    solver->setReady(true);

    m_solvers.push_back(std::move(solver));
  }
}

SolverManager::~SolverManager()
{
  // We need to kill all running solvers
  bool still_one_solver_running = true;
  while(still_one_solver_running)
  {
    still_one_solver_running = false;
    for(const auto& solver : m_solvers)
    {
      if(solver->isReady())
      {
        still_one_solver_running = true;
        solver->sendString(smtlib_utils::smt_exit());
        solver->stopProcess();
        solver->setReady(false);
      }
    }
  }
}

// MARK:- Solver management private functions

void SolverManager::restartSolver(Solver* solver)
{
  SPDLOG_LOGGER_DEBUG(m_log, "Restart solver n° {}", solver->getId());

  // Je demande d'abbord au solver de s'arreter lui-meme
  solver->sendString(smtlib_utils::smt_exit());

  // Je Kill le solver
  solver->stopProcess();

  // Je restart le solver
  solver->startProcess();

  // Je le déclare comme ready
  solver->setReady(true);

  SPDLOG_LOGGER_DEBUG(m_log, "Solver n° {} is now ready", solver->getId());
}

// MARK:- Symbolic variable valuation engine public functions
void SolverManager::getSymbVarsSatValue(std::unordered_map<std::string, uint32_t>& symb_vars_sat_value,
                                        const std::string& smtlib_formula_text)
{

  SPDLOG_LOGGER_DEBUG(m_log, "Enter in getSymbVarsValuation()");

  // If there is nothing to compute, just return
  if(symb_vars_sat_value.empty())
  {
    SPDLOG_LOGGER_DEBUG(m_log, "Exit of getSymbVarsValuation() (1)");
    return;
  }

  // Get solver instance
  Solver* solver = m_solvers.at(0).get();
  unsigned int solver_id = solver->getId();
  SolverStatus solver_final_status;

  SPDLOG_LOGGER_DEBUG(m_log, "Start solver computation on solver n° {}", solver_id);
  solver->getSymbVarsValuation(smtlib_formula_text, symb_vars_sat_value, solver_final_status, m_check_sat_cnt,
                               m_get_value_cnt);
  SPDLOG_LOGGER_DEBUG(m_log, "Solver computation done");

  // Good case
  if(solver_final_status == SolverStatus::SUCCESS)
  {
    // Reset solver for the next usage
    solver->sendString(smtlib_utils::smt_reset());

    m_number_of_get_symb_var_valuation_with_solvers += symb_vars_sat_value.size();
  }

  // Something wrong occured
  else
  {
    // Competely restart solver in this case
    restartSolver(solver);
    throw std::runtime_error("Solver n° " + std::to_string(solver_id) +
                             " ended with FAIL status, that should not happen. Formula:\n" + smtlib_formula_text);
  }

  SPDLOG_LOGGER_DEBUG(m_log, "Exit of getSymbVarsValuation() (2)");
}

void SolverManager::getUniqueSymbVarValuation(
    std::pair<std::string, std::optional<uint32_t>>& symb_var_unique_sat_value, const std::string& smtlib_formula_text,
    unsigned int symb_var_size, unsigned int solver_id_to_use)
{

  SPDLOG_LOGGER_DEBUG(m_log, "Enter in getUniqueSymbVarValuation()");

  Solver* solver = m_solvers.at(solver_id_to_use).get();
  SolverStatus solver_final_status;

  solver->getUniqueSymbVarValuation(smtlib_formula_text, symb_var_unique_sat_value, symb_var_size, solver_final_status,
                                    m_check_sat_cnt, m_get_value_cnt);

  // Good case
  if(solver_final_status == SolverStatus::SUCCESS)
  {
    // Reset solver for the next usage
    solver->sendString(smtlib_utils::smt_reset());
    // Stat
    m_number_of_get_symb_var_valuation_with_solvers++;
  }
  else
  {
    // Competely restart solver in this case
    restartSolver(solver);
    throw std::runtime_error("Solver n° " + std::to_string(solver->getId()) +
                             " ended with FAIL status, that should not happen. Formula:\n" + smtlib_formula_text);
  }

  SPDLOG_LOGGER_DEBUG(m_log, "Exit of getUniqueSymbVarValuation()");
}

// MARK: - Statistics
int SolverManager::getNumberGetSymVarValuationWithSolvers() const
{
  return m_number_of_get_symb_var_valuation_with_solvers;
}

int SolverManager::getNumberGetSymVarValuationWithSavedResults() const
{
  return m_number_of_get_symb_var_valuation_with_saved_results;
}

int SolverManager::getNumberCheckSat() const
{
  return m_check_sat_cnt;
}

int SolverManager::getNumberGetValue() const
{
  return m_get_value_cnt;
}

} // namespace boa
