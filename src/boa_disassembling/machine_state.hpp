#ifndef MACHINE_STATE_H
#define MACHINE_STATE_H

#include <iostream>
#include <map>
#include <stdio.h>
#include <unordered_map>
#include <vector>

#include "binary/binary_parser.hpp"
#include "common/basic_block.hpp"
#include "utils/boa_types.hpp"

namespace boa
{

struct VectorAddrHasher
{
  size_t operator()(const std::vector<addr_t>& s) const
  {
    size_t hash = 0;
    for(unsigned int i = 0; i < s.size(); i++)
    {
      hash = hash ^ s[i];
    }
    return hash;
  }
};

struct exception_context_t
{
  const Instr* trigger_instr;
  addr_t fake_kernel_return_address;
  std::optional<addr_t> handler_entry_addr;
  std::optional<addr_t> handler_exit_target;
  Exception exception_type;
};

struct dll_t
{
  addr_t handle; // vaddr where this DLL was loaded
  std::shared_ptr<Binary> binary; // If we have the DLL file, else nullptr
  std::unordered_map<std::string, addr_t> exported_functions = {}; // vaddr of each exported functions EP (only used when we do not have the DLL file. If we have the DLL file, use export_entries from Binary object)
};

class MachineState
{
public:
  // MARK:- Constructors and destructors
  MachineState(Binary* binary, addr_t instr_ptr, BinaryParser* bin_parser = nullptr, const std::string& windows_dlls_dir = {});

  MachineState(MachineState const&) = default;
  MachineState& operator=(MachineState const&) = delete;

  MachineState(MachineState&&) = default;
  MachineState& operator=(MachineState&&) = delete;

  ~MachineState() = default;

  // MARK:- Getters and setters

  // Written addrs history
  const std::unordered_set<addr_t>& getWrittenAddrsHistory() const;
  void clearWrittenAddrsHistory();
  void addWrittenAddrInHistory(addr_t addr);

  // Registers state
  const std::unordered_map<Register, std::optional<uint32_t>>& getRegValues() const;
  std::optional<uint32_t> getRegValue(Register reg) const;
  void setRegValue(Register reg, std::optional<uint32_t> value);
  void setRegValueFromMemDword(Register reg, addr_t mem_case, bool use_binary_value_if_bottom);

  // Mem cases state
  const std::unordered_map<addr_t, std::optional<uint8_t>>& getMemCellValues() const;
  std::optional<uint8_t> getMemCellValue(addr_t mem_case, bool use_binary_value_if_bottom) const;
  void setMemCellValue(addr_t mem_case, uint8_t value);
  void setMemCellValue(addr_t mem_case, std::optional<uint8_t> value);
  void setMemCellDwordValue(addr_t mem_case, uint32_t value);
  void setMemCellDwordValue(addr_t mem_case, std::optional<uint32_t> value);
  void setMemCellWordValue(addr_t mem_case, uint16_t value);
  std::optional<uint16_t> getMemWord(addr_t addr, bool use_binary_value_if_bottom) const;
  std::optional<uint32_t> getMemDword(addr_t addr, bool use_binary_value_if_bottom) const;
  std::optional<std::string> getAsciiText(addr_t addr, bool use_binary_value_if_bottom) const;
  std::optional<std::string> getUnicodeText(addr_t addr, bool use_binary_value_if_bottom) const;
  bool isMemCellConc(addr_t addr, bool use_binary_value_if_bottom) const;
  bool areMemCellsConc(addr_t first_addr, unsigned int size, bool use_binary_value_if_bottom) const;

  // Mem cases perms
  const std::set<Permission>& getMemCellPerms(addr_t mem_case) const;
  void setMemoryPermsRange(addr_t first_addr, addr_t last_addr, const std::set<Permission>& perms);

  // Exception context
  void setExceptionContext(const exception_context_t& exception_context);
  std::optional<exception_context_t>& getExceptionContext();
  void clearExceptionContext();

  // Virtual alloc ptr
  addr_t getVirtualAllocPtr() const;
  void setVirtualAllocPtr(addr_t v);

  // Heap ptr
  addr_t getHeapPtr() const;
  void setHeapPtr(addr_t v);
  
  // Tls stuff
  uint32_t getTlsSlot() const;
  void setTlsSlot(uint32_t v);

  // Dynamically loaded modules
  void linkDll(const std::string& lib, bool load_dlls);
  void patchIat(const binary::import_entry_t& import_entry);

  // Instruction pointer
  addr_t getInstrPtr() const;
  void setInstrPtr(addr_t instr_ptr);

  // Returnsite stack
  const std::vector<addr_t>& getReturnsiteStack() const;
  void pushReturnsite(addr_t returnsite);
  void popReturnsiteIfNeeded(addr_t returnsite);

  // MARK:- Other functions
  addr_t getStackStartAddr() const;
  const std::unordered_map<std::string, dll_t>& getLoadedDlls() const;
  dll_t* getLoadedDllAt(addr_t module_handle);
  
  void setInitMachineState(BinaryFormat bin_format, bool load_dlls, const std::unordered_map<Register, uint32_t>& user_reg_values = {}, const std::unordered_map<addr_t, uint8_t>& user_mem_cell_values = {});
  std::string toString() const;
  std::string toStringCompare(const MachineState& other_ms) const;
  bool mergeWithMachineState(const MachineState& machine_state_to_be_merged);
  bool isEquivalent(const MachineState& ms) const;
  void dumpMemInFile(const std::string& filepath) const;

private:
  // MARK:- Private member variables

  std::shared_ptr<spdlog::logger> m_log;

  // Keep the value of each CPU register of the machine (is value is std::nullopt, register value is TOP)
  std::unordered_map<Register, std::optional<uint32_t>> m_reg_values;

  // Keep the value of each memory cell of the machine (is value is std::nullopt, memory cell value is TOP)
  std::unordered_map<addr_t, std::optional<uint8_t>> m_mem_cell_values;

  // Ce map décrit les différentes permissions (Read, Write, eXecute) sur l'ensemble de la mémoire
  // Il fonctionne par range, par exemple, si dans le map on a:
  // {0x0, RW}
  // {0x4, R}
  // {0x9, W}
  // Alors on a les permissions suivantes :
  // 0x0, 0x1, 0x2 et 0x3 sont en RW
  // 0x4, 0x5, 0x6, 0x7 et 0x8 sont en R
  // 0x9, 0xA, ... 0xFFFFFFFF sont en W
  std::map<addr_t, std::set<Permission>> m_memory_perms_range;

  // Each time a memory cell is written by an instruction we keep this memory cell vaddr in this set
  // This set is used to detect self modification
  std::unordered_set<addr_t> m_written_addrs_history;

  // If an exception was triggered, all informations are here
  // During a normal execution, this member is equal to std::nullopt
  std::optional<exception_context_t> m_current_exception_context;

  // Load-time and run-time dynamically loaded modules (PE only ATM)
  addr_t m_dlls_loading_ptr; // Always contains the address where to load the next DLL
  std::unordered_map<std::string, dll_t> m_loaded_dlls; // Key: DLL name
  
  // TlsAlloc, TlsGetValue, ...
  uint32_t m_tls_slot;

  // Simulate the instruction pointer register
  // Contains the next instruction address to execute
  addr_t m_instr_ptr;

  // Returnsite stack to detect CST
  std::vector<addr_t> m_returnsite_stack;

  // Information about the initial machine state
  addr_t m_stack_start_addr;
  addr_t m_heap_ptr_addr;
  addr_t m_virtual_alloc_ptr_addr; // allocation with VirtualAlloc (Windows only?)

  // Used to retrieve binary bytes on the fly
  Binary* m_binary;

  BinaryParser* m_bin_parser;

  const std::string& m_windows_dlls_dir;
};

} // namespace boa

#endif /* MACHINE_STATE_H */
