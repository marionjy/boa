#include "boa_disassembling.hpp"

#include <assert.h>
#include <deque>

namespace boa
{

// MARK:- Constructors and destructors
BoaDisassemblingWave::BoaDisassemblingWave(Disassembler* disassembler, Binary* binary, x86ToSmtlib* x86_to_smtlib,
                                           SolverManager& solver_manager,
                                           const config_boa_disassembling_t& config_boa_disassembling,
                                           const config_global_t& config_global, unsigned int wave)
    : m_disassembler(disassembler), m_binary(binary), m_x86_to_smtlib(x86_to_smtlib), m_solver_manager(solver_manager),
      m_config_boa_disassembling(config_boa_disassembling), m_config_global(config_global),
      m_log(spdlog::get(utils::boa_disas_logger)), m_wave(wave), m_symb_executed_bbs_cnt(0),
      m_symb_executed_instrs_cnt(0)
{
  SPDLOG_LOGGER_TRACE(m_log, "BoaDisassemblingWave::BoaDisassemblingWave()");
}

BoaDisassemblingWave::~BoaDisassemblingWave()
{
  SPDLOG_LOGGER_TRACE(m_log, "BoaDisassemblingWave::~BoaDisassemblingWave()");
}


// MARK:- Rets results getters
std::map<const Instr*, std::string> BoaDisassemblingWave::getRetsGenuine() const
{
  std::map<const Instr*, std::string> results{};
  for(const auto& ret_pair : m_rets_results)
  {
    if(ret_pair.second.seen_genuine > 0 && ret_pair.second.seen_violated == 0 && ret_pair.second.seen_unknown == 0)
    {
      std::string targets = waddrs2s(ret_pair.first->getEdgesSet());
      results.insert({ret_pair.first, "Genuine (" + targets + ")"});
    }
  }
  return results;
}

std::map<const Instr*, std::string> BoaDisassemblingWave::getRetsViolated() const
{
  std::map<const Instr*, std::string> results{};
  for(const auto& ret_pair : m_rets_results)
  {
    if(ret_pair.second.seen_genuine == 0 && ret_pair.second.seen_violated > 0 && ret_pair.second.seen_unknown == 0)
    {
      std::string targets = waddrs2s(ret_pair.first->getEdgesSet());
      results.insert({ret_pair.first, "Violated (" + targets + ")"});
    }
  }
  return results;
}

std::map<const Instr*, std::string> BoaDisassemblingWave::getRetsPartiallyUnknown() const
{
  std::map<const Instr*, std::string> results{};
  for(const auto& ret_pair : m_rets_results)
  {
    if(ret_pair.second.seen_unknown > 0 && !ret_pair.first->getEdgesSet().empty())
    {
      std::string targets = waddrs2s(ret_pair.first->getEdgesSet());
      results.insert({ret_pair.first, "Partially unknown (" + targets + ")"});
    }
  }
  return results;
}

std::map<const Instr*, std::string> BoaDisassemblingWave::getRetsUnknown() const
{
  std::map<const Instr*, std::string> results{};
  for(const auto& ret_pair : m_rets_results)
  {
    if(ret_pair.second.seen_unknown > 0 && ret_pair.first->getEdgesSet().empty())
    {
      std::string targets = waddrs2s(ret_pair.first->getEdgesSet());
      results.insert({ret_pair.first, "Unknown (" + targets + ")"});
    }
  }
  return results;
}

std::string BoaDisassemblingWave::getRetsResults() const
{
  return std::to_string(getRetsGenuine().size()) + "/" + std::to_string(getRetsViolated().size()) + "/" +
         std::to_string(getRetsPartiallyUnknown().size()) + "/" + std::to_string(getRetsUnknown().size());
}

std::map<const Instr*, std::string> BoaDisassemblingWave::getRetsResultsDetails() const
{
  std::map<const Instr*, std::string> results{};
  for(const auto& ret_pair : getRetsGenuine())
  {
    results.insert(ret_pair);
  }
  for(const auto& ret_pair : getRetsViolated())
  {
    results.insert(ret_pair);
  }
  for(const auto& ret_pair : getRetsPartiallyUnknown())
  {
    results.insert(ret_pair);
  }
  for(const auto& ret_pair : getRetsUnknown())
  {
    results.insert(ret_pair);
  }
  return results;
}

// MARK:- DynJmps results getters
std::map<const Instr*, std::string> BoaDisassemblingWave::getDynJmpsConcrete() const
{
  std::map<const Instr*, std::string> results{};
  for(const auto& dyn_jmp_pair : m_dyn_jmps_results)
  {
    if(dyn_jmp_pair.second.seen_concrete > 0 && dyn_jmp_pair.second.seen_unknown == 0)
    {
      std::string targets = waddrs2s(dyn_jmp_pair.first->getEdgesSet());
      results.insert({dyn_jmp_pair.first, "Concrete (" + targets + ")"});
    }
  }
  return results;
}

std::map<const Instr*, std::string> BoaDisassemblingWave::getDynJmpsPartiallyUnknown() const
{
  std::map<const Instr*, std::string> results{};
  for(const auto& dyn_jmp_pair : m_dyn_jmps_results)
  {
    if(dyn_jmp_pair.second.seen_unknown > 0 && dyn_jmp_pair.second.seen_concrete > 0)
    {
      std::string targets = waddrs2s(dyn_jmp_pair.first->getEdgesSet());
      results.insert({dyn_jmp_pair.first, "Partially unknown (" + targets + ")"});
    }
  }
  return results;
}

std::map<const Instr*, std::string> BoaDisassemblingWave::getDynJmpsUnknown() const
{
  std::map<const Instr*, std::string> results{};
  for(const auto& dyn_jmp_pair : m_dyn_jmps_results)
  {
    if(dyn_jmp_pair.second.seen_concrete == 0 && dyn_jmp_pair.second.seen_unknown > 0)
    {
      std::string targets = waddrs2s(dyn_jmp_pair.first->getEdgesSet());
      results.insert({dyn_jmp_pair.first, "Unknown (" + targets + ")"});
    }
  }
  return results;
}

std::string BoaDisassemblingWave::getDynJmpsResults() const
{
  return std::to_string(getDynJmpsConcrete().size()) + "/" + std::to_string(getDynJmpsPartiallyUnknown().size()) + "/" +
         std::to_string(getDynJmpsUnknown().size());
}

std::map<const Instr*, std::string> BoaDisassemblingWave::getDynJmpsResultsDetails() const
{
  std::map<const Instr*, std::string> results{};
  for(const auto& dyn_jmp_pair : getDynJmpsConcrete())
  {
    results.insert(dyn_jmp_pair);
  }
  for(const auto& dyn_jmp_pair : getDynJmpsPartiallyUnknown())
  {
    results.insert(dyn_jmp_pair);
  }
  for(const auto& dyn_jmp_pair : getDynJmpsUnknown())
  {
    results.insert(dyn_jmp_pair);
  }
  return results;
}

// MARK:- DynCalls results getters
std::map<const Instr*, std::string> BoaDisassemblingWave::getDynCallsConcrete() const
{
  std::map<const Instr*, std::string> results{};
  for(const auto& dyn_call_pair : m_dyn_calls_results)
  {
    if(dyn_call_pair.second.seen_concrete > 0 && dyn_call_pair.second.seen_unknown == 0)
    {
      std::string targets = waddrs2s(dyn_call_pair.first->getEdgesSet());
      results.insert({dyn_call_pair.first, "Concrete (" + targets + ")"});
    }
  }
  return results;
}

std::map<const Instr*, std::string> BoaDisassemblingWave::getDynCallsPartiallyUnknown() const
{
  std::map<const Instr*, std::string> results{};
  for(const auto& dyn_call_pair : m_dyn_calls_results)
  {
    if(dyn_call_pair.second.seen_unknown > 0 && dyn_call_pair.second.seen_concrete > 0)
    {
      std::string targets = waddrs2s(dyn_call_pair.first->getEdgesSet());
      results.insert({dyn_call_pair.first, "Partially unknown (" + targets + ")"});
    }
  }
  return results;
}

std::map<const Instr*, std::string> BoaDisassemblingWave::getDynCallsUnknown() const
{
  std::map<const Instr*, std::string> results{};
  for(const auto& dyn_call_pair : m_dyn_calls_results)
  {
    if(dyn_call_pair.second.seen_concrete == 0 && dyn_call_pair.second.seen_unknown > 0)
    {
      std::string targets = waddrs2s(dyn_call_pair.first->getEdgesSet());
      results.insert({dyn_call_pair.first, "Unknown (" + targets + ")"});
    }
  }
  return results;
}

std::string BoaDisassemblingWave::getDynCallsResults() const
{
  return std::to_string(getDynCallsConcrete().size()) + "/" + std::to_string(getDynCallsPartiallyUnknown().size()) +
         "/" + std::to_string(getDynCallsUnknown().size());
}

std::map<const Instr*, std::string> BoaDisassemblingWave::getDynCallsResultsDetails() const
{
  std::map<const Instr*, std::string> results{};
  for(const auto& dyn_call_pair : getDynCallsConcrete())
  {
    results.insert(dyn_call_pair);
  }
  for(const auto& dyn_call_pair : getDynCallsPartiallyUnknown())
  {
    results.insert(dyn_call_pair);
  }
  for(const auto& dyn_call_pair : getDynCallsUnknown())
  {
    results.insert(dyn_call_pair);
  }
  return results;
}

// MARK:- Jcc results getters
std::set<const Instr*> BoaDisassemblingWave::getJccsOnlyTarget() const
{
  std::set<const Instr*> jccs{};
  for(const auto& jcc_pair : m_jccs_results)
  {
    if(jcc_pair.second.seen_only_target > 0 && jcc_pair.second.seen_only_next_mem == 0 &&
       jcc_pair.second.seen_both_branches == 0)
    {
      jccs.insert(jcc_pair.first);
    }
  }
  return jccs;
}

std::set<const Instr*> BoaDisassemblingWave::getJccsOnlyNextMemAddr() const
{
  std::set<const Instr*> jccs{};
  for(const auto& jcc_pair : m_jccs_results)
  {
    if(jcc_pair.second.seen_only_target == 0 && jcc_pair.second.seen_only_next_mem > 0 &&
       jcc_pair.second.seen_both_branches == 0)
    {
      jccs.insert(jcc_pair.first);
    }
  }
  return jccs;
}

std::set<const Instr*> BoaDisassemblingWave::getJccsBothBranches() const
{
  std::set<const Instr*> jccs{};
  for(const auto& jcc_pair : m_jccs_results)
  {
    if(jcc_pair.second.seen_both_branches > 0 ||
       (jcc_pair.second.seen_only_next_mem > 0 && jcc_pair.second.seen_only_target > 0))
    {
      jccs.insert(jcc_pair.first);
    }
  }
  return jccs;
}

std::string BoaDisassemblingWave::getJccsResults() const
{
  return std::to_string(getJccsBothBranches().size()) + "/" + std::to_string(getJccsOnlyTarget().size()) + "/" +
         std::to_string(getJccsOnlyNextMemAddr().size());
}

std::map<const Instr*, std::string> BoaDisassemblingWave::getJccsResultsDetails() const
{
  std::map<const Instr*, std::string> results{};

  for(const auto& jcc : getJccsOnlyTarget())
  {
    results.insert({jcc, "Only target branch"});
  }
  for(const auto& jcc : getJccsOnlyNextMemAddr())
  {
    results.insert({jcc, "Only next mem branch"});
  }
  for(const auto& jcc : getJccsBothBranches())
  {
    results.insert({jcc, "Both branches"});
  }
  return results;
}

// MARK:- Symb exec stats getters
size_t BoaDisassemblingWave::getSymbExecutedBbsCnt() const
{
  return m_symb_executed_bbs_cnt;
}

size_t BoaDisassemblingWave::getSymbExecutedInstrsCnt() const
{
  return m_symb_executed_instrs_cnt;
}

long long BoaDisassemblingWave::getTotalTime() const
{
  return m_total_time;
}

// MARK:- Private methods

int BoaDisassemblingWave::loopChecker(const std::vector<addr_t>& returnsite_stack, addr_t bb_ep, addr_t target_addr)
{
  if(m_exec_flow_history.find(returnsite_stack) == m_exec_flow_history.end())
  {
    m_exec_flow_history[returnsite_stack] = std::unordered_map<
    addr_t,
    std::unordered_map<
      addr_t,
      loop_handler_t>
    >();
  }
  auto& exec_flow_history_context = m_exec_flow_history[returnsite_stack];
  
  if(exec_flow_history_context.find(bb_ep) == exec_flow_history_context.end())
  {
    exec_flow_history_context[bb_ep] = std::unordered_map<
    addr_t,
    loop_handler_t>();
  }
  auto& exec_flow_history_context_bb_ep = exec_flow_history_context[bb_ep];
  
  if(exec_flow_history_context_bb_ep.find(target_addr) == exec_flow_history_context_bb_ep.end())
  {
    exec_flow_history_context_bb_ep[target_addr] = {0, nullptr};
  }
  
  int loop_cnt = exec_flow_history_context_bb_ep[target_addr].cnt;
  if(loop_cnt == 0)
  {
    m_log->info("🔄 Target {:#x} was never taken from current basic block with this context", target_addr);
  }
  else
  {
    m_log->info("🔄 Target {:#x} was already taken {} times from current basic block with this context", target_addr, loop_cnt);
  }
  return loop_cnt;
}

std::unique_ptr<exploration_path_t> BoaDisassemblingWave::deepCopyExplorationPath(exploration_path_t* exploration_path) const
{
  std::unique_ptr<exploration_path_t> new_exploration_path = std::make_unique<exploration_path_t>(
  exploration_path_t{std::make_unique<MachineState>(*exploration_path->machine_state), exploration_path->last_instr,
                     exploration_path->found_method});
  return new_exploration_path;
}

void BoaDisassemblingWave::getSuccsToTreat(std::vector<std::unique_ptr<exploration_path_t>>& succs_to_treat,
                                           std::unique_ptr<exploration_path_t> exploration_path,
                                           const BasicBlock* const bb, ExecFormula& bb_exit_formula)
{

  const Instr* const last_instr = bb->getLastInstr();
  MachineState* const machine_state = exploration_path->machine_state.get();

  // Check if we have to handle an fresh exception
  if(machine_state->getExceptionContext().has_value())
  {
    auto exception_context = *machine_state->getExceptionContext();
    if(exception_context.trigger_instr == last_instr)
    {
      if(exception_context.handler_entry_addr.has_value())
      {
        m_log->warn("Last instr ({}) triggered an exception, handler entry: {:#x}", last_instr->toStringLight(),
                    *exception_context.handler_entry_addr);
        machine_state->setInstrPtr(*exception_context.handler_entry_addr);
        exploration_path->found_method = EdgeFoundMethod::BOA_DISAS;
        succs_to_treat.push_back(std::move(exploration_path));
      }
      else
      {
        m_log->warn("Last instr ({}) trigger an exception but we don't know the handler to execute :-/",
                    last_instr->toStringLight());
      }
      return;
    }
  }

  // Else, handle this BB based on the last instruction type

  // SEQ
  if(last_instr->getType() == InstrType::SEQ)
  {
    static const std::vector<uint8_t> zeros_bytes = {0x00, 0x00};
    if(bb->getInstrs().at(0)->getBytes() == zeros_bytes && bb->getLastInstr()->getBytes() == zeros_bytes)
    {
      m_log->warn("This BB is only zeros?, stop this path");
    }
    else
    {
      SPDLOG_LOGGER_DEBUG(m_log, "Last instr type: SEQ ({})", last_instr->toStringLight());
      machine_state->setInstrPtr(last_instr->getNextAddr());
      exploration_path->found_method = EdgeFoundMethod::STATIC_DISAS;
      succs_to_treat.push_back(std::move(exploration_path));
    }
  }

  // JCC
  else if(last_instr->getType() == InstrType::JCC)
  {
    SPDLOG_LOGGER_DEBUG(m_log, "Last instr type: JCC ({})", last_instr->toStringLight());

    // Check if with this formula this Jcc condition is True, False or Unknown
    JccSymbExec jcc = JccSymbExec(bb_exit_formula, m_solver_manager, m_binary);
    const addr_t bb_ep = bb->getEntryPoint().second;
    const auto& returnsite_stack = exploration_path->machine_state->getReturnsiteStack();

    if(jcc.getCondStatus() == JccCondStatus::UNKNOWN)
    {
      m_log->info("❌ {} Jcc condition value: unknown", ExecFormula::resultSrcToString(jcc.getResultSrc()));
      m_jccs_results[last_instr].seen_both_branches++;

      // Quand on a un status UNKNOWN, meme si on est configuré en max_loop_lap = -1 il faut quand meme faire un check de loop pour éviter que BOA boucle à l'infinie
      // D'ailleur on attend meme pas d'atteindre le nombre de tour max, c'est une perte de temps dans ce cas là car de toute façon la condition est inconnue ...
      
      std::vector<std::tuple<addr_t, int, std::unique_ptr<exploration_path_t>>> branches_to_take;
      
      int loop_cnt_next_addr = loopChecker(returnsite_stack, bb_ep, last_instr->getNextAddr());
      int loop_cnt_target = loopChecker(returnsite_stack, bb_ep, *last_instr->getJmpAddr());
      
      // Need to do a deep copy of exploration path :-/
      std::unique_ptr<exploration_path_t> new_exploration_path = deepCopyExplorationPath(exploration_path.get());
      
      if(loop_cnt_target > loop_cnt_next_addr)
      {
        branches_to_take.push_back(std::make_tuple(last_instr->getNextAddr(), loop_cnt_next_addr, std::move(exploration_path)));
        branches_to_take.push_back(std::make_tuple(*last_instr->getJmpAddr(), loop_cnt_target, std::move(new_exploration_path)));
      }
      else
      {
        branches_to_take.push_back(std::make_tuple(*last_instr->getJmpAddr(), loop_cnt_target, std::move(new_exploration_path)));
        branches_to_take.push_back(std::make_tuple(last_instr->getNextAddr(), loop_cnt_next_addr, std::move(exploration_path)));
      }

      // Maintenant je fais le nécessaire pour chaque branche
      for(auto& taken_branch : branches_to_take)
      {
        addr_t target_addr = std::get<0>(taken_branch);
        int loop_cnt = std::get<1>(taken_branch);
        std::unique_ptr<exploration_path_t> branch_exploration_path = std::move(std::get<2>(taken_branch));
        bool add_this_target = true;
        
        branch_exploration_path->machine_state->setInstrPtr(target_addr);
        branch_exploration_path->found_method = EdgeFoundMethod::BOA_DISAS;
        
        auto& exec_flow_target = m_exec_flow_history[returnsite_stack][bb_ep][target_addr];
        
        // Si ce target n'engendre pas de boucle, on ne fait rien de particulier
        // Sinon il faut voir
        if(loop_cnt != 0)
        {
          if(exec_flow_target.machine_state == nullptr)
          {
            m_log->info("🔄 No saved MachineState --> Save this MachineState for later");
            std::unique_ptr<MachineState> machine_state_copy = std::make_unique<MachineState>(*branch_exploration_path->machine_state);
            exec_flow_target.machine_state = std::move(machine_state_copy);
          }
          else
          {
            m_log->info("🔄 We will loop if we take target {:#x} --> Compare current MachineState with saved one", target_addr);
            
            // On merge sur le MS saved (comme ça c'est lui qui perd de l'info)
            bool at_least_one_change = exec_flow_target.machine_state->mergeWithMachineState(*branch_exploration_path->machine_state);
            if(at_least_one_change)
            {
              m_log->info("🔄\tAlready took the target {:#x} but not with the same exit_MS --> merge "
                                        "previous and new exit_MS",
                                        target_addr);
              branch_exploration_path->machine_state = std::make_unique<MachineState>(*exec_flow_target.machine_state);
            }
            else
            {
              m_log->info("🔄\tAlready took the target {:#x} with the same exit_MS --> do not add this target",
                          target_addr);
              add_this_target = false;
            }
          }
        }
        
        if(add_this_target)
        {
          exec_flow_target.cnt ++;
          succs_to_treat.push_back(std::move(branch_exploration_path));
        }
        
      }
    } // Jcc status unknown case
    
    
    // Case where we know this Jcc status
    else
    {
      addr_t target_addr = 0;
      if(jcc.getCondStatus() == JccCondStatus::TRUE)
      {
        m_log->info("✅ {} Jcc condition value: true", ExecFormula::resultSrcToString(jcc.getResultSrc()));
        m_jccs_results[last_instr].seen_only_target++;
        target_addr = *last_instr->getJmpAddr();
      }
      else if(jcc.getCondStatus() == JccCondStatus::FALSE)
      {
        m_log->info("✅ {} Jcc condition value: false", ExecFormula::resultSrcToString(jcc.getResultSrc()));
        m_jccs_results[last_instr].seen_only_next_mem++;
        target_addr = last_instr->getNextAddr();
      }
      
      exploration_path->machine_state->setInstrPtr(target_addr);
      exploration_path->found_method = EdgeFoundMethod::BOA_DISAS;
      
      // Si on peut faire autant de boucles qu'on veut on ajoute simplement ce target à notre pile
      if(m_config_boa_disassembling.max_loop_lap == -1)
      {
        succs_to_treat.push_back(std::move(exploration_path));
      }
      
      // Sinon si on a fixé une limite sur le nombre de boucles il faut vérifier qu'on ne soit pas arriver au max fixé par l'user
      else
      {
        int loop_cnt = loopChecker(returnsite_stack, bb_ep, target_addr);
        auto& exec_flow_target = m_exec_flow_history[returnsite_stack][bb_ep][target_addr];
        bool add_this_target = true;
        
        // Si on n'a pas encore dépassé le nombre max de tour, alors on peut ajouter ce target sans rien faire de particulier
        if(loop_cnt < m_config_boa_disassembling.max_loop_lap)
        {
          
        }
        
        // Si on vient juste d'atteindre la limite il faut juste sauvegarder ce MachineState et on ajoute ce target quand meme
        else if(loop_cnt == m_config_boa_disassembling.max_loop_lap)
        {
          m_log->info("🔄 We just reached the max number of loop --> Save this MachineState for later");
          std::unique_ptr<MachineState> machine_state_copy = std::make_unique<MachineState>(*exploration_path->machine_state);
          exec_flow_target.machine_state = std::move(machine_state_copy);
        }
        
        // Sinon on doit merge le dernier MachineState sauvegardé avec celui actuel et les comparer pour savoir si on continu ou non
        else if(loop_cnt > m_config_boa_disassembling.max_loop_lap)
        {
          m_log->info("🔄 We reached the max number of loop --> Compare current MachineState with saved one");
          
          // On merge sur le MS saved (comme ça c'est lui qui perd de l'info)
          bool at_least_one_change = exec_flow_target.machine_state->mergeWithMachineState(*exploration_path->machine_state);
          if(at_least_one_change)
          {
            m_log->info("🔄\tAlready took the target {:#x} but not with the same exit_MS --> merge "
                                      "previous and new exit_MS",
                                      target_addr);
            exploration_path->machine_state = std::make_unique<MachineState>(*exec_flow_target.machine_state);
          }
          else
          {
            m_log->info("🔄\tAlready took the target {:#x} with the same exit_MS --> do not add this target",
                        target_addr);
            add_this_target = false;
          }
        }

        if(add_this_target)
        {
          exec_flow_target.cnt ++;
          succs_to_treat.push_back(std::move(exploration_path));
        }
      }
    } // Jcc status known case
  }

  // STATIC JMP
  else if(last_instr->getType() == InstrType::JMP && last_instr->getSubType() == InstrSubType::STATIC_JMP)
  {
    SPDLOG_LOGGER_DEBUG(m_log, "Last instr type: STATIC_JMP ({})", last_instr->toStringLight());
    machine_state->setInstrPtr(*last_instr->getJmpAddr());
    exploration_path->found_method = EdgeFoundMethod::STATIC_DISAS;
    succs_to_treat.push_back(std::move(exploration_path));
  }

  // DYN JMP
  else if(last_instr->getType() == InstrType::JMP && last_instr->getSubType() == InstrSubType::DYN_JMP)
  {
    SPDLOG_LOGGER_DEBUG(m_log, "Last instr type: DYN_JMP ({})", last_instr->toStringLight());

    if(m_config_boa_disassembling.dyn_jmp_hooks.find(last_instr->getAddr()) !=
       m_config_boa_disassembling.dyn_jmp_hooks.end())
    {
      addr_t target_addr = m_config_boa_disassembling.dyn_jmp_hooks.at(last_instr->getAddr());
      m_log->info("✅ (hooked) Dyn jmp target: {:#x}", target_addr);
      machine_state->setInstrPtr(target_addr);
      exploration_path->found_method = EdgeFoundMethod::STATIC_DISAS;
      succs_to_treat.push_back(std::move(exploration_path));
    }
    else
    {
      auto import = m_binary->getImportAtAddr(last_instr->getAddr());
      if(import != nullptr)
      {
        m_log->info("✅ (call to library function) Dyn jmp target: {:#x}", import->function_addr);
        exploration_path->machine_state->setInstrPtr(import->function_addr);
        exploration_path->found_method = EdgeFoundMethod::STATIC_DISAS;
        succs_to_treat.push_back(std::move(exploration_path));
      }

      else if(!m_config_boa_disassembling.not_compute_dyn_jmps_targets)
      {
        DynJmpSymbExec dyn_jmp = DynJmpSymbExec(bb_exit_formula, m_solver_manager, m_binary);

        if(dyn_jmp.getStatus() == DynJmpStatus::CONCRETE)
        {
          m_log->info("✅ {} Dyn jmp status: {}", ExecFormula::resultSrcToString(dyn_jmp.getResultSrc()),
                      dyn_jmp.toString());
          m_dyn_jmps_results[last_instr].seen_concrete++;

          machine_state->setInstrPtr(dyn_jmp.getJmpAddr());
          exploration_path->found_method = EdgeFoundMethod::BOA_DISAS;

          succs_to_treat.push_back(std::move(exploration_path));
        }
        else
        {
          m_log->info("❌ {} Dyn jmp status: {}", ExecFormula::resultSrcToString(dyn_jmp.getResultSrc()),
                      dyn_jmp.toString());
          m_dyn_jmps_results[last_instr].seen_unknown++;

          // Check if we can find a corresponding DLL function
          if(last_instr->getPtrAddr().has_value())
          {
            auto import_entry = m_binary->getImportAtAddr(*last_instr->getPtrAddr());
            if(import_entry != nullptr)
            {
              m_log->warn("Assume that this dyn jmp calls {:#x}", import_entry->function_addr);
              machine_state->setInstrPtr(import_entry->function_addr);
              exploration_path->found_method = EdgeFoundMethod::STATIC_DISAS;
              succs_to_treat.push_back(std::move(exploration_path));
            }
          }
        }
      }
    }
  }

  // STATIC CALL
  else if(last_instr->getType() == InstrType::CALL && last_instr->getSubType() == InstrSubType::STATIC_CALL)
  {
    SPDLOG_LOGGER_DEBUG(m_log, "Last instr type: STATIC_CALL ({})", last_instr->toStringLight());
    exploration_path->found_method = EdgeFoundMethod::STATIC_DISAS;

    if(m_config_global.no_disassemble_call_targets)
    {
      machine_state->setInstrPtr(last_instr->getNextAddr());
    }
    else
    {
      machine_state->setInstrPtr(*last_instr->getJmpAddr());
      machine_state->pushReturnsite(last_instr->getNextAddr());
    }
    succs_to_treat.push_back(std::move(exploration_path));
  }

  // DYN CALL
  else if(last_instr->getType() == InstrType::CALL && last_instr->getSubType() == InstrSubType::DYN_CALL)
  {
    SPDLOG_LOGGER_DEBUG(m_log, "Last instr type: DYN_CALL ({})", last_instr->toStringLight());

    if(m_config_boa_disassembling.dyn_jmp_hooks.find(last_instr->getAddr()) !=
       m_config_boa_disassembling.dyn_jmp_hooks.end())
    {
      addr_t target_addr = m_config_boa_disassembling.dyn_jmp_hooks.at(last_instr->getAddr());
      m_log->info("✅ (hooked) Dyn call target: {:#x}", target_addr);
      machine_state->setInstrPtr(target_addr);
      exploration_path->found_method = EdgeFoundMethod::STATIC_DISAS;
      machine_state->pushReturnsite(last_instr->getNextAddr());
      succs_to_treat.push_back(std::move(exploration_path));
    }
    else if(!m_config_boa_disassembling.not_compute_dyn_jmps_targets)
    {

      DynJmpSymbExec dyn_call = DynJmpSymbExec(bb_exit_formula, m_solver_manager, m_binary);

      if(dyn_call.getStatus() == DynJmpStatus::CONCRETE)
      {
        m_log->info("✅ {} Dyn call status: {}", ExecFormula::resultSrcToString(dyn_call.getResultSrc()),
                    dyn_call.toString());
        m_dyn_calls_results[last_instr].seen_concrete++;

        machine_state->setInstrPtr(dyn_call.getJmpAddr());
        exploration_path->found_method = EdgeFoundMethod::BOA_DISAS;
        machine_state->pushReturnsite(last_instr->getNextAddr());
        succs_to_treat.push_back(std::move(exploration_path));
      }
      else
      {
        m_log->info("❌ {} Dyn call status: {}", ExecFormula::resultSrcToString(dyn_call.getResultSrc()),
                    dyn_call.toString());
        m_dyn_calls_results[last_instr].seen_unknown++;

        // Check if we can find a corresponding DLL function
        if(last_instr->getPtrAddr().has_value())
        {
          auto import_entry = m_binary->getImportAtAddr(*last_instr->getPtrAddr());
          if(import_entry != nullptr)
          {
            m_log->warn("Assume that this dyn call calls {:#x}", import_entry->function_addr);
            machine_state->setInstrPtr(import_entry->function_addr);
            exploration_path->found_method = EdgeFoundMethod::STATIC_DISAS;
            machine_state->pushReturnsite(last_instr->getNextAddr());
            succs_to_treat.push_back(std::move(exploration_path));
          }
        }
      }
    }
  }

  // RET
  else if(last_instr->getType() == InstrType::RET)
  {
    SPDLOG_LOGGER_DEBUG(m_log, "Last instr type: RET ({})", last_instr->toStringLight());

    std::optional<uint32_t> target;
    if(m_config_boa_disassembling.assume_all_rets_genuine)
    {
      if(!exploration_path->machine_state->getReturnsiteStack().empty())
      {
        target = exploration_path->machine_state->getReturnsiteStack().back();
        SPDLOG_LOGGER_DEBUG(m_log, "Assume that this RET is genuine and jump to the returnsite: {:#x}", *target);
      }
    }
    else
    {
      DynJmpSymbExec ret = DynJmpSymbExec(bb_exit_formula, m_solver_manager, m_binary);
      if(ret.getStatus() == DynJmpStatus::CONCRETE)
      {
        target = ret.getJmpAddr();

        // Check if this RET is THE RET that jump back in the OS function that restore context after excetion
        // handler execution
        if(machine_state->getExceptionContext().has_value() &&
           machine_state->getExceptionContext()->fake_kernel_return_address == target)
        {
          m_log->warn("Exit of current excpetion handler, we need to simulate Windows (context restoration, ...)");
          MachineStateSymbExec::ExceptionExitHook(machine_state, m_log);
          target = machine_state->getExceptionContext()->handler_exit_target;
          machine_state->clearExceptionContext();
        }

        else if(!machine_state->getReturnsiteStack().empty() && *target == machine_state->getReturnsiteStack().back())
        {
          m_log->info("✅ {} RET status: {} (genuine)", ExecFormula::resultSrcToString(ret.getResultSrc()),
                      ret.toString());
          m_rets_results[last_instr].seen_genuine++;
        }
        else
        {
          m_log->info("✅ {} RET status: {} (violated)", ExecFormula::resultSrcToString(ret.getResultSrc()),
                      ret.toString());
          m_rets_results[last_instr].seen_violated++;
        }
      }
      else
      {
        m_log->info("❌ {} RET status: {}", ExecFormula::resultSrcToString(ret.getResultSrc()), ret.toString());
        m_rets_results[last_instr].seen_unknown++;
      }
    }

    if(target.has_value())
    {
      machine_state->setInstrPtr(*target);
      machine_state->popReturnsiteIfNeeded(*target);
      exploration_path->found_method = EdgeFoundMethod::BOA_DISAS;
      succs_to_treat.push_back(std::move(exploration_path));
    }
  }

  // FAKE_LIB_FUNC
  else if(last_instr->getType() == InstrType::FAKE_LIB_FUNC)
  {
    SPDLOG_LOGGER_DEBUG(m_log, "Last instr type: FAKE_LIB_FUNC ({})", last_instr->toStringLight());

    if(last_instr->getOpcode() == "msvcrt.dll__cexit" || last_instr->getOpcode() == "msvcrt.dll_exit")
    {
      m_log->info("Call 'exit' windows API function, assume that this function does not return");
    }
    else
    {
      //TODO: Remove this hack
      if(machine_state->getInstrPtr() != 0x99999999)
      {
        SPDLOG_LOGGER_DEBUG(
            m_log, "RET of an external lib function jump on: {:#x}",
            machine_state->getInstrPtr());
        
        exploration_path->machine_state->popReturnsiteIfNeeded(machine_state->getInstrPtr());
        exploration_path->found_method = EdgeFoundMethod::BOA_DISAS;
        succs_to_treat.push_back(std::move(exploration_path));
      }
    }
  }

  // TRAP
  else if(last_instr->getType() == InstrType::TRAP)
  {
    static const std::vector<uint8_t> int_80 = {0xcd, 0x80};
    
    // Handle Linux syscall case
    if(last_instr->getBytes() == int_80 && m_binary->getFormat() == BinaryFormat::ELF)
    {
      // Check EAX value
      auto eax = machine_state->getRegValue(Register::EAX);
      if(eax.has_value())
      {
        // sys_exit case
        if(*eax == 0x1)
        {
          m_log->info("Linux syscall: sys_exit");
        }
        // Other syscall
        else
        {
          m_log->info("Linux syscall: eax = {:#x}", *eax);
          machine_state->setInstrPtr(last_instr->getNextAddr());
          exploration_path->found_method = EdgeFoundMethod::STATIC_DISAS;
          succs_to_treat.push_back(std::move(exploration_path));
        }
      }
      else
      {
        // We don't know the next address to execute
      }
    }
  }

  // Not implemented instruction
  else
  {
    throw std::runtime_error("Need to implement succ_to_treat for instr " + last_instr->toStringLight());
  }
}

// MARK: -
void BoaDisassemblingWave::disassembleWave(CFG* cfg, std::list<std::unique_ptr<exploration_path_t>>& paths_to_explore,
                                           std::optional<std::unique_ptr<exploration_path_t>>& next_wave_to_explore,
                                           unsigned int& symb_executed_instrs_cnt_all_waves)
{
  m_log->info("🌊 Sart disassembling wave n°{}", m_wave);

  const auto start_time_wave = std::chrono::steady_clock::now();
  auto start_time_500_bbs = start_time_wave;

  // Main loop: iterate over each exploration_path
  while(!paths_to_explore.empty())
  {

    // Prevent infinite loop
    if(m_symb_executed_bbs_cnt >= 15000000)
    {
      m_log->warn("Main loop counter reached 15000000 --> break to prevent infinite loop");
      break;
    }

    // Pop exploration_path to use in this tour
    std::unique_ptr<exploration_path_t> exploration_path = std::move(paths_to_explore.back());
    paths_to_explore.pop_back();

    m_log->info("🔶 #{}_{} (instrs: {}) (symb instrs: {}) (todo: {}) Symb execution of Basic Block {:#x}", m_wave,
                m_symb_executed_bbs_cnt, cfg->getDisassembledInstrs().size(), symb_executed_instrs_cnt_all_waves,
                paths_to_explore.size(), exploration_path->machine_state->getInstrPtr());

    // Disassemble BB starting at addr_to_disas address
    const BasicBlock* bb = nullptr;
    auto disas_result = m_disassembler->disassembleBasicBlock(
        bb, exploration_path->machine_state->getInstrPtr(), m_wave, exploration_path->last_instr,
        exploration_path->found_method, cfg, exploration_path->machine_state.get());

    // Need to handle the case where we have no BB at this point
    if(bb == nullptr)
    {
      // Self modification case
      if(disas_result == DisasStatus::SELF_MODIF)
      {
        m_log->warn("Next wave to disassemble start at address {:#x}", exploration_path->machine_state->getInstrPtr());
        utils::pushMessage("[Wave " + std::to_string(m_wave) + "] Self-modification at address " +
                           addr2s(exploration_path->machine_state->getInstrPtr()));
        
        /*
        m_log->info("Dump current memory values");
        exploration_path->machine_state->dumpMemInFile("/tmp/emotet_dump");
        m_log->info("Dump current memory values done");
        */
        
        // Create edge going to the next wave, just to know what is the address of the next wave entry point
        if(exploration_path->last_instr.has_value())
        {
          (*exploration_path->last_instr)
              ->addEdge(m_log, {m_wave + 1, exploration_path->machine_state->getInstrPtr()},
                        EdgeFoundMethod::BOA_DISAS);
        }
        next_wave_to_explore = std::make_optional(std::move(exploration_path));
      }

      // Fail case
      if(disas_result == DisasStatus::FAIL)
      {
        // Cela signifie que Capstone a échoué à désassembler ce BB (il a échoué à désassembler l'instruction à
        // l'adresse 'exploration_path->machine_state->getInstrPtr()') Si on est en Windows il faut voir pour trigger
        // une exception EXCEPTION_ILLEGAL_INSTRUCTION et chercher un handler à exécuter dans
        // 'exploration_path->machine_state' Sinon on n'est pas en Windows ou qu'on ne trouve pas le point d'entrée du
        // handler alors on passe ce tour...

        // On gros si on trouve le EP du handler on écrase exploration_path->machine_state->getInstrPtr() avec l'EP du
        // handler et on fait un 'continue;' pour revenir au début de la boucle et désassembler le handler puis
        // l'exécuter. Il ne faut pas oublier de faire un 'machine_state->setExceptionContext' avec tout ce qui va bien
        // (voir dans machine_state_symb_exec)

        if((m_binary->getFormat() == BinaryFormat::PE) && (exploration_path->last_instr.has_value()))
        {

          m_log->warn(
              "Execution of address {:#x} triggered an EXCEPTION_ILLEGAL_INSTRUCTION, we need to simulate Windows",
              exploration_path->machine_state->getInstrPtr());
          utils::pushMessage("[" + addr2s(exploration_path->machine_state->getInstrPtr()) +
                             "] EXCEPTION_ILLEGAL_INSTRUCTION");

          auto last_instr = *(exploration_path->last_instr);

          exploration_path->machine_state->setExceptionContext(
              {last_instr, 0x7ccccccc, std::nullopt, std::nullopt, Exception::ILLEGAL_INSTRUCTION});

          // Simulate Windows exception procedure
          MachineStateSymbExec::ExceptionEntryHook(exploration_path->machine_state.get(), m_log);

          // If we know the handler address we execute it in the next tour
          if(exploration_path->machine_state->getExceptionContext()->handler_entry_addr.has_value())
          {
            m_log->warn("\tHandler entry: {:#x}",
                        *exploration_path->machine_state->getExceptionContext()->handler_entry_addr);
            exploration_path->machine_state->setInstrPtr(
                *exploration_path->machine_state->getExceptionContext()->handler_entry_addr);
            exploration_path->found_method = EdgeFoundMethod::BOA_DISAS;
            paths_to_explore.push_back(std::move(exploration_path));
          }
          else
          {
            m_log->warn("\tWe don't know the handler to execute :-/");
          }
        }
      }

      // We need to "continue;" because there is no BB to symb exec in this tour
      continue;
    }

    // Get BB's micro instructions info (smtlib expr, etc)
    m_x86_to_smtlib->getMicroInstrs(bb);

    // Compute exit ExecFormula
    SPDLOG_LOGGER_DEBUG(m_log, "🔹 Compute exit ExecFormula");
    ExecFormula bb_exit_formula = ExecFormula(exploration_path->machine_state.get(), bb);
    SPDLOG_LOGGER_TRACE(m_log, "Computed exit ExecFormula:\n{}", bb_exit_formula.toString());

    // Generate SMTLIB formula from exit ExecFormula
    SPDLOG_LOGGER_DEBUG(m_log, "🔹 Generate formula in SMT-LIBv2 format from exit ExecFormula object");
    bb_exit_formula.getSmtlibFormula(m_binary, m_solver_manager,
                                     m_config_boa_disassembling.use_fake_val_for_symb_values);

    // Symb exec SMTLIB fromula
    SPDLOG_LOGGER_DEBUG(m_log, "🔹 Compute exit machine state valuation");
    std::optional<addr_t> new_bb_ep;
    MachineStateSymbExec::get(bb_exit_formula, m_log, m_solver_manager, m_binary, new_bb_ep,
                              m_config_boa_disassembling.windows_dlls_json_filepath,
                              m_config_boa_disassembling.disable_lib_fun_hooks, m_config_boa_disassembling.load_dlls);

    // Here we handle two special cases:
    // - exception in the middle of BB
    // - self-modification intra-BB
    if(new_bb_ep.has_value())
    {
      // If instructions after new_bb_ep have never been executed, we can remove it
      if(m_symb_executed_instrs.find(*new_bb_ep) == m_symb_executed_instrs.end())
      {
        m_log->warn("Need to shrink current BB to remove instructions after address {:#x} (including this one)",
                    *new_bb_ep);
        cfg->shrinkBB(m_log, bb, {m_wave, *new_bb_ep});
      }
      // Else, if instructions after new_bb_ep have already been executed, we only need to split the BB
      else
      {
        m_log->warn("Need to split current BB to move instructions after address {:#x} (including this one) outside",
                    *new_bb_ep);
        cfg->splitBB(m_log, {m_wave, *new_bb_ep});
      }
      m_log->warn("Re-symb exec BB {:#x}", bb->getEntryPointAddr());
      paths_to_explore.push_back(std::move(exploration_path));
      continue;
    }

    // Mark instrs of this BB as already executed (used to know if we shrink or split a BB in case of a self-modif
    // intra-BB or an exception
    for(const auto& instr : bb->getInstrs())
    {
      m_symb_executed_instrs.insert(instr->getAddr());
    }

    // Stats
    m_symb_executed_bbs_cnt++;
    m_symb_executed_instrs_cnt += bb->getSize();
    symb_executed_instrs_cnt_all_waves += bb->getSize();
    if(m_symb_executed_bbs_cnt % 500 == 0)
    {
      auto time = utils::milli2hr_min_sec_milli(
          std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start_time_500_bbs)
              .count());
      start_time_500_bbs = std::chrono::steady_clock::now();
      m_log->warn("🕑 #{}_{} (instrs: {}) (symb instrs: {}) (todo: {}) Elapsed time for the last 500 BBs (HH:mm:ss,ms): "
                  "{:02d}:{:02d}:{:02d},{:03d}",
                  m_wave, m_symb_executed_bbs_cnt, cfg->getDisassembledInstrs().size(),
                  symb_executed_instrs_cnt_all_waves, paths_to_explore.size(), time.hr, time.min, time.sec, time.milli);
    }
    
    //TEMPO
    if(paths_to_explore.size() >= 1)
    {
      //assert(false);
    }

    // Update last instr
    exploration_path->last_instr = bb->getLastInstr();

    // Grab all outgoing edges to consider (if necessary, compute dyn targets for dyn jmp instr or taken branch for
    // Jcc instr)
    SPDLOG_LOGGER_DEBUG(m_log, "🔹 Compute BB target(s) addr(s)");
    std::vector<std::unique_ptr<exploration_path_t>> succs_to_treat{};
    getSuccsToTreat(succs_to_treat, std::move(exploration_path), bb, bb_exit_formula);

    SPDLOG_LOGGER_DEBUG(m_log, "🔹 Add BB target(s) addr(s) in the 'paths_to_explore' queue");
    bool add_this_target;
    for(auto& succ_addr_to_symb_exec : succs_to_treat)
    {

      SPDLOG_LOGGER_TRACE(m_log, "🖥 Computed exit machine state:\n{}",
                          succ_addr_to_symb_exec->machine_state->toString());

      add_this_target = true;
      const auto& last_instr_type = (*succ_addr_to_symb_exec->last_instr)->getType();

      // Optimisation : Si la dernière instruction est un RET et qu'on a déjà dans paths_to_explore le même target
      // pour ce même RET en last_instr et avec la même pile d'appels alors on merge notre machine state avec
      // celui déjà présent dans paths_to_explore. ça réduit le nombre de chemins à epxlorer
      if(last_instr_type == InstrType::RET)
      {
        for(const auto& path_to_explore : paths_to_explore)
        {
          if(path_to_explore->machine_state->getInstrPtr() == succ_addr_to_symb_exec->machine_state->getInstrPtr() &&
             path_to_explore->last_instr == succ_addr_to_symb_exec->last_instr &&
             path_to_explore->machine_state->getReturnsiteStack() ==
                 succ_addr_to_symb_exec->machine_state->getReturnsiteStack())
          {
            m_log->info("Target {:#x} with the same returnsite stack is already in the queue, merge it "
                        "with current "
                        "one (RET case)",
                        path_to_explore->machine_state->getInstrPtr());
            path_to_explore->machine_state->mergeWithMachineState(*succ_addr_to_symb_exec->machine_state);
            add_this_target = false;
            break;
          }
        }
      }

      else if(last_instr_type == InstrType::JCC && succs_to_treat.size() == 2 && !paths_to_explore.empty())
      {
        const auto& path_to_explore_back = paths_to_explore.back();
        if(path_to_explore_back->machine_state->getInstrPtr() == succ_addr_to_symb_exec->machine_state->getInstrPtr() &&
           path_to_explore_back->last_instr == succ_addr_to_symb_exec->last_instr &&
           path_to_explore_back->machine_state->getReturnsiteStack() ==
               succ_addr_to_symb_exec->machine_state->getReturnsiteStack())
        {
          m_log->info("Target {:#x} with the same returnsite stack is the back of the queue, replace it with current "
                      "one (JCC case)",
                      path_to_explore_back->machine_state->getInstrPtr());
          path_to_explore_back->machine_state = std::move(succ_addr_to_symb_exec->machine_state);
          add_this_target = false;
        }
      }

      // Now check if we add this target in paths_to_explore or not
      if(add_this_target)
      {
        // If it's a RET, we want to add it before older RETs
        if(last_instr_type == InstrType::RET)
        {
          auto pos = paths_to_explore.begin();
          for(const auto& path_to_explore : paths_to_explore)
          {
            if((*path_to_explore->last_instr)->getType() == InstrType::RET)
            {
              pos++;
            }
            else
            {
              break;
            }
          }
          paths_to_explore.insert(pos, std::move(succ_addr_to_symb_exec));
        }
        else
        {
          paths_to_explore.push_back(std::move(succ_addr_to_symb_exec));
        }
      }

    } // while(succs_to_treat is not empty)

  } // while(addrs_to_symb_exec is not empty)

  m_total_time =
      std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start_time_wave).count();
}

BoaDisassembling::BoaDisassembling(Disassembler* disassembler, Binary* binary, x86ToSmtlib* x86_to_smtlib,
                                   SolverManager& solver_manager, BinaryParser& bin_parser,
                                   const config_boa_disassembling_t& config_boa_disassembling,
                                   const config_global_t& config_global)
    : m_disassembler(disassembler), m_binary(binary), m_x86_to_smtlib(x86_to_smtlib), m_solver_manager(solver_manager),
      m_bin_parser(bin_parser), m_config_boa_disassembling(config_boa_disassembling), m_config_global(config_global),
      m_symb_executed_instrs_cnt(0), m_log(spdlog::get(utils::boa_disas_logger))
{
  SPDLOG_LOGGER_TRACE(m_log, "BoaDisassembling::BoaDisassembling()");
}

void BoaDisassembling::disassembleBinary(CFGManager& cfg_manager)
{
  m_log->info("🚀 Disassemble binary with BOA algorithm");

  unsigned int wave = 0;

  // Initialisation of paths_to_explore with start_addr
  std::list<std::unique_ptr<exploration_path_t>> paths_to_explore;
  for(auto const& start_addr : m_config_global.start_addrs)
  {
    // Init entry machine state
    std::unique_ptr<MachineState> initial_entry_machine_state = std::make_unique<MachineState>(m_binary, start_addr, &m_bin_parser, m_config_boa_disassembling.windows_dlls_dir);
    initial_entry_machine_state->setInitMachineState(m_binary->getFormat(), m_config_boa_disassembling.load_dlls, m_config_boa_disassembling.user_reg_values, m_config_boa_disassembling.user_mem_cell_values);

    std::unique_ptr<exploration_path_t> initial_exploration_path =
        std::make_unique<exploration_path_t>(exploration_path_t{
            std::move(initial_entry_machine_state), std::nullopt, EdgeFoundMethod::STATIC_DISAS});

    paths_to_explore.push_back(std::move(initial_exploration_path));
  }
  std::vector<addr_t> cfg_entry_points = m_config_global.start_addrs;

  bool new_wave_to_disas = true;
  while(new_wave_to_disas)
  {
    new_wave_to_disas = false;

    // Create the fresh CFG for this wave
    CFG* cfg = cfg_manager.createEmptyCfg(cfg_entry_points);

    // Create the BoaDisassemblingWave instance for this wave
    std::unique_ptr<BoaDisassemblingWave> boa_disassembling_wave = std::make_unique<BoaDisassemblingWave>(
        m_disassembler, m_binary, m_x86_to_smtlib, m_solver_manager, m_config_boa_disassembling, m_config_global, wave);
    BoaDisassemblingWave* boa_disassembling_wave_ptr = boa_disassembling_wave.get();
    m_boa_disassembling_waves.insert({wave, std::move(boa_disassembling_wave)});

    // Start disassembly of this wave

    std::optional<std::unique_ptr<exploration_path_t>> next_wave_to_explore = std::nullopt;
    boa_disassembling_wave_ptr->disassembleWave(cfg, paths_to_explore, next_wave_to_explore,
                                                m_symb_executed_instrs_cnt);

    if(next_wave_to_explore.has_value())
    {
      wave++;
      if(m_config_boa_disassembling.max_waves_to_disassemble == -1 ||
         m_config_boa_disassembling.max_waves_to_disassemble != (int)wave)
      {
        new_wave_to_disas = true;
        cfg_entry_points = {(*next_wave_to_explore)->machine_state->getInstrPtr()};
        (*next_wave_to_explore)->last_instr = std::nullopt;
        (*next_wave_to_explore)->machine_state->clearWrittenAddrsHistory();
        paths_to_explore.clear(); // No needed? Should be already empty
        paths_to_explore.push_back(std::move(*next_wave_to_explore));
      }
    }
  }

} // disassembleBinary

const std::map<unsigned int, std::unique_ptr<BoaDisassemblingWave>>& BoaDisassembling::getBoaDisassemblingWaves() const
{
  return m_boa_disassembling_waves;
}

} // namespace boa
