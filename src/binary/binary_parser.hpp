#ifndef BINARY_PARSER_H
#define BINARY_PARSER_H

#include <string>

#include "binary/binary.hpp"
#include "utils/boa_types.hpp"
#include "utils/utils.hpp"

namespace boa
{

extern const char* bin_parser_logger;

class BinaryParser
{
public:
  BinaryParser();
  virtual ~BinaryParser() = 0;

  virtual std::shared_ptr<Binary> loadBinary(const std::string& binary_filepath, addr_t load_base_addr = 0x0000000000000000ULL) = 0;

protected:
  std::shared_ptr<spdlog::logger> m_log;
};

} // namespace boa

#endif
