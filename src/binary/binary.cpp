#include "binary.hpp"

#include <iomanip>
#include <iostream>
#include <sstream>

namespace boa
{

// Static variables
std::map<std::string, BinaryFormat> Binary::s_format_map = {
    {"ELF", BinaryFormat::ELF}, {"PE", BinaryFormat::PE}, {"RAW", BinaryFormat::RAW}};

std::map<std::string, BinaryArch> Binary::s_arch_map = {
    {"X86", BinaryArch::x86}, {"X86-32", BinaryArch::x86}, {"X86-64", BinaryArch::x86_64}};

// MARK:- Constructors and destructors
Binary::Binary(const std::string& filepath)
    : m_log(spdlog::get(utils::bin_parser_logger)), m_filepath(filepath), m_ifstream(filepath, std::ios::binary)
{
  if (!m_ifstream.good())
  {
    m_log->error("Missing file {}", filepath);
  }

  // Compute size of the binary file
  m_ifstream.seekg(0, std::ios::end);
  m_length = m_ifstream.tellg();
  m_ifstream.seekg(0, std::ios::beg);
}

// MARK:- Operators overloading
std::ostream& operator<<(std::ostream& os, const Binary& bin)
{
  os << "- Binary: " << bin.m_filepath << std::endl;
  os << "\t* Format: " << Binary::formatToString(bin.m_format) << std::endl;
  os << "\t* Arch: " << Binary::archToString(bin.m_arch) << std::endl;
  os << "\t* Base address: " << addr2s(bin.m_base_addr) << std::endl;
  os << "\t* Entry point: " << addr2s(bin.m_ep) << std::endl;
  if(bin.m_sections.empty())
  {
    os << "\t* Sections: Ø" << std::endl;
  }
  else
  {
    os << "\t* Sections:" << std::endl;
    for(const auto& section_p : bin.m_sections)
    {
      const auto& section = section_p.second;
      os << std::left << std::setfill(' ') << std::setw(45);
      os << "\t\t- name: " + section.name;
      os << std::left << std::setfill(' ') << std::setw(25);
      os << "vaddr: " + addr2s(section.vaddr);
      os << std::left << std::setfill(' ') << std::setw(25);
      os << "size: " + addr2s(section.size);
      os << std::left << std::setfill(' ') << std::setw(25);
      os << "vsize: " + addr2s(section.vsize);
      os << std::left << std::setfill(' ') << std::setw(25);
      os << "offset: " + addr2s(section.offset);
      os << std::left << std::setfill(' ') << std::setw(25);
      os << "perms: " + perms2s(section.perms) << std::endl;
    }
  }
  if(bin.m_segments.empty())
  {
    os << "\t* Segments: Ø" << std::endl;
  }
  else
  {
    os << "\t* Segments:" << std::endl;
    for(const auto& segment_p : bin.m_segments)
    {
      const auto& segment = segment_p.second;
      os << std::left << std::setfill(' ') << std::setw(45);
      os << "\t\t- name: " + segment.name;
      os << std::left << std::setfill(' ') << std::setw(25);
      os << "vaddr: " + addr2s(segment.vaddr);
      os << std::left << std::setfill(' ') << std::setw(25);
      os << "size: " + addr2s(segment.size);
      os << std::left << std::setfill(' ') << std::setw(25);
      os << "offset: " + addr2s(segment.offset);
      os << std::left << std::setfill(' ') << std::setw(25);
      os << "perms: " + perms2s(segment.perms) << std::endl;
    }
  }
  if(bin.m_imports.empty())
  {
    os << "\t* Imports: Ø" << std::endl;
  }
  else
  {
    os << "\t* Imports:" << std::endl;
    for(const auto& import : bin.m_imports)
    {
      os << std::left << std::setfill(' ') << std::setw(45);
      os << "\t\t- name: " + import.lib + "_" + import.name;
      os << std::left << std::setfill(' ') << std::setw(25);
      os << "vaddr: " + addr2s(import.vaddr);
      os << std::left << std::setfill(' ') << std::setw(35);
      os << "external func @: " + addr2s(import.function_addr);
      os << std::left << std::setfill(' ') << std::setw(30);
      os << "ordinal: " + std::to_string(import.ordinal) << std::endl;
    }
  }
  if(bin.m_exports.empty())
  {
    os << "\t* Exports: Ø" << std::endl;
  }
  else
  {
    os << "\t* Exports:" << std::endl;
    for(const auto& bin_export : bin.m_exports)
    {
      os << std::left << std::setfill(' ') << std::setw(45);
      os << "\t\t- name: " + bin_export.name;
      os << std::left << std::setfill(' ') << std::setw(25);
      os << "vaddr: " + addr2s(bin_export.vaddr);
      os << std::left << std::setfill(' ') << std::setw(30);
      os << "ordinal: " + std::to_string(bin_export.ordinal) << std::endl;
    }
  }

  return os;
}

// MARK:- Getters and setters
const std::string& Binary::getFilepath() const
{
  return m_filepath;
}

BinaryFormat Binary::getFormat() const
{
  return m_format;
}

void Binary::setFormat(BinaryFormat format)
{
  m_format = format;
}

BinaryArch Binary::getArch() const
{
  return m_arch;
}

void Binary::setArch(BinaryArch arch)
{
  m_arch = arch;
}

addr_t Binary::getEntryPoint() const
{
  return m_ep;
}

void Binary::setEntryPoint(addr_t ep)
{
  m_ep = ep;
}

addr_t Binary::getBaseAddr() const
{
  return m_base_addr;
}

void Binary::setBaseAddr(addr_t base_addr)
{
  m_base_addr = base_addr;
}

addr_t Binary::getLastAddr()
{
  addr_t last = 0x0000000000000000ULL;

  switch (m_format)
  {
  case BinaryFormat::RAW:
    last = m_length;
    break;
  case BinaryFormat::PE:
    for (const auto& section_pair : m_sections)
    {
      auto section = section_pair.second;
      auto vaddr = section.vaddr + (addr_t)section.size;
      if (vaddr > last)
        last = vaddr;
    }
    break;
  default:
    throw std::runtime_error("Needs to be implemented");
  }

  return last;
}

void Binary::addSection(const binary::section_t& section)
{
  if(m_format == BinaryFormat::PE && m_sections.find(section.vaddr) != m_sections.end())
  {
    throw std::runtime_error("A section with this vaddr (" + addr2s(section.vaddr) + ") already exists");
  }
  m_sections.insert({section.vaddr, section});
}

const std::map<addr_t, binary::section_t>& Binary::getSections() const
{
  return m_sections;
}

const binary::section_t* Binary::getCorrespondingSection(addr_t addr) const
{
  for(const auto& section : m_sections)
  {
    if(addr >= section.first && addr < section.first + section.second.vsize)
    {
      SPDLOG_LOGGER_TRACE(m_log, "Vaddr {:#x} is in '{}' section", addr, section.second.name);
      return &section.second;
    }
  }
  return nullptr;
}

void Binary::addSegment(const binary::segment_t& segment)
{
  if(m_segments.find(segment.vaddr) != m_segments.end())
  {
    throw std::runtime_error("A segment with this vaddr already exists");
  }
  m_segments.insert({segment.vaddr, segment});
}

const std::map<addr_t, binary::segment_t>& Binary::getSegments() const
{
  return m_segments;
}

const binary::segment_t* Binary::getCorrespondingSegment(addr_t addr) const
{
  for(const auto& segment : m_segments)
  {
    if(addr >= segment.first && addr < segment.first + segment.second.size)
    {
      return &segment.second;
    }
  }
  return nullptr;
}

const std::vector<binary::import_entry_t>& Binary::getImports()
{
  return m_imports;
}

const binary::import_entry_t* Binary::getImportAtAddr(addr_t entry_addr) const
{
  for(auto const& import : m_imports)
  {
    if(import.vaddr == entry_addr)
    {
      return &import;
    }
  }
  return nullptr;
}

const binary::import_entry_t* Binary::getImportWithFuncAddr(addr_t lib_func_addr) const
{
  for(auto const& import : m_imports)
  {
    if(import.function_addr == lib_func_addr)
    {
      return &import;
    }
  }
  return nullptr;
}

void Binary::addImport(const binary::import_entry_t& import)
{
  m_imports.push_back(import);
}

const std::vector<binary::export_entry_t>& Binary::getExports() const
{
  return m_exports;
}

const binary::export_entry_t* Binary::getExportAtAddr(addr_t entry_addr) const
{
  for (auto const& export_entry : m_exports)
  {
    if (export_entry.vaddr == entry_addr)
    {
      return &export_entry;
    }
  }
  return nullptr;
}

const binary::export_entry_t* Binary::getExportByOrdinal(int ordinal) const
{
  for (auto const& export_entry : m_exports)
  {
    if (export_entry.ordinal == ordinal)
    {
      return &export_entry;
    }
  }
  return nullptr;
}

const binary::export_entry_t* Binary::getExportByName(const std::string& name) const
{
  for (auto const& export_entry : m_exports)
  {
    if (export_entry.name == name)
    {
      return &export_entry;
    }
  }
  return nullptr;
}

void Binary::addExport(const binary::export_entry_t& export_entry)
{
  m_exports.push_back(export_entry);
}

std::optional<addr_t> Binary::vaddrToRawOffset(addr_t vaddr) const
{
  SPDLOG_LOGGER_TRACE(m_log, "Get raw offset of Vaddr {:#x}", vaddr);

  addr_t raw_offset;
  if(m_format == BinaryFormat::PE)
  {
    auto section = getCorrespondingSection(vaddr);
    if(section != nullptr)
    {
      raw_offset = vaddr - section->vaddr + section->offset;
    }
    // PE header is not in any section
    // But AsPack (at @01_0x1005389) read in the header...
    else if(vaddr >= m_base_addr && vaddr < m_sections.begin()->first)
    {
      m_log->warn("Read operation at @ {:#x} that is not in a known section (read in header?)", vaddr);
      utils::pushMessage("Read operation at @ " + addr2s(vaddr) + " that is not in a known section (read in header?)");
      raw_offset = vaddr - m_base_addr;
    }
    else
    {
      return std::nullopt;
    }
  }
  else if(m_format == BinaryFormat::ELF)
  {
    auto segment = getCorrespondingSegment(vaddr);
    if(segment != nullptr)
    {
      raw_offset = segment->offset + (vaddr - segment->vaddr);
    }
    else
    {
      return std::nullopt;
    }
  }
  else if(m_format == BinaryFormat::RAW)
  {
    raw_offset = vaddr;
    if(raw_offset >= m_length)
    {
      return std::nullopt;
    }
  }
  else
  {
    throw std::runtime_error("Need to be implemented");
  }
  SPDLOG_LOGGER_TRACE(m_log, "Raw offset of @{:#x} = {:#x}", vaddr, raw_offset);
  return raw_offset;
}

std::optional<uint8_t> Binary::getByte(addr_t addr)
{
  SPDLOG_LOGGER_TRACE(m_log, "Binary::getByte({:#x})", addr);
  auto section = getCorrespondingSection(addr);
  if(section != nullptr)
  {
    if(section->name == ".bss")
    {
      SPDLOG_LOGGER_DEBUG(m_log, "In binray @[{:#x}]_1 = 0x00 because we are in the .bss section", addr);
      return 0x0;
    }
    if(addr > section->vaddr + section->size)
    {
      SPDLOG_LOGGER_DEBUG(m_log, "In binray @[{:#x}]_1 = 0x00 because we are out of the file section (vaddr > section real size)", addr);
      return 0x0;
    }
  }
  auto raw_offset = vaddrToRawOffset(addr);
  if(!raw_offset.has_value())
  {
    return std::nullopt;
  }
  m_ifstream.seekg((int64_t)*raw_offset);
  char byte;
  m_ifstream.read(&byte, 1);

  if(!m_ifstream)
  {
    m_log->warn("Failed to read a byte at file offset {:#x}, return 0x0 for Vaddr {:#x}", *raw_offset, addr);
    m_ifstream.clear();
    return 0x0;
  }

  SPDLOG_LOGGER_DEBUG(m_log, "In binray @[{:#x}]_1 = {:#x}", addr, (uint8_t)byte);

  return (uint8_t)byte;
}

std::optional<uint32_t> Binary::getWord(addr_t addr)
{
  SPDLOG_LOGGER_TRACE(m_log, "Binary::getWord({:#x})", addr);
  auto section = getCorrespondingSection(addr);
  if(section != nullptr)
  {
    if(section->name == ".bss")
    {
      SPDLOG_LOGGER_DEBUG(m_log, "In binray @[{:#x}]_4 = 0x00000000 because we are in the .bss section", addr);
      return 0x0;
    }
    if(addr > section->vaddr + section->size)
    {
      SPDLOG_LOGGER_DEBUG(m_log, "In binray @[{:#x}]_4 = 0x00000000 because we are out of the file section (vaddr > section real size)", addr);
      return 0x0;
    }
  }
  auto raw_offset = vaddrToRawOffset(addr);
  if(!raw_offset.has_value())
  {
    return std::nullopt;
  }
  m_ifstream.seekg((int64_t)*raw_offset);
  std::vector<char> bytes(4);
  m_ifstream.read(&bytes[0], 4);

  if(!m_ifstream)
  {
    m_log->warn("Failed to read a word at file offset {:#x}, return 0x0 for Vaddr {:#x}", *raw_offset, addr);
    m_ifstream.clear();
    return 0x0;
  }

  uint32_t mem_word = bytes[0] + bytes[1] * 256 + bytes[2] * std::pow(256, 2) + bytes[3] * std::pow(256, 3);

  SPDLOG_LOGGER_DEBUG(m_log, "In binray @[{:#x}]_4 = {:#x}", addr, mem_word);
  return mem_word;
}

std::string Binary::toString() const
{
  std::stringstream ss;
  ss << *this;
  return ss.str();
}

unsigned int Binary::getLength() const
{
  return m_length;
}

std::ifstream& Binary::getIfStream()
{
  return m_ifstream;
}

// Static functions
const char* Binary::formatToString(const BinaryFormat& bin_format)
{
  switch(bin_format)
  {
  case BinaryFormat::ELF:
    return "ELF";
  case BinaryFormat::PE:
    return "PE";
  case BinaryFormat::MACHO:
    return "Mach-O";
  case BinaryFormat::RAW:
    return "Raw";
  default:
    return "unknown format";
  }
}

BinaryFormat Binary::stringToFormat(const std::string& bin_format_s)
{
  std::string bin_format_up = bin_format_s;
  transform(bin_format_up.begin(), bin_format_up.end(), bin_format_up.begin(), ::toupper);
  if(Binary::s_format_map.find(bin_format_up) == Binary::s_format_map.end())
  {
    throw std::out_of_range("Unknown format " + bin_format_s);
  }
  return Binary::s_format_map.at(bin_format_up);
}

const char* Binary::archToString(const BinaryArch& bin_arch)
{
  switch(bin_arch)
  {
  case BinaryArch::x86:
    return "x86";
  case BinaryArch::x86_64:
    return "x86-64";
  default:
    return "unknown arch";
  }
}

BinaryArch Binary::stringToArch(const std::string& bin_arch_s)
{
  std::string bin_arch_up = bin_arch_s;
  transform(bin_arch_up.begin(), bin_arch_up.end(), bin_arch_up.begin(), ::toupper);
  if(Binary::s_arch_map.find(bin_arch_up) == Binary::s_arch_map.end())
  {
    throw std::out_of_range("Unknown arch " + bin_arch_s);
  }
  return Binary::s_arch_map.at(bin_arch_up);
}

} // namespace boa
