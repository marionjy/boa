#include "binary_parser_r2.hpp"

#include <algorithm>
#include <iomanip>
#include <iostream>

namespace boa
{

// Constructors and destructors
BinaryParserRadare2::BinaryParserRadare2()
{
  SPDLOG_LOGGER_TRACE(m_log, "BinaryParserRadare2::BinaryParserRadare2()");
}

BinaryParserRadare2::~BinaryParserRadare2()
{
  SPDLOG_LOGGER_TRACE(m_log, "BinaryParserRadare2::~BinaryParserRadare2()");
}

std::map<std::string, std::set<Permission>> perms_map{
    {"----", {}},
    {"-r--", {Permission::READ}},
    {"-rw-", {Permission::READ, Permission::WRITE}},
    {"-r-x", {Permission::READ, Permission::EXECUTE}},
    {"-rwx", {Permission::READ, Permission::WRITE, Permission::EXECUTE}}};

std::shared_ptr<Binary> BinaryParserRadare2::loadBinary(const std::string& binary_filepath, addr_t load_base_addr)
{
  SPDLOG_LOGGER_TRACE(m_log, "BinaryParserRadare2::loadBinary");
  m_log->info("Parse header of {} to collect info", binary_filepath);

  // Open binary file with radare2
  SPDLOG_LOGGER_TRACE(m_log, "R2 open file: {}", binary_filepath);
  std::string parameters = {};
  if (load_base_addr)
    parameters = "-B " + std::to_string(load_base_addr);
  R2 r2 = R2(binary_filepath, parameters);

  // Create Binary object
  std::shared_ptr<Binary> binary = std::make_shared<Binary>(binary_filepath);

  // Send iIj command to obtain binary info (type, arch, base address, ...)
  std::string cmd = "iIj";
  SPDLOG_LOGGER_TRACE(m_log, "R2 sendCmd: {}", cmd);
  std::string answer = r2.sendCmd(cmd);
  SPDLOG_LOGGER_TRACE(m_log, "R2 answer: {}", answer);
  nlohmann::json bin_infos = nlohmann::json::parse(answer);
  std::string format_s = bin_infos["bintype"];
  if(format_s == "")
  {
    format_s = "RAW";
  }
  SPDLOG_LOGGER_DEBUG(m_log, "Binary format: {}", format_s);
  binary->setFormat(Binary::stringToFormat(format_s));

  std::string arch_s = bin_infos["arch"];
  int bits_i = bin_infos["bits"];
  if(arch_s == "")
  {
    arch_s = "X86";
    bits_i = 32;
  }
  SPDLOG_LOGGER_DEBUG(m_log, "Binary arch: {}", arch_s);
  SPDLOG_LOGGER_DEBUG(m_log, "Binary bits: {}", bits_i);
  arch_s = arch_s + "-" + std::to_string(bits_i);
  binary->setArch(Binary::stringToArch(arch_s));

  addr_t base_addr = (addr_t)bin_infos["baddr"];
  SPDLOG_LOGGER_DEBUG(m_log, "Base address: {:#x}", base_addr);
  binary->setBaseAddr(base_addr);

  // Send iej command to obtain entry point
  cmd = "iej";
  SPDLOG_LOGGER_TRACE(m_log, "R2 sendCmd: {}", cmd);
  answer = r2.sendCmd(cmd);
  SPDLOG_LOGGER_TRACE(m_log, "R2 answer: {}", answer);
  nlohmann::json bin_ep = nlohmann::json::parse(answer);
  addr_t ep = 0x0;
  if(bin_ep[0].find("vaddr") != bin_ep[0].end())
  {
    ep = (addr_t)bin_ep[0]["vaddr"];
  }
  SPDLOG_LOGGER_DEBUG(m_log, "Binary ep: {0:#x}", ep);
  binary->setEntryPoint(ep);

  // Send iSj command to obtain sections
  cmd = "iSj";
  SPDLOG_LOGGER_TRACE(m_log, "R2 sendCmd: {}", cmd);
  answer = r2.sendCmd(cmd);
  SPDLOG_LOGGER_TRACE(m_log, "R2 answer: {}", answer);
  nlohmann::json sections_j = nlohmann::json::parse(answer);
  for(auto section_j : sections_j)
  {
    std::string perm = section_j["perm"];
    if(perms_map.find(perm) == perms_map.end())
    {
      throw std::runtime_error("Unknown permission string: " + perm);
    }
    binary::section_t section = {section_j["name"], section_j["size"], section_j["vsize"], section_j["vaddr"], section_j["paddr"],
                                 perms_map.at(perm)};

    SPDLOG_LOGGER_DEBUG(m_log, "Section {}, offset: {:#x}, size: {:#x}, vsize: {:#x}, vaddr: {:#x}, perm: {}", section.name,
                        section.offset, section.size, section.vsize, section.vaddr, perm);

    if(section.vaddr == 0)
    {
      continue;
    }

    binary->addSection(section);
  }

  // Send iSSj command to obtain segments
  cmd = "iSSj";
  SPDLOG_LOGGER_TRACE(m_log, "R2 sendCmd: {}", cmd);
  answer = r2.sendCmd(cmd);
  SPDLOG_LOGGER_TRACE(m_log, "R2 answer: {}", answer);
  nlohmann::json segments_j = nlohmann::json::parse(answer);
  for(auto segment_j : segments_j)
  {
    std::string segment_name = segment_j["name"];
    if(segment_name.find("LOAD") == std::string::npos)
    {
      continue;
    }

    std::string perm = segment_j["perm"];
    if(perms_map.find(perm) == perms_map.end())
    {
      throw std::runtime_error("Unknown permission string: " + perm);
    }
    binary::segment_t segment = {segment_j["name"], segment_j["vsize"], segment_j["vaddr"], segment_j["paddr"],
                                 perms_map.at(perm)};

    SPDLOG_LOGGER_DEBUG(m_log, "Segment {}, offset: {:#x}, size: {:#x}, vaddr: {:#x}, perm: {}", segment.name,
                        segment.offset, segment.size, segment.vaddr, perm);

    binary->addSegment(segment);
  }

  // Send iij command to obtain import entries
  cmd = "iij";
  SPDLOG_LOGGER_TRACE(m_log, "R2 sendCmd: {}", cmd);
  answer = r2.sendCmd(cmd);
  SPDLOG_LOGGER_TRACE(m_log, "R2 answer: {}", answer);
  nlohmann::json imports_j = nlohmann::json::parse(answer);
  for(auto import_j : imports_j)
  {
    std::string type = import_j["type"];
    if(type != "FUNC")
    {
      continue;
    }

    binary::import_entry_t import;

    import.ordinal = 0;
    if(import_j.find("ordinal") != import_j.end())
    {
      import.ordinal = import_j["ordinal"];
    }

    import.name = import_j["name"];

    if(import_j.find("libname") != import_j.end())
    {
      import.lib = import_j["libname"];
      std::transform(import.lib.begin(), import.lib.end(), import.lib.begin(), ::tolower);
    }

    import.vaddr = import_j["plt"];

    std::string cmd = "s " + std::to_string(import.vaddr);
    SPDLOG_LOGGER_TRACE(m_log, "R2 sendCmd: {}", cmd);
    std::string answer = r2.sendCmd(cmd);
    SPDLOG_LOGGER_TRACE(m_log, "R2 answer: {}", answer);

    cmd = "pxwj 4";
    SPDLOG_LOGGER_TRACE(m_log, "R2 sendCmd: {}", cmd);
    answer = r2.sendCmd(cmd);
    SPDLOG_LOGGER_TRACE(m_log, "R2 answer: {}", answer);

    nlohmann::json addr_j = nlohmann::json::parse(answer);
    import.function_addr = (uint32_t)addr_j[0];

    SPDLOG_LOGGER_DEBUG(m_log, "Imported function at entry {:#x}: {}_{}, ordinal: {}, function_addr: {:#x}",
                        import.vaddr, import.lib, import.name, import.ordinal, import.function_addr);
    binary->addImport(import);
  }

  // Send iEj command to obtain export entries
  cmd = "iEj";
  SPDLOG_LOGGER_TRACE(m_log, "R2 sendCmd: {}", cmd);
  answer = r2.sendCmd(cmd);
  SPDLOG_LOGGER_TRACE(m_log, "R2 answer: {}", answer);
  nlohmann::json exports_j = nlohmann::json::parse(answer);
  for (const auto& export_j : exports_j)
  {
    std::string type = export_j["type"];
    if (type != "FUNC")
      continue;

    binary::export_entry_t export_entry;

    export_entry.ordinal = 0;
    if (export_j.find("ordinal") != export_j.end())
      export_entry.ordinal = export_j["ordinal"];

    export_entry.name = export_j["name"];

    export_entry.vaddr = export_j["vaddr"];

    SPDLOG_LOGGER_DEBUG(m_log, "Exported function at entry {:#x}: {}, ordinal: {}", export_entry.vaddr, export_entry.name, export_entry.ordinal);
    binary->addExport(export_entry);
  }

  return binary;
}

} // namespace boa
