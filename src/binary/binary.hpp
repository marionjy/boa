#ifndef BINARY_H
#define BINARY_H

#include <algorithm>
#include <fstream>
#include <map>
#include <memory>
#include <string>
#include <unordered_map>

#include "utils/boa_types.hpp"
#include "utils/utils.hpp"

namespace boa
{

enum class BinaryFormat
{
  ELF,
  PE,
  MACHO,
  RAW
};

enum class BinaryArch
{
  x86,
  x86_64
};

namespace binary
{

struct section_t
{
  std::string name;
  unsigned int size; // Section size in file
  unsigned int vsize; // Virtual size (vsize <= size)
  addr_t vaddr;
  addr_t offset;
  std::set<Permission> perms;
};

struct import_entry_t
{
  int ordinal;
  std::string lib;
  std::string name;
  addr_t vaddr;         // vaddr of the entry
  addr_t function_addr; // This is the "hardcoded" address found at @[vaddr] in the binary file. Don't know (yet) if
                        // this addr is a "preferred" addr added by the compiler? During real execution @[vaddr] is
                        // patched by the OS with the "real" external function entry point address (e.g 0x77e59f93)
};

struct export_entry_t
{
  int ordinal;
  std::string name;
  addr_t vaddr;
};

// For ELF files
struct segment_t
{
  std::string name;
  unsigned int size;
  addr_t vaddr;
  addr_t offset;
  std::set<Permission> perms;
};

} // namespace binary

class Binary
{
public:
  // Static variables
  static std::map<std::string, BinaryFormat> s_format_map;
  static std::map<std::string, BinaryArch> s_arch_map;

  // MARK:- Constructors and destructors
  Binary() = delete;
  Binary(const std::string& filepath);

  ~Binary() = default;

  Binary(Binary const&) = delete;
  Binary& operator=(Binary const&) = delete;

  Binary(Binary&&) = default;
  Binary& operator=(Binary&&) = delete;

  // MARK:- Operators overloading
  friend std::ostream& operator<<(std::ostream& os, const Binary& bin);

  // MARK:- Getters and setters
  const std::string& getFilepath() const;
  BinaryFormat getFormat() const;
  void setFormat(BinaryFormat format);
  BinaryArch getArch() const;
  void setArch(BinaryArch arch);
  addr_t getEntryPoint() const;
  void setEntryPoint(addr_t ep);
  addr_t getBaseAddr() const;
  void setBaseAddr(addr_t base_addr);
  addr_t getLastAddr();

  void addSection(const binary::section_t& section);
  const std::map<addr_t, binary::segment_t>& getSegments() const;
  const binary::segment_t* getCorrespondingSegment(addr_t addr) const;

  void addSegment(const binary::segment_t& segment);
  const std::map<addr_t, binary::section_t>& getSections() const;
  const binary::section_t* getCorrespondingSection(addr_t addr) const;

  const std::vector<binary::import_entry_t>& getImports();
  const binary::import_entry_t* getImportAtAddr(addr_t entry_addr) const;
  const binary::import_entry_t* getImportWithFuncAddr(addr_t lib_func_addr) const;
  void addImport(const binary::import_entry_t& import);
  const std::vector<binary::export_entry_t>& getExports() const;
  const binary::export_entry_t* getExportAtAddr(addr_t entry_addr) const;
  const binary::export_entry_t* getExportByOrdinal(int ordinal) const;
  const binary::export_entry_t* getExportByName(const std::string& name) const;
  void addExport(const binary::export_entry_t& export_entry);
  std::optional<addr_t> vaddrToRawOffset(addr_t vaddr) const;

  std::optional<uint8_t> getByte(addr_t addr);
  std::optional<uint32_t> getWord(addr_t addr);

  std::string toString() const;

  unsigned int getLength() const;
  std::ifstream& getIfStream();

  // Static functions
  static const char* formatToString(const BinaryFormat& bin_format);
  static BinaryFormat stringToFormat(const std::string& bin_format_s);

  static const char* archToString(const BinaryArch& bin_arch);
  static BinaryArch stringToArch(const std::string& bin_arch_s);

private:
  std::shared_ptr<spdlog::logger> m_log;
  const std::string& m_filepath;
  BinaryFormat m_format;
  BinaryArch m_arch;
  addr_t m_ep;
  addr_t m_base_addr; // ImageBase with PEfile
  std::map<addr_t, binary::section_t> m_sections;
  std::map<addr_t, binary::segment_t> m_segments;
  std::ifstream m_ifstream;
  unsigned int m_length; // Number of bytes in the file
  std::vector<binary::import_entry_t> m_imports;
  std::vector<binary::export_entry_t> m_exports;
};

} // namespace boa

#endif
