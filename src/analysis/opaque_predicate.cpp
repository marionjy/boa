#include "opaque_predicate.hpp"

namespace boa
{

OpaquePredicateAnalysis::OpaquePredicateAnalysis(CFG* cfg, SolverManager& solver_manager, Binary* binary,
                                                 x86ToSmtlib* x86_to_smtlib)
    : m_log(spdlog::get(utils::boa_disas_logger)), m_cfg(cfg), m_solver_manager(solver_manager), m_binary(binary),
      m_x86_to_smtlib(x86_to_smtlib)
{
}

void OpaquePredicateAnalysis::startAnalysisOneThread(const std::unordered_set<const BasicBlock*>& jcc_bbs,
                                                     unsigned int index_to_run, unsigned int nb_thread)
{
  unsigned int th_id = index_to_run;
  m_log->info("Start thread n°{}", th_id);

  auto start_time_100_bbs = std::chrono::steady_clock::now();

  int global_cnt = 0;
  unsigned int cnt = 0;
  int bbs_cnt = 0;

  for(const auto& bb : jcc_bbs)
  {
    if(cnt == th_id)
    {
      try
      {
        bbs_cnt++;
        m_x86_to_smtlib->getMicroInstrs(bb);

        MachineState machine_state = MachineState(m_binary, 0x0);
        machine_state.setRegValue(Register::ESP, 0x12345);
        machine_state.setRegValue(Register::EBP, 0x12345);

        ExecFormula exec_formula = ExecFormula(&machine_state, bb);
        const auto& smtlib_formula = exec_formula.getSmtlibFormula(m_binary, m_solver_manager, false, false);

        std::pair<std::string, std::optional<uint32_t>> result = {*smtlib_formula.cond_symb_var, std::nullopt};
        m_solver_manager.getUniqueSymbVarValuation(result, smtlib_formula.smtlib_text, 1, th_id);

        if(result.second.has_value())
        {
          if(*result.second == 1)
          {
            m_log->info("🔶 #{} (th {}) BasicBlock {:#x} --> ✅ Jcc condition value: true (opaque)", global_cnt, th_id,
                        bb->getEntryPointAddr());
          }
          else
          {
            m_log->info("🔶 #{} (th {}) BasicBlock {:#x} --> ✅ Jcc condition value: false (opaque)", global_cnt, th_id,
                        bb->getEntryPointAddr());
          }
        }
        else
        {
          m_log->info("🔶 #{} (th {}) BasicBlock {:#x} --> ❌ Jcc condition value: unknown (not opaque)", global_cnt,
                      th_id, bb->getEntryPointAddr());
        }
      }
      catch(std::exception const& e)
      {
        m_log->warn("⚠️ #{} (th {}) BasicBlock {:#x} opaque predicate analysis failed", global_cnt, th_id,
                    bb->getEntryPointAddr());
      }

      if(bbs_cnt % 100 == 0)
      {
        auto milli =
            std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start_time_100_bbs)
                .count();
        start_time_100_bbs = std::chrono::steady_clock::now();
        m_log->info("Time to analyse 100 BBs on thread n°{}: {} ms", th_id, milli);
      }
    }

    cnt++;
    if(cnt == nb_thread)
    {
      cnt = 0;
    }
    global_cnt++;
  }
}

void OpaquePredicateAnalysis::startAnalysis(unsigned int nb_thread)
{
  m_log->info("Start Opaque Predicate Analysis with {} threads", nb_thread);

  auto start_time = std::chrono::steady_clock::now();

  // Prepare a set with all BBs ending by a Jcc
  std::unordered_set<const BasicBlock*> jcc_bbs;
  for(const auto& bb_p : m_cfg->getBasicBlocks())
  {
    if(bb_p.second->getLastInstr()->getType() == InstrType::JCC)
    {
      jcc_bbs.insert(bb_p.second.get());
    }
  }

  // Launch nb_thread threads
  std::vector<std::thread> threads;
  for(unsigned int i = 0; i < nb_thread; i++)
  {
    threads.push_back(
        std::thread(&OpaquePredicateAnalysis::startAnalysisOneThread, this, std::ref(jcc_bbs), i, nb_thread));
  }

  // Wait for all threads to finish
  for(auto& th : threads)
  {
    th.join();
  }

  auto milli =
      std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start_time).count();
  auto hr = milli / 3600000;
  milli = milli - 3600000 * hr;
  auto min = milli / 60000;
  milli = milli - 60000 * min;
  auto sec = milli / 1000;
  milli = milli - 1000 * sec;

  m_log->info("🕑 Elapsed time for opaque predicate analysis (HH:mm:ss,ms): {:02d}:{:02d}:{:02d},{:03d}", hr, min, sec,
              milli);
}

} // namespace boa
