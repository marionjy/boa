#include "config.hpp"

#include "CLI/CLI.hpp"
#include <iostream>

namespace boa
{

Config::Config(int argc, char* argv[])
{

  CLI::App app{"BOA: The hybrid disassembler by symbolic Basic blOck Analysis"};

  // INI config file support
  app.set_config("-c,--config", "", "Configuration ini file", false);

  // MARK:- Global config group
  app.add_option("-b,--binary", m_config_global.bin_filepath, "Binary file to disassemble")
      ->required()
      ->check(CLI::ExistingFile)
      ->group("Global");

  std::vector<std::pair<std::string, DisassemblyMode>> disassembly_mode_map{{"static", DisassemblyMode::STATIC},
                                                                            {"boa", DisassemblyMode::BOA}};
  m_config_global.disas_mode = DisassemblyMode::BOA;
  app.add_option("-d,--disas-mode", m_config_global.disas_mode, "Disassembly mode")
      ->transform(CLI::CheckedTransformer(disassembly_mode_map, CLI::ignore_case))
      ->group("Global");

  app.add_option("-s,--start-addrs", m_config_global.start_addrs, "Start addresses to disassemble [Binary entry point]")
      ->group("Global");

  std::vector<std::pair<std::string, DisassemblerEngine>> disassember_map{{"cs", DisassemblerEngine::CAPSTONE}};
  m_config_global.disassembler = DisassemblerEngine::CAPSTONE;
  app.add_option("--disassembler", m_config_global.disassembler, "Disassembler engine to use [capstone]")
      ->transform(CLI::CheckedTransformer(disassember_map, CLI::ignore_case))
      ->group("Global");

  std::vector<addr_t> addrs_not_to_disas;
  app.add_option("--addrs-not-to-disas", addrs_not_to_disas, "Blacklisted addresses not to disassemble")
      ->group("Global");

  app.add_flag("--no-disas-call-targets", m_config_global.no_disassemble_call_targets,
               "Do not disassemble CALL targets [false]")
      ->group("Global");

  app.add_flag("--print-csv-results", m_config_global.print_csv_result, "Print all results in CSV format [false]")
      ->group("Global");


  // MARK: - BOA mode group
  m_config_boa_disassembling.max_loop_lap = 50;
  app.add_option("-l,--loop-lap", m_config_boa_disassembling.max_loop_lap,
                 "Max number of lap to perform in loop (-1 to completely unroll all loops) [50]")
      ->group("BOA disassembling mode");

  m_config_boa_disassembling.max_waves_to_disassemble = -1;
  app.add_option("--max-waves", m_config_boa_disassembling.max_waves_to_disassemble,
                 "Max number of waves to disassemble (-1 to disassemble all waves) [-1]")
      ->group("BOA disassembling mode");

  std::vector<std::pair<std::string, SMTSolver>> solver_smt_map{{"cvc4", SMTSolver::CVC4},
                                                                {"boolector", SMTSolver::BOOLECTOR},
                                                                {"z3", SMTSolver::Z3},
                                                                {"yices", SMTSolver::YICES},
                                                                {"mathsat", SMTSolver::MATHSAT}};
  m_config_boa_disassembling.config_solver.smt_solver = SMTSolver::YICES;
  app.add_option("--solver", m_config_boa_disassembling.config_solver.smt_solver, "SMT solver to use [yices]")
      ->transform(CLI::CheckedTransformer(solver_smt_map, CLI::ignore_case))
      ->group("BOA disassembling mode");

  m_config_boa_disassembling.assume_all_rets_genuine = false;
  app.add_flag("--assume-rets-genuine", m_config_boa_disassembling.assume_all_rets_genuine,
               "Assume that all RETs are genuine and do not compute targets with solver [false]")
      ->group("BOA disassembling mode");

  m_config_boa_disassembling.not_compute_dyn_jmps_targets = false;
  app.add_flag("--no-compute-dyn-jmps-targets", m_config_boa_disassembling.not_compute_dyn_jmps_targets,
               "Do not compute dyn jmps targets valuation [false]")
      ->group("BOA disassembling mode");

  m_config_boa_disassembling.disable_lib_fun_hooks = false;
  app.add_flag("--disable-lib-fun-hooks", m_config_boa_disassembling.disable_lib_fun_hooks,
               "Do not simulate external library functions calls [false]")
      ->group("BOA disassembling mode");
  
  m_config_boa_disassembling.load_dlls = false;
  app.add_flag("--load-dlls", m_config_boa_disassembling.load_dlls,
               "Load DLLs from IAT in memory space [false]")
      ->group("BOA disassembling mode");

  m_config_boa_disassembling.windows_dlls_json_filepath = "./bin/windows_dlls.json";
  app.add_option("--windows-dlls-json", m_config_boa_disassembling.windows_dlls_json_filepath, "Windows DLLs json file")
      ->check(CLI::ExistingFile)
      ->group("BOA disassembling mode");

  m_config_boa_disassembling.windows_dlls_dir = "./bin/dll";
  app.add_option("--windows-dlls-dir", m_config_boa_disassembling.windows_dlls_dir, "Windows DLLs directory")
      ->check(CLI::ExistingDirectory)
      ->group("BOA disassembling mode");

  std::vector<std::string> dyn_jmp_hooks_s = {};
  app.add_option("--dyn-jmp-hooks", dyn_jmp_hooks_s, "Dynamic jumps hooks (syntax: instr_addr-target_addr)")
      ->group("BOA disassembling mode");

  m_config_boa_disassembling.use_fake_val_for_symb_values = false;
  app.add_flag("--use-val-symb", m_config_boa_disassembling.use_fake_val_for_symb_values,
               "Use fake value for symbolic vairables [false]")
      ->group("BOA disassembling mode");

  std::vector<std::string> reg_values = {};
  app.add_option("--regs", reg_values, "Overload default register values for initial machine state (e.g.: EAX=0x1234 ESP=0x4321)")
  ->group("BOA disassembling mode");

  std::vector<std::string> mem_cell_values = {};
  app.add_option("--mem-cells", mem_cell_values, "Overload default memory cell values for initial machine state (e.g.: 0x11111111=0x12 0x11111112=0xAB)")
  ->group("BOA disassembling mode");


  // MARK: - Logging group
  std::vector<std::pair<std::string, spdlog::level::level_enum>> loglevel_map{
      {"trace", spdlog::level::trace},  {"debug", spdlog::level::debug}, {"info", spdlog::level::info},
      {"warning", spdlog::level::warn}, {"error", spdlog::level::err},   {"critical", spdlog::level::critical},
      {"off", spdlog::level::off}};

  spdlog::level::level_enum default_loglevel = spdlog::level::info;

  spdlog::level::level_enum loglevel_global = default_loglevel;
  app.add_option("--loglevel-global", loglevel_global,
                 "Global logging level (if not set, other module levels are set to this level) [info]")
      ->transform(CLI::CheckedTransformer(loglevel_map, CLI::ignore_case))
      ->group("Logging");

  spdlog::level::level_enum loglevel_core = default_loglevel;
  app.add_option("--loglevel-core", loglevel_core, "Log level core [info]")
      ->transform(CLI::CheckedTransformer(loglevel_map, CLI::ignore_case))
      ->group("Logging");

  spdlog::level::level_enum loglevel_bin_parser = default_loglevel;
  app.add_option("--loglevel-bin-parser", loglevel_bin_parser, "Log level binary parser module [info]")
      ->transform(CLI::CheckedTransformer(loglevel_map, CLI::ignore_case))
      ->group("Logging");

  spdlog::level::level_enum loglevel_disassembler = default_loglevel;
  app.add_option("--loglevel-disassembler", loglevel_disassembler, "Log level disassembler module [info]")
      ->transform(CLI::CheckedTransformer(loglevel_map, CLI::ignore_case))
      ->group("Logging");

  spdlog::level::level_enum loglevel_rec_disas = default_loglevel;
  app.add_option("--loglevel-rec-disas", loglevel_rec_disas, "Log level recursive disassembling module [info]")
      ->transform(CLI::CheckedTransformer(loglevel_map, CLI::ignore_case))
      ->group("Logging");

  spdlog::level::level_enum loglevel_boa_disas = default_loglevel;
  app.add_option("--loglevel-boa-disas", loglevel_boa_disas, "Log level BOA disassembling module [info]")
      ->transform(CLI::CheckedTransformer(loglevel_map, CLI::ignore_case))
      ->group("Logging");

  spdlog::level::level_enum loglevel_x86_to_smtlib = default_loglevel;
  app.add_option("--loglevel-x86-to-smtlib", loglevel_x86_to_smtlib, "Log level x86ToSmtlib module [info]")
      ->transform(CLI::CheckedTransformer(loglevel_map, CLI::ignore_case))
      ->group("Logging");

  spdlog::level::level_enum loglevel_solver = default_loglevel;
  app.add_option("--loglevel-solver", loglevel_solver, "Log level solver module [info]")
      ->transform(CLI::CheckedTransformer(loglevel_map, CLI::ignore_case))
      ->group("Logging");

  spdlog::level::level_enum loglevel_tainting = default_loglevel;
  app.add_option("--loglevel-tainting", loglevel_tainting, "Log level tainting module [info]")
      ->transform(CLI::CheckedTransformer(loglevel_map, CLI::ignore_case))
      ->group("Logging");

  // MARK:- Analysis group
  app.add_flag("--op-analysis", m_config_global.opaque_predicate_analysis,
               "Perform opaque predicate analysis after disassembly step [false]")
      ->group("Analysis");

  m_config_global.op_analysis_nb_thread = 4;
  app.add_option("--op-analysis-mt", m_config_global.op_analysis_nb_thread,
                 "Number of thread for opaque predicate analysis [4]")
      ->check(CLI::PositiveNumber)
      ->group("Analysis");

  // MARK:- Debug group
  app.add_flag("--test", m_config_global.test_mode, "Enable test mode (only for experimental purpose) [false]")
      ->group("Debug");

  app.add_flag("--compute-bbs-smtlib", m_config_global.all_bbs_to_smtlib,
               "Compute SMTLIB formula for all disassembled basic blocks (only for experimental purpose) [false]")
      ->group("Debug");

  app.add_flag("--dump-bbs-smtlib", m_config_global.dump_bbs_to_smtlib, "For each disassembled basic block, dump smtlib file (feature for Dylan) (only for experimental purpose) [false]")
      ->group("Debug");

  // Parse argv
  try
  {
    app.parse(argc, argv);
  }
  catch(const CLI::ParseError& e)
  {
    std::cout << "CLI error: " << e.get_name() << std::endl;
    throw app.exit(e);
  }

  // Copy addrs_not_to_disas vector in set
  for(auto addr : addrs_not_to_disas)
  {
    m_config_global.addrs_not_to_disas.insert(addr);
  }

  // Check for Radare2
  std::pair<int, std::string> result = utils::exec_shell_command("which radare2");
  if(result.first != 0)
  {
    throw std::runtime_error("Radare2 is needed to run BOA but it is not found in your PATH, see "
                             "https://github.com/radare/radare2 to install it");
  }

  // Dyn jmp hooks
  for(const auto& dyn_jmp_hook_s : dyn_jmp_hooks_s)
  {
    std::vector<std::string> splitted_s = utils::string_splitter(dyn_jmp_hook_s, "-");
    addr_t src = strtoul(splitted_s.at(0).c_str(), NULL, 16);
    addr_t target = strtoul(splitted_s.at(1).c_str(), NULL, 16);
    m_config_boa_disassembling.dyn_jmp_hooks.insert({src, target});
  }

  // Reg values
  for(const auto& reg_value_s : reg_values)
  {
    std::vector<std::string> splitted_s = utils::string_splitter(reg_value_s, "=");
    Register reg = s2reg(splitted_s.at(0));
    uint32_t value = strtoul(splitted_s.at(1).c_str(), NULL, 16);
    m_config_boa_disassembling.user_reg_values[reg] = value;
  }

  // Mem cell values
  for(const auto& mem_cell_value_s : mem_cell_values)
  {
    std::vector<std::string> splitted_s = utils::string_splitter(mem_cell_value_s, "=");
    addr_t addr = strtoul(splitted_s.at(0).c_str(), NULL, 16);
    uint8_t value = (uint8_t)atoi(splitted_s.at(1).c_str());
    m_config_boa_disassembling.user_mem_cell_values[addr] = value;
  }

  // Create and register loggers
  if(app.count("--loglevel-core") == 0)
  {
    loglevel_core = loglevel_global;
  }
  if(app.count("--loglevel-bin-parser") == 0)
  {
    loglevel_bin_parser = loglevel_global;
  }
  if(app.count("--loglevel-disassembler") == 0)
  {
    loglevel_disassembler = loglevel_global;
  }
  if(app.count("--loglevel-rec-disas") == 0)
  {
    loglevel_rec_disas = loglevel_global;
  }
  if(app.count("--loglevel-boa-disas") == 0)
  {
    loglevel_boa_disas = loglevel_global;
  }
  if(app.count("--loglevel-x86-to-smtlib") == 0)
  {
    loglevel_x86_to_smtlib = loglevel_global;
  }
  if(app.count("--loglevel-solver") == 0)
  {
    loglevel_solver = loglevel_global;
  }
  if(app.count("--loglevel-tainting") == 0)
  {
    loglevel_tainting = loglevel_global;
  }

  // https://github.com/gabime/spdlog/wiki/3.-Custom-formatting
  // std::string loggers_pattern = "[%H:%M:%S.%e] [%=13n] [%^%L%$] %v";
  std::string loggers_pattern = "[%H:%M:%S.%e] %L [%=13n] %v";

  auto core_logger = spdlog::stdout_color_st(utils::core_logger);
  core_logger->set_level(loglevel_core);
  core_logger->set_pattern(loggers_pattern);

  auto bin_parser_logger = spdlog::stdout_color_st(utils::bin_parser_logger);
  bin_parser_logger->set_level(loglevel_bin_parser);
  bin_parser_logger->set_pattern(loggers_pattern);

  auto disassembler_logger = spdlog::stdout_color_st(utils::disassembler_logger);
  disassembler_logger->set_level(loglevel_disassembler);
  disassembler_logger->set_pattern(loggers_pattern);

  auto rec_disas_logger = spdlog::stdout_color_st(utils::rec_disas_logger);
  rec_disas_logger->set_level(loglevel_rec_disas);
  rec_disas_logger->set_pattern(loggers_pattern);

  auto boa_disas_logger = spdlog::stdout_color_st(utils::boa_disas_logger);
  boa_disas_logger->set_level(loglevel_boa_disas);
  boa_disas_logger->set_pattern(loggers_pattern);

  auto x86_to_smtlib_logger = spdlog::stdout_color_st(utils::x86_to_smtlib_logger);
  x86_to_smtlib_logger->set_level(loglevel_x86_to_smtlib);
  x86_to_smtlib_logger->set_pattern(loggers_pattern);

  auto solver_logger = spdlog::stdout_color_st(utils::solver_logger);
  solver_logger->set_level(loglevel_solver);
  solver_logger->set_pattern(loggers_pattern);

  auto tainting_logger = spdlog::stdout_color_st(utils::tainting_logger);
  tainting_logger->set_level(loglevel_tainting);
  tainting_logger->set_pattern(loggers_pattern);

  // Other
  m_config_global.bin_folderpath = utils::get_folder_path(m_config_global.bin_filepath);
  m_config_global.bin_filename = utils::get_filename(m_config_global.bin_filepath);
}

// MARK:- Getters
const config_global_t& Config::getConfigGlobal() const
{
  return m_config_global;
}

const config_boa_disassembling_t& Config::getConfigBoaDisassembling() const
{
  return m_config_boa_disassembling;
}

const config_rec_disassembling_t& Config::getConfigRecDisassembling() const
{
  return m_config_rec_disassembling;
}

// MARK:- Setters
void Config::addStartAddr(addr_t addr)
{
  m_config_global.start_addrs.push_back(addr);
}

} // namespace boa
