#ifndef CONFIG_H
#define CONFIG_H

#include <optional>
#include <string>

#include "binary/binary.hpp"
#include "boa_disassembling/boa_disassembling.hpp"
#include "recursive_disassembling/recursive_disassembling.hpp"
#include "utils/utils.hpp"

namespace boa
{

extern const char* core_logger;
extern const char* bin_parser_logger;
extern const char* disassembler_logger;
extern const char* rec_disas_logger;
extern const char* boa_disas_logger;
extern const char* x86_to_smtlib_logger;
extern const char* solver_logger;
extern const char* tainting_logger;

class Config
{
public:
  Config(int argc, char* argv[]);

  const config_global_t& getConfigGlobal() const;
  const config_boa_disassembling_t& getConfigBoaDisassembling() const;
  const config_rec_disassembling_t& getConfigRecDisassembling() const;

  void addStartAddr(addr_t addr);

private:
  config_global_t m_config_global;
  config_rec_disassembling_t m_config_rec_disassembling;
  config_boa_disassembling_t m_config_boa_disassembling;
};

} // namespace boa

#endif
