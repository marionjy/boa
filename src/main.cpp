/**
 *
 * \file main.cpp
 * \author Sylvain CECCHETTO
 * Contact: sylvain.cecchetto at loria.fr
 *
 */

#include "config.hpp"
#include "analysis/opaque_predicate.hpp"
#include "binary/binary_parser_r2.hpp"
#include "boa_disassembling/boa_disassembling.hpp"
#include "boa_disassembling/solver/solver_manager.hpp"
#include "common/cfg_manager.hpp"
#include "disassembler/disassembler_capstone.hpp"
#include "recursive_disassembling/recursive_disassembling.hpp"
#include "test.hpp"
#include "utils/utils.hpp"
#include "x86_to_smtlib/x86_to_smtlib_binsec.hpp"

#include <chrono>
#include <iostream>
#include <signal.h>

extern const char* boa::utils::core_logger;

namespace boa
{

void handleResults(Config* config, std::shared_ptr<spdlog::logger> log, bool is_boa_mode, bool is_static_mode,
                   SolverManager* solver_manager, const CFGManager& cfg_manager, BoaDisassembling* boa_disassembling,
                   const my_time_format_t& total_time)
{
  log->info("");
  log->info("🔷 Results of {}", config->getConfigGlobal().bin_filename);
  log->info("");

  // BOA case
  if(is_boa_mode)
  {
    // Solvers
    log->info("⚙️ Get symbolic variable valuation (by solvers/by saved "
              "result): {}/{}",
              solver_manager->getNumberGetSymVarValuationWithSolvers(),
              solver_manager->getNumberGetSymVarValuationWithSavedResults());
    log->info("⚙️ Check-sat: {}", solver_manager->getNumberCheckSat());
    log->info("⚙️ Get-value: {}", solver_manager->getNumberGetValue());

    log->info("⚠️ Write operation at an unknown location: {}",
              MachineStateSymbExec::getWriteAtUnknownLocationCnt());

    /*
    // Self modification
    if(config->getConfigBoaDisassembling().enable_self_modification_analysis)
    {
      if(boa_disassembling->getSelfModifiedAddrs().empty())
      {
        log->info("➡️ No self modification");
      }
      else
      {
        log->info("➡️ Self modification detected: ({})",
    boa_disassembling->getSelfModifiedAddrs().size()); for(const auto& addr :
    boa_disassembling->getSelfModifiedAddrs())
        {
          log->info("\t🔹 {:#x}", addr);
        }
      }

      if(boa_disassembling->getWrittenAddrsOnExecSections().empty())
      {
        log->info("➡️ No written address on executable section");
      }
      else
      {
        log->info("➡️ Written addresses on executable section: ({})",
    boa_disassembling->getWrittenAddrsOnExecSections().size()); for(const auto&
    addr : boa_disassembling->getWrittenAddrsOnExecSections())
        {
          log->info("\t🔹 {:#x}", addr);
        }
      }
    }
     */
  }

  const auto& cfgs_p = cfg_manager.getCFGs();
  for(const auto& cfg_p : cfgs_p)
  {
    unsigned int wave = cfg_p.first;
    const CFG* cfg = cfg_p.second.get();

    // Collect infos to print
    std::optional<const BoaDisassemblingWave*> boa_disassembling_wave = std::nullopt;
    if(is_boa_mode)
    {
      boa_disassembling_wave = boa_disassembling->getBoaDisassemblingWaves().at(wave).get();
    }

    auto static_calls = cfg->getInstrsWithSubType(InstrSubType::STATIC_CALL);
    auto rets = cfg->getInstrsWithSubType(InstrSubType::RET);
    auto dyn_jmps = cfg->getInstrsWithSubType(InstrSubType::DYN_JMP);
    auto dyn_calls = cfg->getInstrsWithSubType(InstrSubType::DYN_CALL);
    auto jccs = cfg->getInstrsWithSubType(InstrSubType::JCC);

    // Print results
    log->info("🔶 CFG of wave n°{}", wave);

    // Static CALLs
    log->info("\t➡️ Static CALLs: {}", static_calls.size());
    for(auto const& instr : static_calls)
    {
      log->info("\t\t🔹 {} --> Returnsite: {:#x}", instr->toStringLight(), instr->getNextAddr());
    }

    if(is_boa_mode)
    {
      // BOA stats
      log->info("\t⚙️ Symbolically executed basic blocks: {}", (*boa_disassembling_wave)->getSymbExecutedBbsCnt());
      log->info("\t⚙️ Symbolically executed instructions: {}",
                (*boa_disassembling_wave)->getSymbExecutedInstrsCnt());

      log->info("\t➡️ RETs (total/genuine/violated/partially "
                "unknown/unknown): {}/{}",
                rets.size(), (*boa_disassembling_wave)->getRetsResults());
      for(auto const& ret_result : (*boa_disassembling_wave)->getRetsResultsDetails())
      {
        log->info("\t\t🔹 {} --> {}", ret_result.first->toStringLight(), ret_result.second);
      }

      log->info("\t➡️ Dyn JMPs (total/concrete/partially unknown/unknown): {}/{}", dyn_jmps.size(),
                (*boa_disassembling_wave)->getDynJmpsResults());
      for(auto const& dyn_jmp_result : (*boa_disassembling_wave)->getDynJmpsResultsDetails())
      {
        log->info("\t\t🔹 {} --> {}", dyn_jmp_result.first->toStringLight(), dyn_jmp_result.second);
      }

      log->info("\t➡️ Dyn Calls (total/concrete/partially unknown/unknown): {}/{}", dyn_calls.size(),
                (*boa_disassembling_wave)->getDynCallsResults());
      for(auto const& dyn_call_result : (*boa_disassembling_wave)->getDynCallsResultsDetails())
      {
        log->info("\t\t🔹 {} --> {}", dyn_call_result.first->toStringLight(), dyn_call_result.second);
      }

      log->info("\t➡️ Jccs (total/both branches/target only/next mem only): {}/{}", jccs.size(),
                (*boa_disassembling_wave)->getJccsResults());
      for(auto const& jcc_result : (*boa_disassembling_wave)->getJccsResultsDetails())
      {
        log->info("\t\t🔹 {} --> {}", jcc_result.first->toStringLight(), jcc_result.second);
      }
    }

    else if(is_static_mode)
    {
      log->info("\t➡️ RETs: {}", rets.size());
      for(auto const& instr : rets)
      {
        log->info("\t\t🔹 {}", instr->toStringLight());
      }

      log->info("\t➡️ Dyn JMPs: {}", dyn_jmps.size());
      for(auto const& instr : dyn_jmps)
      {
        log->info("\t\t🔹 {}", instr->toStringLight());
      }

      log->info("\t➡️ Dyn Calls: {}", dyn_calls.size());
      for(auto const& instr : dyn_calls)
      {
        log->info("\t\t🔹 {}", instr->toStringLight());
      }

      log->info("\t➡️ Jccs: {}", jccs.size());
      for(auto const& instr : jccs)
      {
        log->info("\t\t🔹 {}", instr->toStringLight());
      }
    }

    // Print main disassembly results
    log->info("\t➡️ Basic blocks: {}", cfg->getNumberBasicBlocks());
    log->info("\t➡️ Instructions: {}", cfg->getNumberInstrs());
    log->info("\t➡️ Edges: {}", cfg->getNumberEdges());

    // If we have mutliple waves, build one CFG for each wave in addition to the
    // "big" CFG that contains all waves
    if(cfgs_p.size() > 1)
    {
      std::string cfg_filepath = config->getConfigGlobal().bin_folderpath + config->getConfigGlobal().bin_filename +
                                 "_wave_" + std::to_string(cfg_p.first) + "_" +
                                 utils::DisassemblyMode_to_string(config->getConfigGlobal().disas_mode) + "_CFG";

      // Generate CFG dot and PDF file
      utils::create_text_file(cfg->createDot(), cfg_filepath + ".dot");
      if(cfg->getBasicBlocks().size() < 2000)
      {
        utils::dot_to_pdf(cfg_filepath + ".dot", cfg_filepath + ".pdf");
        log->info("\t➡️ CFG: {}", cfg_filepath + ".pdf");
      }
    }

    if(is_boa_mode && cfgs_p.size() > 1)
    {
      auto total_time_wave = utils::milli2hr_min_sec_milli((*boa_disassembling_wave)->getTotalTime());
      log->info("\t🕑 Elapsed time for this wave (HH:mm:ss,ms): {:02d}:{:02d}:{:02d},{:03d}", total_time_wave.hr,
                total_time_wave.min, total_time_wave.sec, total_time_wave.milli);
    }

  } // loop on each CFG

  // Create filename_prefix to create text files with results (BBs, instrs,
  // edges, CFG, ...)
  std::string files_preffix = config->getConfigGlobal().bin_folderpath + config->getConfigGlobal().bin_filename + "_" +
                              utils::DisassemblyMode_to_string(config->getConfigGlobal().disas_mode) + "_";

  // Dump instructions and edges list in a text file
  cfg_manager.createInstrsAndEdgesListTextFile(files_preffix + "instrs.txt", files_preffix + "edges.txt");
  log->info("➡️ Disassembled instructions list: {}", files_preffix + "instrs.txt");
  log->info("➡️ CFG edges list: {}", files_preffix + "edges.txt");

  // Dump BBs list in text file
  cfg_manager.createBBsListTextFile(files_preffix + "BBs.txt");
  log->info("➡️ Disassembled BBs list: {}", files_preffix + "BBs.txt");

  // Generate CFG dot and PDF file
  utils::create_text_file(cfg_manager.createDot(), files_preffix + "CFG.dot");
  if(cfg_manager.getDisassembledInstrsSize() < 1000)
  {
    utils::dot_to_pdf(files_preffix + "CFG.dot", files_preffix + "CFG.pdf");
    log->info("➡️ CFG (with all waves): {}", files_preffix + "CFG.pdf");
  }

  const auto& messages_stack = utils::getMessagesStack();
  if(!messages_stack.empty())
  {
    log->info("➡️ Important messages:");
    for(const auto& m : messages_stack)
    {
      log->info("\t🔹 {}", m);
    }
  }

  log->info("🕑 Elapsed time (HH:mm:ss,ms): {:02d}:{:02d}:{:02d},{:03d}", total_time.hr, total_time.min, total_time.sec,
            total_time.milli);
}

void printCsvResults(Config* config, bool is_boa_mode, const CFGManager& cfg_manager,
                     BoaDisassembling* boa_disassembling, const my_time_format_t& total_time)
{
  for(const auto& cfg_p : cfg_manager.getCFGs())
  {
    unsigned int wave = cfg_p.first;
    const CFG* cfg = cfg_p.second.get();

    std::optional<const BoaDisassemblingWave*> boa_disassembling_wave = std::nullopt;
    if(is_boa_mode)
    {
      boa_disassembling_wave = boa_disassembling->getBoaDisassemblingWaves().at(wave).get();
    }

    std::vector<std::string> csv_titles{};
    std::vector<std::string> csv_result{};

    // Binary filename
    csv_titles.push_back("Binary");
    csv_result.push_back(config->getConfigGlobal().bin_filename);

    // Wave number
    csv_titles.push_back("Wave number");
    csv_result.push_back(std::to_string(cfg_p.first));

    // Number BB
    csv_titles.push_back("#BBs");
    csv_result.push_back(std::to_string(cfg->getNumberBasicBlocks()));

    // Number instructions
    csv_titles.push_back("#Instrs");
    csv_result.push_back(std::to_string(cfg->getNumberInstrs()));

    // Number edges
    csv_titles.push_back("#Edges");
    csv_result.push_back(std::to_string(cfg->getNumberEdges()));

    // RET (total, genuine, violated, partialy unknown, unknwon)
    csv_titles.push_back("#RETs");
    csv_result.push_back(std::to_string(cfg->getNumberOfInstrWithSubType(InstrSubType::RET)));
    if(is_boa_mode)
    {
      csv_titles.push_back("#Genuine RETs");
      csv_result.push_back(std::to_string((*boa_disassembling_wave)->getRetsGenuine().size()));
      csv_titles.push_back("#Violated RETs");
      csv_result.push_back(std::to_string((*boa_disassembling_wave)->getRetsViolated().size()));
      csv_titles.push_back("#Partially unknown RETs");
      csv_result.push_back(std::to_string((*boa_disassembling_wave)->getRetsPartiallyUnknown().size()));
      csv_titles.push_back("#Unknown RETs");
      csv_result.push_back(std::to_string((*boa_disassembling_wave)->getRetsUnknown().size()));
    }

    // Dyn JMP (total, concrete, partialy unknown, unknwon)
    csv_titles.push_back("#Dyn JMPs");
    csv_result.push_back(std::to_string(cfg->getNumberOfInstrWithSubType(InstrSubType::DYN_JMP)));
    if(is_boa_mode)
    {
      csv_titles.push_back("#Concrete dyn JMPs");
      csv_result.push_back(std::to_string((*boa_disassembling_wave)->getDynJmpsConcrete().size()));
      csv_titles.push_back("#Partially unknown dyn JMPs");
      csv_result.push_back(std::to_string((*boa_disassembling_wave)->getDynJmpsPartiallyUnknown().size()));
      csv_titles.push_back("#Unknown dyn JMPs");
      csv_result.push_back(std::to_string((*boa_disassembling_wave)->getDynJmpsUnknown().size()));
    }

    // Dyn Call (total, concrete, partialy unknown, unknwon)
    csv_titles.push_back("#Dyn Calls");
    csv_result.push_back(std::to_string(cfg->getNumberOfInstrWithSubType(InstrSubType::DYN_CALL)));
    if(is_boa_mode)
    {
      csv_titles.push_back("#Concrete dyn Calls");
      csv_result.push_back(std::to_string((*boa_disassembling_wave)->getDynCallsConcrete().size()));
      csv_titles.push_back("#Partially unknown dyn Calls");
      csv_result.push_back(std::to_string((*boa_disassembling_wave)->getDynCallsPartiallyUnknown().size()));
      csv_titles.push_back("#Unknown dyn Calls");
      csv_result.push_back(std::to_string((*boa_disassembling_wave)->getDynCallsUnknown().size()));
    }

    // Jcc (total, both branches, only target, only next memory)
    csv_titles.push_back("#JCCs");
    csv_result.push_back(std::to_string(cfg->getNumberOfInstrWithSubType(InstrSubType::JCC)));
    if(is_boa_mode)
    {
      csv_titles.push_back("#JCCs both branches");
      csv_result.push_back(std::to_string((*boa_disassembling_wave)->getJccsBothBranches().size()));
      csv_titles.push_back("#JCCs only target");
      csv_result.push_back(std::to_string((*boa_disassembling_wave)->getJccsOnlyTarget().size()));
      csv_titles.push_back("#JCCs only next mem addr");
      csv_result.push_back(std::to_string((*boa_disassembling_wave)->getJccsOnlyNextMemAddr().size()));
    }

    if(is_boa_mode)
    {
      // Total write at unknown location
      csv_titles.push_back("#Write at unknown location");
      csv_result.push_back(std::to_string(MachineStateSymbExec::getWriteAtUnknownLocationCnt()));

      // Self modification (detected self modification, written addresses on
      // exec section)
      // csv_result.push_back(std::to_string((*boa_disassembling_wave)->getSelfModifiedAddrs().size()));
      // csv_result.push_back(std::to_string((*boa_disassembling_wave)->getWrittenAddrsOnExecSections().size()));

      // Symb exec counter (BB, instructions)
      csv_titles.push_back("#Symb exec BBs");
      csv_result.push_back(std::to_string((*boa_disassembling_wave)->getSymbExecutedBbsCnt()));
      csv_titles.push_back("#Symb exec instrs");
      csv_result.push_back(std::to_string((*boa_disassembling_wave)->getSymbExecutedInstrsCnt()));

      if(cfg_manager.getCFGs().size() > 1)
      {
        // Total time for this wave
        auto total_time_wave = utils::milli2hr_min_sec_milli((*boa_disassembling_wave)->getTotalTime());
        csv_titles.push_back("Time for this wave");
        csv_result.push_back(std::to_string(total_time_wave.hr).substr(0, 2) + ":" +
                             std::to_string(total_time_wave.min).substr(0, 2) + ":" +
                             std::to_string(total_time_wave.sec).substr(0, 2) + "," +
                             std::to_string(total_time_wave.milli).substr(0, 3));
      }
    }

    // Total time for this binary
    csv_titles.push_back("Time for binary");
    csv_result.push_back(
        std::to_string(total_time.hr).substr(0, 2) + ":" + std::to_string(total_time.min).substr(0, 2) + ":" +
        std::to_string(total_time.sec).substr(0, 2) + "," + std::to_string(total_time.milli).substr(0, 3));

    std::cout << "\nCSV TITLES: ";
    for(auto const& result : csv_titles)
    {
      std::cout << result << ";";
    }

    std::cout << "\nCSV RESULTS: ";
    for(auto const& result : csv_result)
    {
      std::cout << result << ";";
    }

    std::cout << std::endl;
  }
}

int run(Config* config, std::shared_ptr<spdlog::logger> log)
{
  const auto start_time = std::chrono::steady_clock::now();
  const bool is_boa_mode = config->getConfigGlobal().disas_mode == DisassemblyMode::BOA;
  const bool is_static_mode = config->getConfigGlobal().disas_mode == DisassemblyMode::STATIC;

  // Test mode
  if(config->getConfigGlobal().test_mode)
  {
    // If you want to try something,
    // put some code here and start BOA with --test argument
    return EXIT_SUCCESS;
  }

  log->info("🔷 Welcome to BOA v2!");

  // Create a binary parser
  std::unique_ptr<BinaryParser> bin_parser = std::make_unique<BinaryParserRadare2>();

  // Create Binary object by parsing the binary file header (get binary type,
  // section addresses, IAT, ...)
  auto binary = bin_parser->loadBinary(config->getConfigGlobal().bin_filepath);
  log->info("\n{}", binary->toString());

  // If user did not provide any start_addr to disassemble
  // then use the binary entry point
  if(config->getConfigGlobal().start_addrs.empty())
  {
    config->addStartAddr(binary->getEntryPoint());
  }

  // Create the disassembler engine
  std::unique_ptr<Disassembler> disas_engine;
  if(config->getConfigGlobal().disassembler == DisassemblerEngine::CAPSTONE)
  {
    disas_engine = std::make_unique<DisassemblerCapstone>(binary.get(), config->getConfigGlobal().addrs_not_to_disas);
  }
  else
  {
    throw std::runtime_error("Unknown disassembler engine");
  }

  // Create CFGManager object to manage CFG of each wave
  CFGManager cfg_manager = CFGManager();

  // Create the solver manager and the boaDisassembler (init only in BOA mode)
  std::unique_ptr<SolverManager> solver_manager;
  std::unique_ptr<BoaDisassembling> boa_disassembling;

  // Start the CFG building algortihm
  // If we are in static mode (simple static recursive algorithm)
  if(is_static_mode)
  {
    RecursiveDisassembling rec_disassembling = RecursiveDisassembling(
        disas_engine.get(), *binary, config->getConfigRecDisassembling(), config->getConfigGlobal());
    rec_disassembling.disassembleBinary(cfg_manager);
  }
  // If we are in BOA mode (static recursive algorithm + basic block symbolic execution)
  else if(is_boa_mode)
  {
    solver_manager = std::make_unique<SolverManager>(config->getConfigBoaDisassembling().config_solver);
    std::unique_ptr<x86ToSmtlib> x86_to_smtlib = std::make_unique<x86ToSmtlibBinsec>();
    boa_disassembling =
        std::make_unique<BoaDisassembling>(disas_engine.get(), binary.get(), x86_to_smtlib.get(), *solver_manager,
                                           *bin_parser, config->getConfigBoaDisassembling(), config->getConfigGlobal());
    boa_disassembling->disassembleBinary(cfg_manager);
  }

  // Only for experimental purpose (see --compute-bbs-smtlib argument)
  if(config->getConfigGlobal().all_bbs_to_smtlib)
  {
    x86ToSmtlibBinsec x86_to_smtlib;
    for(const auto& cfg_p : cfg_manager.getCFGs())
    {
      for(auto const& bb : cfg_p.second->getSortedBasicBlocks())
      {
        x86_to_smtlib.getMicroInstrs(&(*bb.second));
      }
    }
  }
  
  // Only for experiemntal purpose (see --dump-bbs-smtlib argument)
  if(config->getConfigGlobal().dump_bbs_to_smtlib)
  {
    x86ToSmtlibBinsec x86_to_smtlib;
    
    std::string folderpath = config->getConfigGlobal().bin_folderpath + "BBS_smtlib/";
    utils::exec_shell_command("mkdir " + folderpath);
    
    log->info("Save each basic block formula in {} folder", folderpath);

    MachineState machine_state = MachineState(binary.get(), 0x0);

    for(const auto& cfg_p : cfg_manager.getCFGs())
    {
      for(auto const& bb : cfg_p.second->getSortedBasicBlocks())
      {
        log->debug("\tDump SMTLIB formula of BB {:#x}", bb.first);
        // Ask BinSec for DBA instrs
        x86_to_smtlib.getMicroInstrs(&(*bb.second));

        // First, create a raw formula that is not valid for an SMT solver
        std::string formula;
        for(const auto& instr : bb.second->getInstrs())
        {
          for(const auto& micro_instr : *instr->getMicroInstrs())
          {
            formula += micro_instr->getSmtlibExpr() + "\n";
          }
        }
        std::string filepath = folderpath + waddr2s(bb.second->getEntryPoint()) + ".smt2";
        utils::create_text_file(formula, filepath);

        // Second, create a "real" formula that is valid for an SMT solver
        ExecFormula exec_formula = ExecFormula(&machine_state, bb.second);
        const auto& smtlib_formula = exec_formula.getSmtlibFormula(binary.get(), *solver_manager, false, false);
        filepath = folderpath + waddr2s(bb.second->getEntryPoint()) + "_full.smt2";
        utils::create_text_file(smtlib_formula.smtlib_text, filepath);
      }
    }
  }

  // If --op-analysis argument, start opaque predicate analysis
  if(config->getConfigGlobal().opaque_predicate_analysis)
  {
    SolverManager solver_manager = SolverManager(config->getConfigBoaDisassembling().config_solver, true,
                                                 config->getConfigGlobal().op_analysis_nb_thread);
    std::unique_ptr<x86ToSmtlib> x86_to_smtlib = std::make_unique<x86ToSmtlibBinsec>();
    for(const auto& cfg_p : cfg_manager.getCFGs())
    {
      OpaquePredicateAnalysis op_analysis =
          OpaquePredicateAnalysis(cfg_p.second.get(), solver_manager, binary.get(), x86_to_smtlib.get());
      op_analysis.startAnalysis(config->getConfigGlobal().op_analysis_nb_thread);
    }
  }

  const auto total_time = utils::milli2hr_min_sec_milli(
      std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start_time).count());

  // Print results to STDOUT and generate result files
  handleResults(config, log, is_boa_mode, is_static_mode, solver_manager.get(), cfg_manager, boa_disassembling.get(),
                total_time);

  // If --print-csv-results argument, print all results in CSV format (usefull for benchmarks)
  if(config->getConfigGlobal().print_csv_result)
  {
    printCsvResults(config, is_boa_mode, cfg_manager, boa_disassembling.get(), total_time);
  }

  return EXIT_SUCCESS;

} // run()

} // namespace boa

// MARK:- BOA platform entry point
int main(int argc, char* argv[])
{
  // Ignore SIGPIPE
  signal(SIGPIPE, SIG_IGN);

  // Parse user config
  std::unique_ptr<boa::Config> config;
  try
  {
    config = std::make_unique<boa::Config>(argc, argv);
  }
  catch(std::exception const& e)
  {
    std::cout << "[error] " << e.what() << std::endl;
    return EXIT_FAILURE;
  }
  catch(int const& r)
  {
    return r;
  }

  // Get core logger
  auto core_log = spdlog::get(boa::utils::core_logger);

  // Start BOA platform
  try
  {
    return boa::run(config.get(), core_log);
  }
  catch(std::exception const& e)
  {
    core_log->error("{}", e.what());
    return EXIT_FAILURE;
  }
}
