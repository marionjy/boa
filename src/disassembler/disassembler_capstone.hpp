
#ifndef DISASSEMBLER_CAPSTONE_H
#define DISASSEMBLER_CAPSTONE_H

#include <string>
#include <unordered_map>

#include <capstone/capstone.h>

#include "disassembler.hpp"
#include "common/instr.hpp"
#include "utils/utils.hpp"

namespace boa
{

class DisassemblerCapstone : public Disassembler
{

public:
  DisassemblerCapstone(Binary* binary, const std::unordered_set<addr_t>& addrs_not_to_disas);
  virtual ~DisassemblerCapstone();

  virtual DisasStatus disasInstr(Instr* instr, addr_t addr, const MachineState* machine_state = nullptr);

private:
  csh m_cs_handle;
  cs_insn* m_cs_insn;
};

} // namespace boa

#endif
