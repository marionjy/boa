
#include "disassembler_capstone.hpp"

#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

namespace boa
{
DisassemblerCapstone::DisassemblerCapstone(Binary* binary, const std::unordered_set<addr_t>& addrs_not_to_disas)
    : Disassembler(addrs_not_to_disas)
{
  SPDLOG_LOGGER_TRACE(m_log, "DisassemblerCapstone::DisassemblerCapstone()");
  m_binary = binary;
  cs_open(CS_ARCH_X86, CS_MODE_32, &m_cs_handle);
  cs_option(m_cs_handle, CS_OPT_DETAIL, CS_OPT_ON);
  m_cs_insn = cs_malloc(m_cs_handle);
}

DisassemblerCapstone::~DisassemblerCapstone()
{
  SPDLOG_LOGGER_TRACE(m_log, "DisassemblerCapstone::~DisassemblerCapstone()");
  cs_free(m_cs_insn, 1);
  cs_close(&m_cs_handle);
}

DisasStatus DisassemblerCapstone::disasInstr(Instr* instr, addr_t addr, const MachineState* machine_state)
{
  // Here, 'instr' should already be ready
  if(instr == nullptr)
  {
    throw std::runtime_error("Instr is nullptr, please alloc it before disasInstr call");
  }

  // Try to retrieve 15 (or less) bytes at @ 'addr' in binary file
  auto raw_offset = m_binary->vaddrToRawOffset(addr);
  size_t size = 15;

  if(raw_offset.has_value())
  {
    auto& ifstream = m_binary->getIfStream();
    unsigned int bin_length = m_binary->getLength();

    if(!ifstream.seekg((int64_t)*raw_offset))
    {
      throw std::runtime_error("Failed to move in offset " + addr2s(*raw_offset) + " in the binary file");
    }

    // Do not read in the file if *raw_offset is outside the file
    if(*raw_offset >= bin_length)
    {
      size = 0;
    }
    else
    {
      // Check if we can read 15 bytes, or maybe less
      if(*raw_offset + 15 > bin_length)
      {
        size = bin_length - *raw_offset;
      }

      // Read 'size' bytes in the binary file
      if(!ifstream.read(reinterpret_cast<char*>(&instr->m_bytes[0]), (long)size))
      {
        throw std::runtime_error("Failed to read " + std::to_string(size) + " bytes in binary file");
      }
    }
  }

  // Check for self-modified bytes
  if(machine_state != nullptr)
  {
    const auto& machine_state_mem_cells = machine_state->getMemCellValues();
    for(unsigned int i = 0; i < 15; i++)
    {
      if(machine_state_mem_cells.find(addr + i) != machine_state_mem_cells.end())
      {
        auto byte = machine_state_mem_cells.at(addr + i);
        if(byte.has_value())
        {
          instr->m_bytes[i] = *byte;
          // Adjust size value in case where less bytes was read from the file
          if(size < i + 1)
          {
            size = i + 1;
          }
        }
      }
    }
  }

  // Ask Capstone to disassemble 'size' bytes
  const uint8_t* code = &instr->m_bytes[0];
  if(!cs_disasm_iter(m_cs_handle, &code, &size, &addr, m_cs_insn))
  {
    // Capstone failed to disassemble these bytes
    return DisasStatus::FAIL;
  }

  cs_insn& cs_instr = m_cs_insn[0];
  instr->m_bytes.resize(cs_instr.size);
  instr->m_size = cs_instr.size;

  // We need to check that the freshly disassembled instr is not on written addresses (to detect a new wave entry)
  if(isInstrOnSelfModifiedAddrs(instr, machine_state))
  {
    return DisasStatus::SELF_MODIF;
  }

  instr->m_opcode = std::string(cs_instr.mnemonic) + " " + std::string(cs_instr.op_str);
  instr->m_type = InstrType::SEQ;
  instr->m_sub_type = InstrSubType::UNKNOWN;

  bool op_is_imm = true;

  // Iterate over operands
  bool at_least_one_op_reg = false;
  for(int n = 0; n < cs_instr.detail->x86.op_count; n++)
  {
    cs_x86_op* op = &(cs_instr.detail->x86.operands[n]);
    switch(op->type)
    {
    case X86_OP_IMM:
      // m_log->debug("\tX86_OP_IMM");
      instr->m_jmp_addr = op->imm;
      break;
    case X86_OP_MEM:
      // m_log->debug("\tX86_OP_MEM");
      instr->m_ptr_addr = op->mem.disp;
      op_is_imm = false;
      break;
    case X86_OP_REG:
      // m_log->debug("\tX86_OP_REG");
      op_is_imm = false;
      at_least_one_op_reg = true;
      break;
    default:
      // m_log->debug("\tdefault");
      break;
    }
  }

  if(at_least_one_op_reg)
  {
    instr->m_ptr_addr = std::nullopt;
  }

  /*
  // Check read regs
  cs_regs regs_read, regs_write;
  uint8_t regs_read_count, regs_write_count;
  if(!cs_regs_access(m_cs_handle, m_cs_insn, regs_read, &regs_read_count, regs_write, &regs_write_count))
  {
    if(regs_read_count)
    {
      instr->m_ptr_addr = std::nullopt;
    }
  }
   */

  for(int n = 0; n < cs_instr.detail->groups_count; n++)
  {
    int group = cs_instr.detail->groups[n];
    // m_log->info("Group: {}", group);
    if(group == X86_GRP_JUMP)
    {
      instr->m_type = InstrType::JMP;
      if(op_is_imm)
      {
        instr->m_sub_type = InstrSubType::STATIC_JMP;
      }
      else
      {
        instr->m_sub_type = InstrSubType::DYN_JMP;
      }
    }
    else if(group == X86_GRP_CALL)
    {
      instr->m_type = InstrType::CALL;
      if(op_is_imm)
      {
        instr->m_sub_type = InstrSubType::STATIC_CALL;
      }
      else
      {
        instr->m_sub_type = InstrSubType::DYN_CALL;
      }
      break;
    }
    else if(group == X86_GRP_RET)
    {
      instr->m_type = InstrType::RET;
      instr->m_sub_type = InstrSubType::RET;
      break;
    }
    else if(group == X86_GRP_INT)
    {
      instr->m_type = InstrType::TRAP;
      instr->m_sub_type = InstrSubType::TRAP;
    }
    else if(group == X86_GRP_IRET)
    {
    }
    else if(group == X86_GRP_PRIVILEGE)
    {
    }
    else if(group == X86_GRP_BRANCH_RELATIVE)
    {
      switch(cs_instr.id)
      {
      case X86_INS_JAE:
      case X86_INS_JA:
      case X86_INS_JBE:
      case X86_INS_JB:
      case X86_INS_JCXZ:
      case X86_INS_JECXZ:
      case X86_INS_JE:
      case X86_INS_JGE:
      case X86_INS_JG:
      case X86_INS_JLE:
      case X86_INS_JL:
      case X86_INS_JNE:
      case X86_INS_JNO:
      case X86_INS_JNP:
      case X86_INS_JNS:
      case X86_INS_JO:
      case X86_INS_JP:
      case X86_INS_JRCXZ:
      case X86_INS_JS:
      case X86_INS_LOOP:
      case X86_INS_LOOPE:
      case X86_INS_LOOPNE:
        instr->m_type = InstrType::JCC;
        instr->m_sub_type = InstrSubType::JCC;
        break;
      default:
        instr->m_type = InstrType::JMP;
        if(op_is_imm)
        {
          instr->m_sub_type = InstrSubType::STATIC_JMP;
        }
        else
        {
          instr->m_sub_type = InstrSubType::DYN_JMP;
        }
        break;
      }
      break;
    }
  }

  // Detect REP prefix
  switch(cs_instr.detail->x86.prefix[0])
  {
  case X86_PREFIX_REP:
  case X86_PREFIX_REPNE:
    instr->m_sub_type = InstrSubType::REP;
    break;
  default:
    break;
  }

  instr->m_next_addr = addr;

  return DisasStatus::SUCCESS;
}

} // namespace boa
