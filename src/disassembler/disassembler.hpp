#ifndef DISASSEMBLER_PARSER_H
#define DISASSEMBLER_PARSER_H

#include <string>

#include "binary/binary.hpp"
#include "boa_disassembling/machine_state.hpp"
#include "common/basic_block.hpp"
#include "common/cfg.hpp"
#include "common/instr.hpp"

namespace boa
{

enum class DisasStatus
{
  SUCCESS,
  SELF_MODIF,
  ADDR_NOT_IN_FILE,
  BLACKLISTED_ADDR,
  FAIL
};

class Disassembler
{
public:
  Disassembler(const std::unordered_set<addr_t>& addrs_not_to_disas);

  Disassembler() = delete;

  Disassembler(Disassembler const&) = delete;
  Disassembler& operator=(Disassembler const&) = delete;

  Disassembler(Disassembler&&) = delete;
  Disassembler& operator=(Disassembler&&) = delete;

  virtual ~Disassembler() = 0;

  bool isInstrOnSelfModifiedAddrs(const Instr* instr, const MachineState* machine_state) const;

  virtual DisasStatus disasInstr(Instr* instr, addr_t addr, const MachineState* machine_state = nullptr) = 0;

  DisasStatus disassembleBasicBlock(const BasicBlock*& bb, addr_t addr, unsigned int wave,
                                    EdgeFoundMethod edge_found_method, CFG* cfg,
                                    const MachineState* machine_state = nullptr);

  DisasStatus disassembleBasicBlock(const BasicBlock*& bb, addr_t addr, unsigned int wave,
                                    std::optional<const Instr*> ant_instr, EdgeFoundMethod edge_found_method, CFG* cfg,
                                    const MachineState* machine_state = nullptr);

  const std::set<std::pair<addr_t, addr_t>>& getSectionChanges() const;
  const std::set<addr_t>& getInstrsInSectionNotExecutable() const;

protected:
  std::shared_ptr<spdlog::logger> m_log;
  Binary* m_binary;

private:
  const std::unordered_set<addr_t>& m_addrs_not_to_disas;

  std::set<std::pair<addr_t, addr_t>> m_section_changes; // Save any section changes during disassembly
  std::set<addr_t> m_instrs_in_section_not_executable;
};

} // namespace boa

#endif
