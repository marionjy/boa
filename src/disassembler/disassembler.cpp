
#include "disassembler.hpp"

namespace boa
{

Disassembler::Disassembler(const std::unordered_set<addr_t>& addrs_not_to_disas)
    : m_addrs_not_to_disas(addrs_not_to_disas)
{
  m_log = spdlog::get(utils::disassembler_logger);
  SPDLOG_LOGGER_TRACE(m_log, "Disassembler::Disassembler()");
}

Disassembler::~Disassembler()
{
  SPDLOG_LOGGER_TRACE(m_log, "Disassembler::~Disassembler()");
}

bool Disassembler::isInstrOnSelfModifiedAddrs(const Instr* instr, const MachineState* machine_state) const
{
  if(machine_state != nullptr)
  {
    const auto& written_addrs_history = machine_state->getWrittenAddrsHistory();
    for(unsigned int i = 0; i < instr->getSize(); i++)
    {
      addr_t byte_addr = instr->getAddr() + i;
      if(written_addrs_history.find(byte_addr) != written_addrs_history.end())
      {
        return true;
      }
    }
  }
  return false;
}

DisasStatus Disassembler::disassembleBasicBlock(const BasicBlock*& bb, addr_t addr, unsigned int wave,
                                                EdgeFoundMethod edge_found_method, CFG* cfg,
                                                const MachineState* machine_state)
{
  return this->disassembleBasicBlock(bb, addr, wave, std::nullopt, edge_found_method, cfg, machine_state);
}

DisasStatus Disassembler::disassembleBasicBlock(const BasicBlock*& bb, addr_t addr, unsigned int wave,
                                                std::optional<const Instr*> ant_instr,
                                                EdgeFoundMethod edge_found_method, CFG* cfg,
                                                const MachineState* machine_state)
{
  SPDLOG_LOGGER_TRACE(m_log, "Disassembler::disassembleBasicBlock from addr {:#x}", addr);

  // 'disas_result' always hold the last disassembled address status
  DisasStatus disas_result = DisasStatus::SUCCESS;

  // If start_addr is the EP of an existing BB, no need to re-disassemble the BB, just take the existing one
  if(cfg->isBasicBlockExists(addr))
  {
    SPDLOG_LOGGER_DEBUG(m_log, "BasicBlock at address {:#x} already exists", addr);
    bb = cfg->getBasicBlock(addr);
  }

  // If start_addr is part of an existing BB, we need to split the existing BB
  else if(cfg->isInstrExists(addr))
  {
    bb = cfg->splitBB(m_log, {wave, addr});
  }

  // If at this point we already have the BB, we need to check for self-modification
  if(bb != nullptr)
  {
    // We need to check for self-modification if we have the machine_state
    if(machine_state != nullptr)
    {
      for(const auto& instr : bb->getInstrs())
      {
        if(isInstrOnSelfModifiedAddrs(instr.get(), machine_state))
        {
          if(instr->getAddr() == addr)
          {
            // The modified self-modified instr is the first instr of the BB
            bb = nullptr;
            m_log->warn("First instruction of BB {:#x} ({}) is not valid anymore due to self modification, no BB was "
                        "disassembled",
                        addr, instr->toStringLight());
            // Do not add edge in this case
            ant_instr = std::nullopt;
          }
          else
          {
            m_log->warn("Instruction {} of BB {:#x} is not valid anymore due to self modification. Split this BB to "
                        "isolate the modified instr and return the splitted BB {:#x}",
                        instr->toStringLight(), addr, addr);
            cfg->splitBB(m_log, instr->getWaddr());
          }
          disas_result = DisasStatus::SELF_MODIF;
          break;
        }
      }
    }

    // If we are here, we can add the edge
    if(ant_instr.has_value())
    {
      (*ant_instr)->addEdge(m_log, {wave, addr}, edge_found_method);
    }

    return disas_result;
  }

  // Else, we need to disassemble the BB starting at address 'addr'

  m_log->info("Disassemble BasicBlock at address {:#x}", addr);
  bool keep_disassembling = true;

  while(keep_disassembling)
  {

    // If any BB already contains an instruction at addr address we are done with the current BB
    if(cfg->isInstrExists(addr))
    {
      break;
    }

    // This is where the future disassembled instr will be stored
    std::unique_ptr<Instr> instr = nullptr;

    // If this addr is blacklisted, do not disassemble it
    if(m_addrs_not_to_disas.find(addr) != m_addrs_not_to_disas.end())
    {
      disas_result = DisasStatus::BLACKLISTED_ADDR;
    }

    // Else, try to disassemble it
    else
    {
      // If this is a PE and if we are in DLL world this instr is a fake external function entry point
      if(m_binary->getFormat() == BinaryFormat::PE && addr >= 0x0000000070000000ULL)
      {
        std::string func = "FAKE_LIB_FUNC (UNKNOWN)";
        // BOA mode
        if(machine_state != nullptr)
        {
          for(const auto& dll : machine_state->getLoadedDlls())
          {
            if(dll.second.binary != nullptr)
            {
              // Check if this addr if a function entry point of this DLL
              auto export_entry = dll.second.binary->getExportAtAddr(addr);
              if(export_entry != nullptr)
              {
                func = dll.first + "_" + export_entry->name;
                break;
              }
            }
            else
            {
              bool found = false;
              for(const auto& exported_function : dll.second.exported_functions)
              {
                if(exported_function.second == addr)
                {
                  found = true;
                  func = dll.first + "_" + exported_function.first;
                  break;
                }
              }
              if(found)
              {
                break;
              }
            }
          }
        }
        // Static mode
        else
        {
          auto import_entry = m_binary->getImportWithFuncAddr(addr);
          if(import_entry != nullptr)
          {
            func = import_entry->lib + "_" + import_entry->name;
          }
        }
        std::vector<uint8_t> bytes{};
        instr = std::make_unique<Instr>(func, bytes, InstrType::FAKE_LIB_FUNC, InstrSubType::FAKE_LIB_FUNC, addr, wave,
                                        0, addr, std::nullopt, std::nullopt);
        disas_result = DisasStatus::SUCCESS;
      }

      // Else if this is an ELF, we need to check if this instr is a fake external function entry point
      else if(m_binary->getFormat() == BinaryFormat::ELF)
      {
        auto import_entry = m_binary->getImportWithFuncAddr(addr);
        if(import_entry != nullptr)
        {
          std::vector<uint8_t> bytes{};
          instr = std::make_unique<Instr>(import_entry->name, bytes, InstrType::FAKE_LIB_FUNC,
                                          InstrSubType::FAKE_LIB_FUNC, addr, wave, 0, addr, std::nullopt, std::nullopt);
          disas_result = DisasStatus::SUCCESS;
        }
      }

      // If at this point instr is still nullptr, try to disassemble bytes at addr with the disassembler engine
      if(instr == nullptr)
      {
        //  // Create new instr to store the future disassembled instr at addr
        instr = std::make_unique<Instr>(addr, wave);
        disas_result = this->disasInstr(instr.get(), addr, machine_state);
      }
    }

    // We create the edge between this instr and the ant one
    if(ant_instr.has_value() && disas_result != DisasStatus::SELF_MODIF)
    {
      (*ant_instr)->addEdge(m_log, {wave, addr}, edge_found_method);
    }

    // Continue only if last addr was correctly disassembled
    keep_disassembling = false;
    if(disas_result == DisasStatus::SELF_MODIF)
    {
      m_log->warn("Didn't try to disassemble instruction at address {:#x} because we are on self modified addresses",
                  addr);
    }
    else if(disas_result == DisasStatus::BLACKLISTED_ADDR)
    {
      m_log->warn("Didn't try to disassemble instruction at address {:#x} because it is blacklisted", addr);
    }
    else if(disas_result == DisasStatus::FAIL)
    {
      m_log->warn("Disassembler engine failed to disassemble instruction at address {:#x}", addr);
    }

    else if(disas_result == DisasStatus::SUCCESS)
    {
      // Next edges of the BB will be STATIC
      edge_found_method = EdgeFoundMethod::STATIC_DISAS;

      // If were are here that means that we have a "correct" instruction
      keep_disassembling = true;

      bool add_instr_to_bb = true;

      ant_instr = instr.get();

      // TODO: check for overlap

      // If instr is a RDTSC we want to have it last in the BB
      static const std::vector<uint8_t> rdtsc_bytes = {0x0f, 0x31};
      if(instr->getBytes() == rdtsc_bytes)
      {
        keep_disassembling = false;
      }

      // If instr is a REP, we want to isolate it
      static const std::vector<uint8_t> bnd_ret_bytes = {0xf2, 0xc3};
      if(instr->getSubType() == InstrSubType::REP && instr->getBytes() != bnd_ret_bytes)
      {
        if(bb != nullptr)
        {
          add_instr_to_bb = false;
        }
        else
        {
          m_log->info("REP detected");
        }
        keep_disassembling = false;
      }

      // If instr is not SEQ this is the last one of this BB
      if(instr->getType() != InstrType::SEQ)
      {
        keep_disassembling = false;
      }

      // We want small (SMT solvers are too slow with too much instructions :-()
      if(bb != nullptr && bb->getSize() >= 50)
      {
        keep_disassembling = false;
      }

      if(keep_disassembling)
      {
        // La prochaine isntruction à désassembler est le seul target possible de l'instruciton précédente
        addr = instr->getNextAddr();
      }

      if(add_instr_to_bb)
      {
        // Print the disassembled instr added to BB
        m_log->info("\t{}", instr->toString());
        if(bb == nullptr)
        {
          bb = cfg->createBB(std::move(instr));
        }
        else
        {
          cfg->pushBackInstrInBB(bb, std::move(instr));
        }
      }
    }
    else
    {
      throw std::runtime_error("Need to implement this case of DisasStatus");
    }
  }

  if(bb == nullptr)
  {
    m_log->warn("No BB was disassembled at address {:#x}", addr);
  }

  return disas_result;
}

const std::set<std::pair<addr_t, addr_t>>& Disassembler::getSectionChanges() const
{
  return m_section_changes;
}

const std::set<addr_t>& Disassembler::getInstrsInSectionNotExecutable() const
{
  return m_instrs_in_section_not_executable;
}

} // namespace boa
