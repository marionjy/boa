#include "x86_to_smtlib.hpp"

namespace boa
{

x86ToSmtlib::x86ToSmtlib()
{
  m_log = spdlog::get(utils::x86_to_smtlib_logger);
  SPDLOG_LOGGER_TRACE(m_log, "x86ToSmtlib::x86ToSmtlib()");
}

x86ToSmtlib::~x86ToSmtlib()
{
  SPDLOG_LOGGER_TRACE(m_log, "x86ToSmtlib::~x86ToSmtlib()");
}

} // namespace boa
