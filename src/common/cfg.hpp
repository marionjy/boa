#ifndef CFG_H
#define CFG_H

#include <map>
#include <set>
#include <stdio.h>
#include <unordered_map>

#include "basic_block.hpp"
#include "boa_disassembling/exec_formula.hpp"
#include "utils/boa_types.hpp"

namespace boa
{

using UPtrExecFormula = std::unique_ptr<ExecFormula>;
using uptrConstBasicBlock = std::unique_ptr<const BasicBlock>;
using uptrConstExecFormula = std::unique_ptr<const ExecFormula>;

class CFG
{
public:
  // MARK:- Constructor
  CFG(const std::vector<addr_t>& entry_points, unsigned int wave);

  // MARK:- Getters and setters

  // Basic blocks
  const BasicBlock* getBasicBlock(const addr_t& addr) const;
  const BasicBlock* getBasicBlock(const Instr* instr) const;
  std::map<addr_t, const BasicBlock*> getSortedBasicBlocks() const;
  const std::unordered_map<addr_t, uptrConstBasicBlock>& getBasicBlocks() const;
  void insertBasicBlock(std::unique_ptr<const BasicBlock> basic_block);
  bool isBasicBlockExists(const addr_t& addr) const;
  bool isBasicBlockExists(const Instr* instr) const;
  const BasicBlock* getCorrespondingBasicBlock(const addr_t& instr_addr) const;
  // std::set<const BasicBlock*> getBasicBlockExecSuccs(const BasicBlock* bb) const;
  // std::set<const BasicBlock*> getBasicBlockExecAnts(const BasicBlock* bb) const;

  // Instrs
  // const Instr* getInstr(const addr_t &instr_addr) const;
  std::map<addr_t, const Instr*> getSortedInstrs() const;
  unsigned int getNumberInstrs() const;
  unsigned int getNumberOfInstrWithSubType(const InstrSubType& sub_type) const;
  std::set<const Instr*> getInstrsWithSubType(const InstrSubType& sub_type) const;
  bool isInstrExists(const addr_t& addr);
  const std::unordered_map<addr_t, const BasicBlock*>& getDisassembledInstrs() const;
  const BasicBlock* createBB(uptrConstInstr bb_ep_instr);
  void pushBackInstrInBB(const BasicBlock* bb, uptrConstInstr instr);
  void shrinkBB(std::shared_ptr<spdlog::logger> log, const BasicBlock* bb, waddr_t first_instr_to_remove);
  const BasicBlock* splitBB(std::shared_ptr<spdlog::logger> log, waddr_t new_bb_ep);

  // Entry point
  const std::unordered_set<addr_t>& getEntryPoints() const;

  // Other functions
  std::string createDotWithoutDigraph() const;
  std::string createDot() const;
  void createInstrsAndEdgesListTextFile(const std::string& instrs_list_filepath,
                                        const std::string& edges_list_filepath) const;
  void createBBsListTextFile(const std::string& filepath) const;

  // Other functions (stats)
  unsigned int getNumberBasicBlocks() const;
  unsigned int getNumberEdges() const;
  unsigned int getApproximateNumberInstrs() const;

  // Edges
  // std::set<const Edge*> getEdgesWithDst(const addr_t& dst) const;

private:
  std::unordered_set<addr_t> m_entry_points;
  unsigned int m_wave;

  // Disassembled BasicBlocks live here
  std::unordered_map<addr_t, uptrConstBasicBlock> m_basic_blocks;

  // Key: Disassembled instr addr
  // Value: Corresponding BB
  std::unordered_map<addr_t, const BasicBlock*> m_instrs_bb;
};

} // namespace boa

#endif /* CFG_H */
