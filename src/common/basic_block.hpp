#ifndef BASIC_BLOCK_H
#define BASIC_BLOCK_H

#include <unordered_map>
#include <vector>

#include "instr.hpp"
#include "utils/boa_types.hpp"
#include "utils/utils.hpp"

namespace boa
{

namespace bb
{

struct tainting_info_t
{
  std::set<Register> regs_read;
  std::set<Register> regs_write;
  std::set<const microinstr::mem_op_t*> mems_read;
  std::set<const microinstr::mem_op_t*> mems_write;
};

} // namespace bb

using uptrConstInstr = std::unique_ptr<const Instr>;

class BasicBlock
{
public:
  // MARK:- Constructors and destructors
  BasicBlock(const waddr_t& entry_point);
  ~BasicBlock() = default;

  // MARK:- Operators overloading
  friend bool operator==(const BasicBlock& lhs, const BasicBlock& rhs);
  friend bool operator!=(const BasicBlock& lhs, const BasicBlock& rhs);

  // MARK:- Getters and setters
  // Instrs
  const std::vector<uptrConstInstr>& getInstrs() const;
  // bool isContainsInstr(const addr_t &instr_addr) const;
  // const Instr* getInstr(const addr_t &instr_addr) const;
  const Instr* getLastInstr() const;
  size_t getSize() const;
  void pushBackInstr(uptrConstInstr instr) const;
  void pushFrontInstr(uptrConstInstr instr) const;
  uptrConstInstr popBackInstr() const;

  // Edges
  const Edge& getEdge(const waddr_t& dst) const;
  const std::map<waddr_t, uptrConstEdge>& getEdges() const;

  // Other
  const bb::tainting_info_t& getTaintingInfo() const;
  const waddr_t& getEntryPoint() const;
  const addr_t& getEntryPointAddr() const;
  std::string toString() const;
  bool isAllMicroInstrsAlreadyKnown() const;
  size_t getHash() const;

private:
  // MARK:- Private member variables
  waddr_t m_entry_point;
  mutable std::vector<uptrConstInstr> m_instrs;
  mutable std::optional<bb::tainting_info_t> m_tainting_info;
};

} // namespace boa

#endif
