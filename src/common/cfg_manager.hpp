#ifndef CFG_MANAGER_H
#define CFG_MANAGER_H

#include <map>
#include <set>
#include <stdio.h>
#include <unordered_map>

#include "cfg.hpp"

namespace boa
{

class CFGManager
{
public:
  // MARK:- The rule of six
  CFGManager() = default;

  CFGManager(CFGManager const&) = delete;
  CFGManager& operator=(CFGManager const&) = delete;

  CFGManager(CFGManager&&) = delete;
  CFGManager& operator=(CFGManager&&) = delete;

  ~CFGManager() = default;

  // MARK:- Getters and setters
  CFG* createEmptyCfg(const std::vector<addr_t>& entry_points);
  const std::map<unsigned int, std::unique_ptr<CFG>>& getCFGs() const;
  std::set<const Instr*> getInstrsWithSubType(const InstrSubType& sub_type) const;
  void createInstrsAndEdgesListTextFile(const std::string& instrs_list_filepath,
                                        const std::string& edges_list_filepath) const;
  void createBBsListTextFile(const std::string& filepath) const;
  std::string createDot() const;
  unsigned int getDisassembledInstrsSize() const;

private:
  // CFGs live here
  // Key: wave number, starts at 0
  std::map<unsigned int, std::unique_ptr<CFG>> m_cfgs;
};

} // namespace boa

#endif /* CFG_MANAGER_H */
