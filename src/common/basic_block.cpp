#include "basic_block.hpp"

namespace boa
{

// MARK:- Constructors and destructors
BasicBlock::BasicBlock(const waddr_t& entry_point) : m_entry_point(entry_point)
{
}

// MARK:- Operators overloading
bool operator==(const BasicBlock& lhs, const BasicBlock& rhs)
{
  // Compare instrs size
  if(lhs.m_instrs.size() != rhs.m_instrs.size())
  {
    return false;
  }

  // Compare each instr
  for(size_t i = 0; i < rhs.m_instrs.size(); i++)
  {
    if(lhs.m_instrs.at(i) != rhs.m_instrs.at(i))
    {
      return false;
    }
  }
  return true;
}

bool operator!=(const BasicBlock& lhs, const BasicBlock& rhs)
{
  return !(lhs == rhs);
}

// MARK:- Getters and setters
// Instrs
const std::vector<uptrConstInstr>& BasicBlock::getInstrs() const
{
  return m_instrs;
}

/*
bool BasicBlock::isContainsInstr(const addr_t& instr_addr) const
{
  for(const auto& instr : m_instrs)
  {
    if(instr_addr == instr->getAddr())
    {
      return true;
    }
  }
  return false;
}
 */

/*
const Instr* BasicBlock::getInstr(const addr_t &instr_addr) const
{
  for(const auto& instr : m_instrs)
  {
    if(instr_addr == instr->getAddr())
    {
      return instr.get();
    }
  }
  throw std::out_of_range("Instruction at address " + addr2s(instr_addr) + " not found in basic block " + toString());
}
*/

const Instr* BasicBlock::getLastInstr() const
{
  return m_instrs.back().get();
}

size_t BasicBlock::getSize() const
{
  return m_instrs.size();
}

void BasicBlock::pushBackInstr(uptrConstInstr instr) const
{
  m_instrs.push_back(std::move(instr));

  // If we push an instr, we need to clear tainting infos
  m_tainting_info = std::nullopt;
}

void BasicBlock::pushFrontInstr(uptrConstInstr instr) const
{
  m_instrs.insert(m_instrs.begin(), std::move(instr));

  // If we push an instr, we need to clear tainting infos
  m_tainting_info = std::nullopt;
}

uptrConstInstr BasicBlock::popBackInstr() const
{
  m_tainting_info = std::nullopt;
  uptrConstInstr back_instr = std::move(m_instrs.back());
  m_instrs.pop_back();
  return back_instr;
}

// Edges
const Edge& BasicBlock::getEdge(const waddr_t& dst) const
{
  return getLastInstr()->getEdge(dst);
}

const std::map<waddr_t, uptrConstEdge>& BasicBlock::getEdges() const
{
  return getLastInstr()->getEdges();
}

// Other
const bb::tainting_info_t& BasicBlock::getTaintingInfo() const
{
  if(!m_tainting_info.has_value())
  {
    std::shared_ptr<spdlog::logger> log = spdlog::get(utils::tainting_logger);

    m_tainting_info = std::make_optional<bb::tainting_info_t>();

    for(const auto& instr : m_instrs)
    {
      const auto& instr_tainting_info = instr->getTaintingInfo();

      m_tainting_info->regs_read.insert(instr_tainting_info.regs_read.begin(), instr_tainting_info.regs_read.end());
      m_tainting_info->regs_write.insert(instr_tainting_info.regs_write.begin(), instr_tainting_info.regs_write.end());
      m_tainting_info->mems_read.insert(instr_tainting_info.mems_read.begin(), instr_tainting_info.mems_read.end());
      m_tainting_info->mems_write.insert(instr_tainting_info.mems_write.begin(), instr_tainting_info.mems_write.end());
    }
  }
  return *m_tainting_info;
}

const waddr_t& BasicBlock::getEntryPoint() const
{
  return m_entry_point;
}

const addr_t& BasicBlock::getEntryPointAddr() const
{
  return m_entry_point.second;
}

std::string BasicBlock::toString() const
{
  return waddr2s(m_entry_point);
}

bool BasicBlock::isAllMicroInstrsAlreadyKnown() const
{
  for(const auto& instr : m_instrs)
  {
    if(!instr->getMicroInstrs().has_value())
    {
      return false;
    }
  }
  return true;
}

size_t BasicBlock::getHash() const
{
  // Hash of a basic block = hash of all instrs
  size_t hash{};
  for(auto const& instr : this->m_instrs)
  {
    hash = hash ^ instr->getHash();
  }
  return hash;
}

} // namespace boa
