#include "cfg.hpp"

#include <sstream>

namespace boa
{

// MARK:- Constructor
CFG::CFG(const std::vector<addr_t>& entry_points, unsigned int wave)
{
  m_entry_points.insert(entry_points.begin(), entry_points.end());
  m_wave = wave;
}

// MARK:- Getters and setters

// Basic blocks
const BasicBlock* CFG::getBasicBlock(const addr_t& addr) const
{
  if(!isBasicBlockExists(addr))
  {
    throw std::out_of_range("Basic block at address 0x" + addr2s(addr) + " not found");
  }
  return m_basic_blocks.at(addr).get();
}

const BasicBlock* CFG::getBasicBlock(const Instr* instr) const
{
  return getBasicBlock(instr->getAddr());
}

std::map<addr_t, const BasicBlock*> CFG::getSortedBasicBlocks() const
{
  std::map<addr_t, const BasicBlock*> bbs_map;
  for(const auto& bb_p : m_basic_blocks)
  {
    bbs_map.insert({bb_p.first, bb_p.second.get()});
  }
  return bbs_map;
}

const std::unordered_map<addr_t, uptrConstBasicBlock>& CFG::getBasicBlocks() const
{
  return m_basic_blocks;
}

bool CFG::isBasicBlockExists(const addr_t& addr) const
{
  return (m_basic_blocks.find(addr) != m_basic_blocks.end());
}

bool CFG::isBasicBlockExists(const Instr* instr) const
{
  return isBasicBlockExists(instr->getAddr());
}

bool CFG::isInstrExists(const addr_t& addr)
{
  return (m_instrs_bb.find(addr) != m_instrs_bb.end());
}

const std::unordered_map<addr_t, const BasicBlock*>& CFG::getDisassembledInstrs() const
{
  return m_instrs_bb;
}

const BasicBlock* CFG::createBB(uptrConstInstr bb_ep_instr)
{
  waddr_t bb_ep = bb_ep_instr->getWaddr();

  // Create new BB
  std::unique_ptr<BasicBlock> bb = std::make_unique<BasicBlock>(bb_ep);
  const BasicBlock* bb_ptr = bb.get();

  // Move instr in BB
  m_instrs_bb.insert({bb_ep.second, bb_ptr});
  bb_ptr->pushBackInstr(std::move(bb_ep_instr));

  // Add BB in CFG
  if(m_basic_blocks.find(bb_ep.second) != m_basic_blocks.end())
  {
    throw std::runtime_error("Basic block " + addr2s(bb_ep.second) + " already exists");
  }
  m_basic_blocks.insert({bb_ep.second, std::move(bb)});

  return bb_ptr;
}

void CFG::pushBackInstrInBB(const BasicBlock* bb, uptrConstInstr instr)
{
  m_instrs_bb.insert({instr->getAddr(), bb});
  bb->pushBackInstr(std::move(instr));
}

void CFG::shrinkBB(std::shared_ptr<spdlog::logger> log, const BasicBlock* bb, waddr_t first_instr_to_remove)
{
  log->info("Shrink existing BasicBlock {:#x} to remove all instruction after address {:#x} (including this one)",
            bb->getEntryPointAddr(), first_instr_to_remove.second);
  bool again = true;
  while(again)
  {
    uptrConstInstr removed_instr = bb->popBackInstr();

    // Remove instr from BB/instr map
    m_instrs_bb.erase(removed_instr->getAddr());

    if(removed_instr->getAddr() == first_instr_to_remove.second)
    {
      again = false;
    }
  }

  // Remove edges of last BB instr
  bb->getLastInstr()->removeEdge(first_instr_to_remove);

  log->info("Modified BasicBlock at address {:#x}", bb->getEntryPointAddr());
  for(auto const& instr : bb->getInstrs())
  {
    log->info("\t{}", instr->toString());
  }
}

const BasicBlock* CFG::splitBB(std::shared_ptr<spdlog::logger> log, waddr_t new_bb_ep)
{
  const BasicBlock* existing_bb = getCorrespondingBasicBlock(new_bb_ep.second);
  log->info("Split existing BasicBlock {:#x} at address {:#x} (the new BB entry point)",
            existing_bb->getEntryPointAddr(), new_bb_ep.second);

  // Create new BB with new_bb_ep
  std::unique_ptr<const BasicBlock> new_bb = std::make_unique<const BasicBlock>(new_bb_ep);
  const BasicBlock* new_bb_ptr = new_bb.get();

  // Transfer instrs from existing_bb to new_bb
  bool move_instrs_to_new_bb = true;
  while(move_instrs_to_new_bb)
  {
    // Pop back instr from exiting BB
    uptrConstInstr moving_instr = existing_bb->popBackInstr();

    // Check if we are at the split point
    if(moving_instr->getAddr() == new_bb_ep.second)
    {
      move_instrs_to_new_bb = false;
    }

    // Set correct BB/instr map
    m_instrs_bb[moving_instr->getAddr()] = new_bb_ptr;

    // Move instr in new_bb
    new_bb->pushFrontInstr(std::move(moving_instr));
  }

  // Add new BB in CFG
  if(m_basic_blocks.find(new_bb_ep.second) != m_basic_blocks.end())
  {
    throw std::runtime_error("Basic block " + addr2s(new_bb_ep.second) + " already exists");
  }
  m_basic_blocks.insert({new_bb_ep.second, std::move(new_bb)});

  // Print old and new BB after split operation
  log->info("Modified BasicBlock at address {:#x}:", existing_bb->getEntryPointAddr());
  for(auto const& instr : existing_bb->getInstrs())
  {
    log->info("\t{}", instr->toString());
  }
  log->info("New BasicBlock at address {:#x}:", new_bb_ep.second);
  for(auto const& instr : new_bb_ptr->getInstrs())
  {
    log->info("\t{}", instr->toString());
  }
  return new_bb_ptr;
}

const BasicBlock* CFG::getCorrespondingBasicBlock(const addr_t& instr_addr) const
{
  if(m_instrs_bb.find(instr_addr) != m_instrs_bb.end())
  {
    return m_instrs_bb.at(instr_addr);
  }
  throw std::runtime_error("Instruction with address " + addr2s(instr_addr) + " not found in any existing basic block");
}

/*
std::set<const BasicBlock*> CFG::getBasicBlockExecSuccs(const BasicBlock* bb) const
{
  std::set<const BasicBlock*> exec_succs = {};
  for(auto const& edge_pair : bb->getEdges())
  {
    exec_succs.insert(m_basic_blocks.at(edge_pair.first).get());
  }
  return exec_succs;
}
 */

/*
std::set<const BasicBlock*> CFG::getBasicBlockExecAnts(const BasicBlock* bb) const
{
  std::set<const BasicBlock*> exec_ants = {};
  for(auto const& bb_pair : m_basic_blocks)
  {
    if(bb_pair.second->getLastInstr()->getEdges().find(bb->getEntryPoint()) !=
bb_pair.second->getLastInstr()->getEdges().end())
    {
      exec_ants.insert(bb_pair.second.get());
    }
  }
  return exec_ants;
}
 */

/*
// Instrs
const Instr* CFG::getInstr(const addr_t &instr_addr) const
{
  return getCorrespondingBasicBlock(instr_addr)->getInstr(instr_addr);
}
 */

std::map<addr_t, const Instr*> CFG::getSortedInstrs() const
{
  std::map<addr_t, const boa::Instr*> sorted_instrs = {};
  for(auto const& bb_pair : m_basic_blocks)
  {
    for(auto const& instr : bb_pair.second->getInstrs())
    {
      if(instr->getType() != InstrType::FAKE_LIB_FUNC)
      {
        sorted_instrs.insert({instr->getAddr(), instr.get()});
      }
    }
  }

  return sorted_instrs;
}

unsigned int CFG::getNumberInstrs() const
{
  unsigned int n = 0;
  for(auto const& bb_pair : m_basic_blocks)
  {
    for(auto const& instr : bb_pair.second->getInstrs())
    {
      if(instr->getType() != InstrType::FAKE_LIB_FUNC)
      {
        n++;
      }
    }
  }
  return n;
}

unsigned int CFG::getNumberOfInstrWithSubType(const InstrSubType& sub_type) const
{
  unsigned int n = 0;
  for(auto const& bb_pair : m_basic_blocks)
  {
    for(auto const& instr : bb_pair.second->getInstrs())
    {
      if(instr->getSubType() == sub_type)
      {
        n++;
      }
    }
  }
  return n;
}

std::set<const Instr*> CFG::getInstrsWithSubType(const InstrSubType& sub_type) const
{
  std::set<const Instr*> instrs{};
  for(auto const& bb_pair : m_basic_blocks)
  {
    for(auto const& instr : bb_pair.second->getInstrs())
    {
      if(instr->getSubType() == sub_type)
      {
        instrs.insert(instr.get());
      }
    }
  }
  return instrs;
}

// Entry point
const std::unordered_set<addr_t>& CFG::getEntryPoints() const
{
  return m_entry_points;
}

// Other functions (CFG)
std::string CFG::createDotWithoutDigraph() const
{
  std::stringstream f;

  // First, we add each BB without edges
  auto bbs = getSortedBasicBlocks();
  for(auto const& bb_pair : bbs)
  {
    waddr_t bb_ep = {m_wave, bb_pair.first};
    const BasicBlock* bb = bb_pair.second;

    // Key: EP of the BB
    f << '"' << waddr2s(bb_ep) << "\"[label=\"";
    std::string bb_instrs = "";
    for(auto const& instr : bb->getInstrs())
    {
      bb_instrs += instr->toStringLight() + "\\l";
    }
    f << bb_instrs;

    f << "\",shape=box,style=\"rounded,filled\", fontweight=\"bold\",fillcolor=\"";
    if(m_entry_points.find(bb_ep.second) != m_entry_points.end())
    {
      f << "orange";
    }
    else if(bb->getLastInstr()->getSubType() == InstrSubType::FAKE_LIB_FUNC)
    {
      f << "cyan";
    }
    else
    {
      f << "white";
    }
    f << "\"];" << std::endl;
  }

  // Second, we add edges
  for(auto const& bb_pair : bbs)
  {
    std::string src = waddr2s({m_wave, bb_pair.first});
    for(auto const& edge_pair : bb_pair.second->getEdges())
    {
      std::string dst = waddr2s(edge_pair.first);

      // If STATIC DISAS --> black
      if(edge_pair.second->getFoundMethod() == EdgeFoundMethod::STATIC_DISAS)
      {
        f << '"' << src << "\" -> \"" << dst << "\";" << std::endl;
      }

      // If BOA DISAS --> blue
      else if(edge_pair.second->getFoundMethod() == EdgeFoundMethod::BOA_DISAS)
      {
        f << '"' << src << "\" -> \"" << dst << "\"[color=blue];" << std::endl;
      }
    }
  }

  // We add construction links between CALL and returnsite
  for(auto const& bb_pair : bbs)
  {
    auto bb_last_instr = bb_pair.second->getLastInstr();
    if(bb_last_instr->getType() == InstrType::CALL)
    {
      if(isBasicBlockExists(bb_last_instr->getNextAddr()))
      {
        std::string src = waddr2s({m_wave, bb_pair.first});
        std::string dst = waddr2s({m_wave, bb_last_instr->getNextAddr()});
        f << '"' << src << "\" -> \"" << dst << "\"[style=dotted];" << std::endl;
      }
    }
  }
  return f.str();
}

std::string CFG::createDot() const
{
  return "Digraph G {\n" + createDotWithoutDigraph() + "}";
}

void CFG::createInstrsAndEdgesListTextFile(const std::string& instrs_list_filepath,
                                           const std::string& edges_list_filepath) const
{
  std::string instrs_list;
  std::string edges_list;
  for(auto const& instr_p : getSortedInstrs())
  {
    instrs_list += instr_p.second->toString() + "\n";
    for(const auto& edge : instr_p.second->getEdges())
    {
      edges_list += waddr2s({m_wave, instr_p.first}) + " --> " + waddr2s(edge.first) + "\n";
    }
  }
  utils::create_text_file(instrs_list, instrs_list_filepath);
  utils::create_text_file(edges_list, edges_list_filepath);
}

void CFG::createBBsListTextFile(const std::string& filepath) const
{
  std::string bbs_list;
  for(auto const& bb : getSortedBasicBlocks())
  {
    bbs_list += bb.second->toString();
    bbs_list += " (size: " + std::to_string(bb.second->getSize()) + ")";
    bbs_list += " (first instr: " + bb.second->getInstrs().at(0)->toStringLight() + ")\n";
  }
  utils::create_text_file(bbs_list, filepath);
}

// Other functions (stats)
unsigned int CFG::getNumberBasicBlocks() const
{
  return m_basic_blocks.size();
}

unsigned int CFG::getNumberEdges() const
{
  size_t n = 0;
  for(auto const& bb_pair : m_basic_blocks)
  {
    n += bb_pair.second->getEdges().size();
  }
  return n;
}

/*
// Edges
std::set<const Edge*> CFG::getEdgesWithDst(const addr_t& dst) const
{
  std::set<const Edge*> edges {};
  for(auto const& bb_pair : m_basic_blocks)
  {
    if(bb_pair.second->getLastInstr()->getEdges().find(dst) != bb_pair.second->getLastInstr()->getEdges().end())
    {
      edges.insert(bb_pair.second->getLastInstr()->getEdges().at(dst).get());
    }
  }
  return edges;
}
 */

} // namespace boa
