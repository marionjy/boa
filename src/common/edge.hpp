#ifndef EDGE_H
#define EDGE_H

#include <map>
#include <set>
#include <unordered_map>
#include <vector>

#include "utils/boa_types.hpp"
#include "utils/utils.hpp"

namespace boa
{

enum class EdgeFoundMethod
{
  STATIC_DISAS,
  BOA_DISAS
};

class Edge
{
public:
  // MARK:- Constructors and destructors
  Edge(waddr_t src, waddr_t dst, EdgeFoundMethod found_method);

  //  Edge() = delete;

  //  Edge(Edge const&) = default;
  //  Edge& operator=(Edge const&) = default;
  //
  //  Edge(Edge&&) = default;
  //  Edge& operator=(Edge&&) = default;

  ~Edge() = default;

  // MARK:- Getters and setters
  const char* getFoundMethodString() const;
  EdgeFoundMethod getFoundMethod() const;
  waddr_t getSrc() const;
  waddr_t getDst() const;

  // MARK:- Static functions
  static const char* foundMethodToString(const EdgeFoundMethod& found_method);

private:
  waddr_t m_src;
  waddr_t m_dst;
  EdgeFoundMethod m_found_method;
};

} // namespace boa

#endif
