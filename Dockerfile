FROM ocaml/opam2:debian-10-ocaml-4.07

USER root

# Install dependencies
RUN apt-get --allow-releaseinfo-change update
RUN apt-get install -y cmake autoconf wget curl graphviz gperf libgmp-dev pkg-config python2.7 libzmq3-dev llvm-7-dev protobuf-compiler

# Install Radare2
RUN wget https://github.com/radareorg/radare2/archive/7ed581d2dee432c685420524f1b1eaa723db273d.zip -O r2.zip && \
unzip r2.zip && \
cd radare2-7ed581d2dee432c685420524f1b1eaa723db273d && \
sys/install.sh --install && \
cd .. && \
rm -rf r2.zip radare2-7ed581d2dee432c685420524f1b1eaa723db273d

# Install Capstone
RUN wget https://github.com/aquynh/capstone/archive/4.0.1.zip -O capstone_4.0.1.zip && \
unzip capstone_4.0.1.zip && \
cd capstone-4.0.1 && \
CAPSTONE_ARCHS="x86" CAPSTONE_X86_ATT_DISABLE=yes ./make.sh && \
./make.sh install && \
cd .. && \
rm -rf capstone_4.0.1.zip capstone-4.0.1

# Install Yices
RUN wget https://github.com/SRI-CSL/yices2/archive/afb9bfd5834b3ead4c9d00b5cad86cec643e73c6.zip -O yices2.zip && \
unzip yices2.zip && \
cd yices2-afb9bfd5834b3ead4c9d00b5cad86cec643e73c6 && \
autoconf && \
./configure && \
make && \
make install && \
cd .. && \
rm -rf yices2.zip yices2-afb9bfd5834b3ead4c9d00b5cad86cec643e73c6

# Update opam and install opam packages
RUN git config --global --add safe.directory /home/opam/opam-repository
RUN git -C /home/opam/opam-repository remote add http https://github.com/ocaml/opam-repository.git
RUN git -C /home/opam/opam-repository pull http master
RUN opam update
RUN opam install ocamlfind menhir.20200624 ocamlgraph piqi.0.7.7 zarith zmq llvm.7.0.0 yojson ounit qcheck seq


WORKDIR /app

RUN chown -R opam /app

USER opam

COPY --chown=opam . /app

# Compile BinSec and BOA
RUN mkdir build && \
cd build && \
eval $(opam env) && \
cmake -DCMAKE_BUILD_TYPE=Release .. && \
make binsec && \
make boa_v2

ENV PATH="${PATH}:/app/bin"

ENTRYPOINT ["/app/bin/boa_v2"]