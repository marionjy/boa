# Quelques notes de formalisations et optimisations

## Formalisation

### Register
* name (e.g. `EAX`)
* size (e.g. 32)

### MemCase
* address (e.g. `0x545`)

### VarState

* status (BOTTOM, TOP or CONCRETE)
* value *(only if status is CONCRETE)*

### MachineStateValuation (notée MSV)
* regs\_state (`map<Register, VarState>`)
* mem\_cases\_state (`map<MemCase, VarState>`)

Un objet *MachineStateValuation* est une valuation de l'état de la machine (donc une valuation de chaque registre et chaque case mémoire de la machine).

(e.g. `EAX = 3` $$\wedge$$ `EBX = TOP` $$\wedge$$ `0x545 = 0x34 `$$\wedge$$ `0x546 = BOTTOM`)

### BasicBlockFormula ($$\phi$$)

Pour un bloc de base **B**, on note $$\phi_B$$ la formule sémantique (au format SMTLIB) qui lui est associée. Cette dernière décrit fidèlement la sémantique de chaque instruction x86 qui compose le bloc de base.

### La fonction *valuation* (notée *val*)

État donné un bloc de base **B** et une valuation de l'état de la machine **MSV\_init**, la fonction *valuation* permet de calculer la valuation de l'état de la machine **MSV\_final** après l'exécution du bloc de base **B** avec pour état initial de la machine **MSV\_init**.

Si le bloc de base **B** a pour dernière instruction une instruction de type `Jcc`, il est possible, lors de la valuation, de volontairement contraindre la condition évaluée par le `Jcc` à `True` ou à `False`.
Cette option est utile dans le cas où l'on souhaite obtenir la valuation de l'état de la machine dans, par exemple, la branche "True" d'un `Jcc`.

**Cette fonction fait appel aux solvers SMT (c'est un appel coûteux en temps et en CPU !)**

Finalement la fonction *valuation* est donnée par :

* arg1: **MSV\_init** (*L'état initial de la machine*)
* arg2: $$\phi_B$$ (*La formule du bloc de base à évaluer*)
* arg3 (optional): `True` or `False` (*Si l'on souhaite contraindre la condition évaluée par le `Jcc`*)
* return: **MSV_final** (*L'état de la machine après exécution de B avec MSV\_init*)

## Optimisations

Comme l'appel de la fonction *val* est coûteux, voici les optimisations que nous pouvons utiliser afin de limiter la solicitation des solvers SMT :

### 1. Sauvegarder les résultats

À chaque fois que la fonction *val* est appelée nous gardons en cache la résultat obtenu pour les arguments donnés. Ainsi, si à l'avenir cette fonction est appelée à nouveau nous regardons dans le cache si cet appel n'a pas déjà eu lieu avec ces mêmes arguments, si tel est le cas, le résultat présent en cache est renvoyé et le solver SMT n'est pas sollicité.

### 2. Calcul des invariants de sortie des blocs de base

Dès qu'un nouveau bloc de base **B** est désassemblé, nous cherchons les variables (registres et cases mémoires) dont la valeur en sortie du bloc de base est toujours la même quel que soit l'état de la machine avant l'exécution de ce bloc de base.

Ces invariants sont calculés grâce à la fonction *val* de cette façon :

$$MSV\_inv = val(TOP, \phi_B)$$

Ainsi, pour n'importe quel appel à la fonction *val* avec $$\phi_B$$ et un état initial **MSV** quelconque :

Si `EAX` est `CONCRETE` dans **MSV\_inv** alors on pourra remplacer le calcul de

$$MSV\_final\{EAX\} = val(MSV\_init, \phi_B)\{EAX\}$$

par

$$MSV\_final\{EAX\} = MSV\_inv\{EAX\}$$

Ce qui évite une solicitation du solver SMT et donc un gain de temps.

### 3. Propagation des variables à travers les blocs de base

Grâce à BinSec nous pouvons savoir, pour chaque instruction d'un bloc de base, la liste des registres qu'elle lit et/ou qu'elle modifie. Nous pouvons également savoir si la mémoire est lu et/ou écrite.

Ainsi, pour un bloc de base entier nous avons la liste des registres lus et, la liste des registres modifiés et également la possibilité de savoir si la mémoire et lue et/ou modifiée.

Ces informations nous donnent la possibilité d'économiser des appels au solver SMT.

Ainsi, pour n'importe quel appel à la fonction *val* avec $$\phi_B$$ et un état initial **MSV** quelconque :

Si `EAX` est n'est pas modifié par **B**, alors on pourra remplacer le calcul de

$$MSV\_final\{EAX\} = val(MSV\_init, \phi_B)\{EAX\}$$

par

$$MSV\_final\{EAX\} = MSV\_init\{EAX\}$$

**Cependant** si B se termine par une instruction `Jcc` ET que la condition du `Jcc` est contrainte dans *val* alors la simplification donnée ci-dessus n'est possible que si `EAX` n'est pas modifié par **B** ET qu'il n'est pas lu non plus par **B**.

### 4. Simplification de l'état initial de la machine utilisé dans *val*

Étant donné MSV\_init1 = `EAX = 3` $$\wedge$$ `EBX = 4`.  
Étant donné MSV\_init2 = `EAX = 3` $$\wedge$$ `EBX = 5`.  
Étant donné un bloc de base **B**.

Imaginons que devons calculer, pour le bloc de base **B**, MSV\_final1 à partir de MSV\_init1 ainsi que MSV\_final2 à partir de MSV\_init2.  
Nous effectuons alors les deux calculs $$MSV\_final1 = val(MSV\_init1, \phi_B)$$ et $$MSV\_final2 = val(MSV\_init2, \phi_B)$$.

Nous pouvons remarquer que dans ce cas le solver SMT est sollicité deux fois, une première fois pour le calcul de MSV\_final1 et une seconde fois pour le calcul de MSV\_final2. En effet, il est impossible d'utiliser le résultat de MSV\_final1 présent en cache pour MSV\_final2 car *val* n'est pas appelé avec les mêmes arguments.

**Cependant**, si nous remarquons que **B** "n'utilise pas" la valeur de `EBX` présente en entrée (autrement dit, si **B** ne lit jamais `EBX` ou bien si **B** effectue au moins une lecture sur `EBX` mais qu'il écrit sur `EBX` avant la première lecture) l'information sur `EBX` présente dans l'état de la machine initial est inutile car "inutilisée" par **B**.

Nous pouvons alors simplifier MSV\_init1 et MSV\_init2 par :  

* MSV\_init1' = `EAX = 3`.  
* MSV\_init2' = `EAX = 3`.

Finalement MSV\_init1' et MSV\_init2' sont maintenant égaux, donc le calcul de MSV\_final2 par le solver SMT ne sera pas nécessaire car le résultat en cache de MSV\_final1 pour être utilisé.






 




