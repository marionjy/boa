# Fonctionnement de la gestion des boucles

* Au lancement de BOA on peut donner en argument avec l'option `-l` le nombre max de tour qu'on peut faire dans les boucles (on note ce nombre `kmax`)
* On part sur le fait que les instructions qui peuvent engendrer une boucle sont :
    * Les sauts conditionnels
    * Les sauts dynamiques de façon général (`JMP EAX`, `CALL EAX`, `CALL [EAX]`, `RET`, ...)
* Ici, on va appeler ces instructions des instructions de "bouclage"
* À chaque fois qu'on exécute (symboliquement) un bloc de base finissant par une instruction de bouclage :
    * On incrémente un compteur pour chaque adresse cible de ce bloc de base
    * Si le compteur atteint `kmax` on va sauvegarder l'environnement machine de sortie dans un coin
    * Si le compteur a dépassé `kmax` alors on va comparer l'environnement actuel avec le dernier sauvegardé
        * Si ils sont identiques alors on n'ajoute pas ce successeur
        * Sinon on fusionne les deux environnements et on continu