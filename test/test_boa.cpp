#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>

#include "../src/Register.h"
#include "../src/MemoryCase.h"
#include "../src/MachineState.h"

namespace boa
{
  TEST_CASE("Register", "[register]")
  {
    Register eax1 = Register(RegisterName::EAX);
    Register eax2 = Register(RegisterName::EAX);
    Register ebx = Register(RegisterName::EBX);
    
    // Equality
    REQUIRE(eax1 == eax2);
    REQUIRE(eax1 != ebx);
    
    // Comparaison
    REQUIRE(eax1 < ebx);
    
    // Getters
    REQUIRE(eax1.getSize() == 32);
    REQUIRE(eax1.getHash() == 0);
    REQUIRE(ebx.getHash() == 1);
  }
  
  TEST_CASE("RegisterState", "[register_state]")
  {
    RegisterState rs1 = RegisterState();
    RegisterState rs2 = RegisterState();
    RegisterState rs3 = RegisterState();
    
    // Setters
    rs1.setStatus(RegisterStateStatus::BOTTOM);
    rs2.setStatus(RegisterStateStatus::TOP);
    rs3.setStatus(RegisterStateStatus::CONCRETE);
    rs3.setValues({0x1, 0x2});
    
    // Getters
    REQUIRE(rs1.getStatus() == RegisterStateStatus::BOTTOM);
    REQUIRE(rs2.getStatus() == RegisterStateStatus::TOP);
    std::unordered_set<addr_t> v = {0x2, 0x1};
    REQUIRE(rs3.getValues() == v);
    REQUIRE_THROWS(rs1.getValues());
    REQUIRE_THROWS(rs2.getValues());
    
    // Equality
    RegisterState rs4 = RegisterState();
    rs4.setStatus(RegisterStateStatus::BOTTOM);
    RegisterState rs5 = RegisterState();
    rs5.setStatus(RegisterStateStatus::CONCRETE);
    REQUIRE(rs1 == rs4);
    REQUIRE(rs1 != rs2);
    REQUIRE(rs3 != rs5);
    
    rs5.setValues({0x2, 0x1});
    REQUIRE(rs3 == rs5);
    
    rs5.setValues({0x3, 0x1});
    REQUIRE(rs3 != rs5);
    
    // Hash
    RegisterState rs6 = RegisterState();
    RegisterState rs7 = RegisterState();
    rs6.setStatus(RegisterStateStatus::BOTTOM);
    rs7.setStatus(RegisterStateStatus::BOTTOM);
    REQUIRE(rs6.getHash() == rs7.getHash());
    
    rs6.setValues({0x1, 0x6});
    REQUIRE(rs6.getHash() == rs7.getHash());
    
    rs6.setStatus(RegisterStateStatus::CONCRETE);
    REQUIRE(rs6.getHash() != rs7.getHash());
    
    rs7.setValues({0x6, 0x1});
    REQUIRE(rs6.getHash() != rs7.getHash());
    
    rs7.setStatus(RegisterStateStatus::CONCRETE);
    REQUIRE(rs6.getHash() == rs7.getHash());
  }
  
  TEST_CASE("MemoryCase", "[mem_case]")
  {
    MemoryCase mc1 = MemoryCase(0x12345678);
    MemoryCase mc2 = MemoryCase(0x12345678);
    MemoryCase mc3 = MemoryCase(0x87654321);
    
    // Equality
    REQUIRE(mc1 == mc2);
    REQUIRE(mc1 != mc3);
    
    // Comparaison
    REQUIRE(mc1 < mc3);
    
    // Getters
    REQUIRE(mc1.getAddr() == 0x12345678);
    REQUIRE(mc1.getHash() == 0x12345678);
    REQUIRE(mc3.getHash() == 0x87654321);
  }
  
  TEST_CASE("MemoryCaseState", "[memory_case_state]")
  {
    MemoryCaseState mcs1 = MemoryCaseState();
    MemoryCaseState mcs2 = MemoryCaseState();
    MemoryCaseState mcs3 = MemoryCaseState();
    
    // Setters
    mcs1.setStatus(MemoryCaseStateStatus::BOTTOM);
    mcs2.setStatus(MemoryCaseStateStatus::TOP);
    mcs3.setStatus(MemoryCaseStateStatus::CONCRETE);
    mcs3.setValues({0x1, 0x2});
    
    // Getters
    REQUIRE(mcs1.getStatus() == MemoryCaseStateStatus::BOTTOM);
    REQUIRE(mcs2.getStatus() == MemoryCaseStateStatus::TOP);
    std::unordered_set<uint8_t> v = {0x2, 0x1};
    REQUIRE(mcs3.getValues() == v);
    REQUIRE_THROWS(mcs1.getValues());
    REQUIRE_THROWS(mcs2.getValues());
    
    // Equality
    MemoryCaseState mcs4 = MemoryCaseState();
    mcs4.setStatus(MemoryCaseStateStatus::BOTTOM);
    MemoryCaseState mcs5 = MemoryCaseState();
    mcs5.setStatus(MemoryCaseStateStatus::CONCRETE);
    REQUIRE(mcs1 == mcs4);
    REQUIRE(mcs1 != mcs2);
    REQUIRE(mcs3 != mcs5);
    
    mcs5.setValues({0x2, 0x1});
    REQUIRE(mcs3 == mcs5);
    
    mcs5.setValues({0x3, 0x1});
    REQUIRE(mcs3 != mcs5);
    
    // Hash
    MemoryCaseState mcs6 = MemoryCaseState();
    MemoryCaseState mcs7 = MemoryCaseState();
    mcs6.setStatus(MemoryCaseStateStatus::BOTTOM);
    mcs7.setStatus(MemoryCaseStateStatus::BOTTOM);
    REQUIRE(mcs6.getHash() == mcs7.getHash());
    
    mcs6.setValues({0x1, 0x6});
    REQUIRE(mcs6.getHash() == mcs7.getHash());
    
    mcs6.setStatus(MemoryCaseStateStatus::CONCRETE);
    REQUIRE(mcs6.getHash() != mcs7.getHash());
    
    mcs7.setValues({0x6, 0x1});
    REQUIRE(mcs6.getHash() != mcs7.getHash());
    
    mcs7.setStatus(MemoryCaseStateStatus::CONCRETE);
    REQUIRE(mcs6.getHash() == mcs7.getHash());
  }
  
  TEST_CASE("MachineState", "[machine_state]")
  {
    MachineState m1 = MachineState(MemoryCaseStateStatus::BOTTOM);
    MachineState m2 = MachineState(MemoryCaseStateStatus::BOTTOM);
    REQUIRE(m1 == m2);
    REQUIRE(m1.getHash() == m2.getHash());
    
    m1.setRegisterState(Register(RegisterName::EAX), RegisterStateStatus::CONCRETE, {0x1, 0x2});
    REQUIRE(m1 != m2);
    REQUIRE(m1.getHash() != m2.getHash());
    
    m2.setRegisterState(Register(RegisterName::EAX), RegisterStateStatus::CONCRETE, {0x2, 0x1});
    REQUIRE(m1 == m2);
    REQUIRE(m1.getHash() == m2.getHash());
    
    m1.setRegisterState(Register(RegisterName::EBX), RegisterStateStatus::BOTTOM, {});
    REQUIRE(m1 == m2);
    REQUIRE(m1.getHash() != m2.getHash());
    
    m2.setMemoryCaseState(MemoryCase(0x12345678), MemoryCaseStateStatus::BOTTOM, {});
    REQUIRE(m1 == m2);
    
    m2.setMemoryCaseState(MemoryCase(0x12345678), MemoryCaseStateStatus::CONCRETE, {0x5});
    REQUIRE(m1 != m2);
  
  }
}


