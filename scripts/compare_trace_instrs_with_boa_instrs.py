#!/usr/bin/env python3

import sys


if len(sys.argv) <= 2:
    print('Usage: python3 {} txt_boa_instrs_filepath txt_trace_filepath [wave_number]'.format(sys.argv[0]))
    exit(-1)


TXT_BOA_INSTRS_FILEPATH = sys.argv[1]
TXT_TRACE_FILEPATH = sys.argv[2]


wave_cnt = 0
if(len(sys.argv) == 4):
    wave_cnt = int(sys.argv[3])


print('')
print('# Compare instructions found by BOA with instructions in execution trace')
print('\t* Execution trace filepath: {} (wave: {})'.format(TXT_TRACE_FILEPATH, wave_cnt))
print('\t* Instructions found by BOA: {}'.format(TXT_BOA_INSTRS_FILEPATH))
print('')

# Parse trace filepath
trace_unique_instrs = dict()
total_instrs_in_trace = 0

keyword = str(wave_cnt) + '_0x'

f = open(TXT_TRACE_FILEPATH)
for line in f:
    splitted_line = line.split()
    if len(splitted_line) >= 1:
        addr = splitted_line[0]
        if keyword in addr:
            addr = addr.split('_')[1]
            addr = int(addr, 0)
            trace_unique_instrs[addr] = line.strip('\n')
            total_instrs_in_trace = total_instrs_in_trace + 1



# Parse boa instrs
boa_unique_instrs = dict()

f = open(TXT_BOA_INSTRS_FILEPATH)
for line in f:
    splitted_line = line.split()
    if len(splitted_line) >= 1:
        addr = splitted_line[0]
        if '0x' in addr:
            addr = int(addr, 0)
            boa_unique_instrs[addr] = line.strip('\n')

diff = len(boa_unique_instrs) - len(trace_unique_instrs)


print('* Instructions found in execution trace but not by BOA:')
instr_in_trace_but_in_boa = dict()
for trace_instr_addr, trace_instr in trace_unique_instrs.items():
    if trace_instr_addr not in boa_unique_instrs:
        print('\t- {}'.format(trace_instr))
        instr_in_trace_but_in_boa[trace_instr_addr] = trace_instr

if len(instr_in_trace_but_in_boa) == 0:
    print('\t- Ø')

print('* Instructions found by BOA but not in execution trace:')
instr_in_boa_but_in_trace = dict()
for boa_instr_addr, boa_instr in boa_unique_instrs.items():
    if boa_instr_addr not in trace_unique_instrs:
        print('\t- {}'.format(boa_instr))
        instr_in_boa_but_in_trace[boa_instr_addr] = boa_instr

if len(instr_in_boa_but_in_trace) == 0:
    print('\t- Ø')


print('')

print('* Trace size: {}'.format(total_instrs_in_trace))
print('* Unique instrs in trace: {}'.format(len(trace_unique_instrs)))
print('* Unique instrs with BOA: {}'.format(len(boa_unique_instrs)))
print('* Unique instrs in execution trace but not in BOA: {}'.format(len(instr_in_trace_but_in_boa)))
print('* Unique instrs in BOA but not in execution trace: {}'.format(len(instr_in_boa_but_in_trace)))
print('')
print('CSV RESULT {};{};{};{};{}'.format(total_instrs_in_trace, len(trace_unique_instrs), len(boa_unique_instrs), len(instr_in_trace_but_in_boa), len(instr_in_boa_but_in_trace)))
print('')
