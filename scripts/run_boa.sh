#!/usr/bin/env bash

# arg 1: print BOA stdout? (yes or no)
# arg 2: folderpath of the binary

if [ "$#" -lt 2 ]; then
    echo -e "\nusage: ./script/run_boa.sh print_stdout(yes or no) binary_folder_path [other BOA args]\n"
    echo -e "(e.g. ./scripts/run_boa.sh yes ./examples/my_binary)"
    exit 1
fi


BASEDIR=$(dirname "$0")
BASEDIR=$(pwd)
PRINT_STDOUT="$1"
BINARY_FOLDER="${2%/}"

# Remove unnecessary args
shift
shift

# Check if we are in the correct directory
if [ ! -d "./bin" ] || [ ! -d "./scripts" ]; then
    echo -e "\nFolder 'bin' or 'scripts' can not be found in your current directory"
    exit 1
fi


# Check for BOA and BinSec binaries filepath
BOA_BIN="./bin/boa_v2"
if [ ! -f "${BOA_BIN}" ]; then
    echo -e "\nBOA executable can not be found at './bin/boa_v2' location"
    exit 1
fi

BINSEC_BIN="./bin/binsec"
if [ ! -f "${BINSEC_BIN}" ]; then
    echo -e "\nBinSec executable can not be found at './bin/binsec' location"
    exit 1
fi

# Add bin folder in PATH so that BOA will find BinSec binary
export PATH=${BASEDIR}/bin:$PATH


BINARY_FILENAME=$(basename $BINARY_FOLDER)
CONFIG_FILEPATH=${BINARY_FOLDER}/config.ini
BINARY_FILEPATH=${BINARY_FOLDER}/${BINARY_FILENAME}

# Check if binary file exists
if [ ! -f "${BINARY_FILEPATH}" ]; then
    BINARY_FILEPATH="${BINARY_FOLDER}/${BINARY_FILENAME}.exe"
fi
if [ ! -f "${BINARY_FILEPATH}" ]; then
    BINARY_FILEPATH="${BINARY_FOLDER}/${BINARY_FILENAME}.exe.bin"
fi
if [ ! -f "${BINARY_FILEPATH}" ]; then
    BINARY_FILEPATH="${BINARY_FOLDER}/${BINARY_FILENAME}.bin"
fi
if [ ! -f "${BINARY_FILEPATH}" ]; then
    echo ""
    echo "Binary file '${BINARY_FILEPATH}(.exe)(.bin)' can not be found in '${BINARY_FOLDER}' folder"
    exit 1
fi

BOA_CMD=("${BOA_BIN}" "-b" "${BINARY_FILEPATH}" "-c" "${CONFIG_FILEPATH}" "$@")

# Check if config file exists
if [ ! -f "${CONFIG_FILEPATH}" ]; then
    # echo -e "\nConfig file '${CONFIG_FILEPATH}' can not be found in '${BINARY_FOLDER}' folder"
    BOA_CMD=("${BOA_BIN}" "-b" "${BINARY_FILEPATH}" "$@")
fi



LOGFILE_FILEPATH=${BINARY_FOLDER}/${BINARY_FILENAME}_boa.log
# Backup old log file
if [ -f "${LOGFILE_FILEPATH}" ]; then
    mv "${LOGFILE_FILEPATH}" "${LOGFILE_FILEPATH}.old"
fi


# Print BOA command on stdout
echo -e "\nBOA command: ${BOA_CMD[@]}\n"

# Add date in log
echo -e "$(date)\n" > ${LOGFILE_FILEPATH}

# Add BOA command in log
echo -e "####################################" >> ${LOGFILE_FILEPATH}
echo -e "#            BOA command           #" >> ${LOGFILE_FILEPATH}
echo -e "####################################\n" >> ${LOGFILE_FILEPATH}
echo -e "${BOA_CMD[@]}\n" >> ${LOGFILE_FILEPATH}

if [ -f "${CONFIG_FILEPATH}" ]; then
    # Add config.ini contents in log
    echo -e "####################################" >> ${LOGFILE_FILEPATH}
    echo -e "#          config.ini file         #" >> ${LOGFILE_FILEPATH}
    echo -e "####################################\n" >> ${LOGFILE_FILEPATH}
    cat ${CONFIG_FILEPATH} >> ${LOGFILE_FILEPATH}
    echo -e "\n" >> ${LOGFILE_FILEPATH}
fi

# Run BOA and write BOA stdout in log
echo -e "####################################" >> ${LOGFILE_FILEPATH}
echo -e "#             BOA stdout           #" >> ${LOGFILE_FILEPATH}
echo -e "####################################\n" >> ${LOGFILE_FILEPATH}

# Print or not print BOA stdout in the current shell 
if [ "$PRINT_STDOUT" = "no" ]; then
    echo -e "\nPlease wait until BOA finished...\n"
    "${BOA_CMD[@]}" >> ${LOGFILE_FILEPATH}
    BOA_EXIT_CODE=${PIPESTATUS[0]}
    # Print only the result
	cat ${LOGFILE_FILEPATH} | sed -ne '/Results of/,$ p'
else
    "${BOA_CMD[@]}" | tee -a ${LOGFILE_FILEPATH}
    BOA_EXIT_CODE=${PIPESTATUS[0]}
fi


# Print log filepath
echo -e "\nBOA log file: ${LOGFILE_FILEPATH}"

exit $BOA_EXIT_CODE