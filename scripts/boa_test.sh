#!/usr/bin/env bash

# Check if we are in the correct directory
if [ ! -d "./bin" ] || [ ! -d "./scripts" ]; then
    echo -e "\nFolder 'bin' or 'scripts' can not be found in your current directory"
    exit 1
fi


# Check for BOA and BinSec binaries filepath
BOA_BIN="./bin/boa_v2"
if [ ! -f "${BOA_BIN}" ]; then
    echo -e "\nBOA executable can not be found at './bin/boa_v2' location"
    exit 1
fi

BINSEC_BIN="./bin/binsec"
if [ ! -f "${BINSEC_BIN}" ]; then
    echo -e "\nBinSec executable can not be found at './bin/binsec' location"
    exit 1
fi

# Add bin folder in PATH so that BOA will find BinSec binary
BASEDIR=$(dirname "$0")
BASEDIR=$(pwd)
export PATH=${BASEDIR}/bin:$PATH

# Test mode
# BOA_CMD=("./bin/boa_v2" "-b" "./bin/boa_v2" "--disas-mode" "static" "--test")
# SECONDS=0
# ${BOA_CMD[@]}
# if [ $? -ne 0 ]; then
#     echo -e "[FAILED] ${BOA_CMD[@]}"
#     exit 1
# fi
# duration=$SECONDS
# echo -e "[SUCCESS] ($(($duration / 60)) min $(($duration % 60)) sec) ${BOA_CMD[@]}"



run () {
    BINARY_FILENAME=$(basename $BINARY_FOLDER)
    CONFIG_FILEPATH=${BINARY_FOLDER}/${CONFIG}
    BINARY_FILEPATH=${BINARY_FOLDER}/${BINARY_FILENAME}

    # Check if binary file exists
    if [ ! -f "${BINARY_FILEPATH}" ]; then
        BINARY_FILEPATH="${BINARY_FOLDER}/${BINARY_FILENAME}.exe"
    fi
    if [ ! -f "${BINARY_FILEPATH}" ]; then
        BINARY_FILEPATH="${BINARY_FOLDER}/${BINARY_FILENAME}.exe.bin"
    fi
    if [ ! -f "${BINARY_FILEPATH}" ]; then
        BINARY_FILEPATH="${BINARY_FOLDER}/${BINARY_FILENAME}.bin"
    fi
    if [ ! -f "${BINARY_FILEPATH}" ]; then
        echo ""
        echo "Binary file '${BINARY_FILEPATH}(.exe)(.bin)' can not be found in '${BINARY_FOLDER}' folder"
        exit 1
    fi

    # Start BOA with given config file
    BOA_CMD=("./bin/boa_v2" "-b" "${BINARY_FILEPATH}" "-c" "${CONFIG_FILEPATH}" "--loglevel-global=6")
    echo -e "➡️  ${BOA_CMD[@]}"
    SECONDS=0
    ${BOA_CMD[@]}
    if [ $? -ne 0 ]; then
        echo -e "\`-->❌"
        exit 1
    fi
    duration=$SECONDS

    # Check results
    for file in "${FILES_TO_COMPARE[@]}"
    do
        current_file=${BINARY_FILEPATH}_${file}.txt
        correct_file=${BINARY_FILEPATH}_${file}_expected.txt
        cmp --silent $current_file $correct_file
        if [ $? -ne 0 ]; then
            echo -e "\`-->❌ (file ${current_file} is wrong)"
            git --no-pager diff --no-index ${correct_file} ${current_file}
            exit 1
        fi
    done
    echo -e "\`-->✅ in $(($duration / 60)) min $(($duration % 60)) sec"
    
}


BINARY_FOLDER="./examples/decryptmsg"
CONFIG="config_test.ini"
FILES_TO_COMPARE=("BOA_edges")
run

BINARY_FOLDER="./examples/selfmodif/1"
CONFIG="config_test_static.ini"
FILES_TO_COMPARE=("STATIC_edges")
run
CONFIG="config_test_boa.ini"
FILES_TO_COMPARE=("BOA_edges")
run

BINARY_FOLDER="./examples/selfmodif/2"
CONFIG="config_test_static.ini"
FILES_TO_COMPARE=("STATIC_edges")
run
CONFIG="config_test_boa.ini"
FILES_TO_COMPARE=("BOA_edges")
run

BINARY_FOLDER="./examples/selfmodif/3"
CONFIG="config_test_static.ini"
FILES_TO_COMPARE=("STATIC_edges")
run
CONFIG="config_test_boa.ini"
FILES_TO_COMPARE=("BOA_edges")
run

BINARY_FOLDER="./examples/selfmodif/4"
CONFIG="config_test_static.ini"
FILES_TO_COMPARE=("STATIC_edges")
run
CONFIG="config_test_boa.ini"
FILES_TO_COMPARE=("BOA_edges")
run

BINARY_FOLDER="./examples/selfmodif/5"
CONFIG="config_test_static.ini"
FILES_TO_COMPARE=("STATIC_edges")
run
CONFIG="config_test_boa.ini"
FILES_TO_COMPARE=("BOA_edges")
run

BINARY_FOLDER="./examples/selfmodif/6"
CONFIG="config_test_static.ini"
FILES_TO_COMPARE=("STATIC_edges")
run
CONFIG="config_test_boa.ini"
FILES_TO_COMPARE=("BOA_edges")
run

BINARY_FOLDER="./examples/illegal_instr/1"
CONFIG="config_test_static.ini"
FILES_TO_COMPARE=("STATIC_edges")
run
CONFIG="config_test_boa.ini"
FILES_TO_COMPARE=("BOA_edges")
run

BINARY_FOLDER="./examples/illegal_instr/2"
CONFIG="config_test_static.ini"
FILES_TO_COMPARE=("STATIC_edges")
run
CONFIG="config_test_boa.ini"
FILES_TO_COMPARE=("BOA_edges")
run

BINARY_FOLDER="./examples/hostname/original"
CONFIG="config_test.ini"
FILES_TO_COMPARE=("BOA_edges")
run

BINARY_FOLDER="./examples/hostname/ep_protector_0_3"
CONFIG="config_test.ini"
FILES_TO_COMPARE=("BOA_edges")
run


# BINARY_FOLDER="./examples/wcet/bs_O0"
# CONFIG="config_test.ini"
# FILES_TO_COMPARE=("BOA_edges")
# run

























