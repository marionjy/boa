; Petit programme pour tester la divisions
; Celui-ci ne doit pas lever d'exception

; To assemble (RAW binary) --> nasm -f bin xxxx.asm


; DIV r/m32    Unsigned divide EDX:EAX by r/m32

bits 32
global main

        section   .text

main:

        mov eax, 0x6
        xor edx, edx
        mov ecx, 0x2
        div ecx
        ret



