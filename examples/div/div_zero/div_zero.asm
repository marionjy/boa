; Petit programme pour tester la divisions
; Celui-ci doit lever une exception de type division par zéro

; To assemble (RAW binary) --> nasm -f bin xxxx.asm


; DIV r/m32    Unsigned divide EDX:EAX by r/m32

bits 32
global main

        section   .text

main:

        mov eax, 0x6
        xor edx, edx
        mov ecx, 0x0
        div ecx
        ret



