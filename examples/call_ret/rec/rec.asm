bits 32
global main



        section   .text




main:
   mov   ebx, 0x5             ;for calculating factorial 5
   call  proc_fact
   mov ebx, 0x90000000
   add eax, ebx ; eax <-- eax + ebx
   jmp eax
	
proc_fact:
   cmp   ebx, 1
   jg    do_calculation
   mov   eax, 1
   ret
	
do_calculation:
   dec   ebx
   call  proc_fact
   inc   ebx
   mul   ebx        ;eax = eax * ebx
   ret


; To compile: nasm -f elf32 xxxxx.asm && gcc -m32 xxxxx.o -o xxxxx


