bits 32
global main



        section   .text

main:
        mov eax, 0x0
again   inc eax
        cmp eax, 0x4
        jle again
        mov ebx, 0x99999990
        add ebx, eax
	    jmp ebx


; To compile: nasm -f elf32 xxxxx.asm && gcc -m32 xxxxx.o -o xxxxx


