bits 32
global main



        section   .text

main:
        mov eax, 0x0
        mov ebx,  0x99999990

again   inc eax
		dec ebx
        cmp eax, 0x5
        jle again

	    jmp ebx


; To compile: nasm -f elf32 xxxxx.asm && gcc -m32 xxxxx.o -o xxxxx


