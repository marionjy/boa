bits 32
global main



        section   .text

main:

        xor eax, eax
        inc eax
        jmp B

B       inc ebx
        cmp ebx, 0x3
        jz F

C       inc eax
        jmp D

D       inc edx
        cmp edx, 0x6
        jz B

E       ret

F       inc eax
        inc eax
        cmp ecx, 0x4
        jz D

G       ret



; To compile: nasm -f elf32 xxxxx.asm && gcc -m32 xxxxx.o -o xxxxx


