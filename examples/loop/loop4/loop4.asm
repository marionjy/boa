bits 32
global main



        section   .text

main:

A       mov eax,  0x0
        jmp B

B       inc eax
        jmp C

C       cmp eax, 0x5
		jnz B

D		ret



; To compile: nasm -f elf32 xxxxx.asm && gcc -m32 xxxxx.o -o xxxxx


