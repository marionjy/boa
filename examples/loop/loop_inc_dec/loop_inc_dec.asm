bits 32
global main



        section   .text

main:

A       mov eax, 0x0
		jmp B

B 		cmp ecx, 0x0
		jz D

C 		jmp 0x99999999

D 		cmp eax, 0x0
		jz F

E 		dec eax
		jmp B

F 		inc eax
		jmp B


; To compile: nasm -f elf32 xxxxx.asm && gcc -m32 xxxxx.o -o xxxxx


