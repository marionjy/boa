#include <time.h>
#include <pthread.h>
#include <stdio.h>

#define INF 1000000

struct pair
{
    int a;
    int b;
} pairs[4000];

int p[4000];

int mx[4000][4000];

int main()
{
    int n, m;

    int i, j;

    scanf("%d %d", &n, &m);

    for(i = 0; i < n; i++)
    {
        for(j = 0; j < n; j++)
        {
            mx[i][j] = 0;
        }
    }

    for(i = 0; i < m; i++)
    {
        int fr, to;

        scanf("%d %d", &fr, &to);
        fr--;
        to--;
        pairs[i].a = fr;
        pairs[i].b = to;

        mx[fr][to] = 1;
        mx[to][fr] = 1;

        p[fr]++;
        p[to]++;   
    }

    int ans = INF;

    for(i = 0; i < m; i++)
    {
        int f, s;
        f = pairs[i].a;
        s = pairs[i].b;

        for(j = 0; j < n; j++)
        {
            if(mx[f][j] && mx[s][j])
            {
                if(p[f] + p[s] + p[j] < ans)
                {
                    ans = p[f] + p[s] + p[j];
                }
            }
        }
    }

    printf("%d", (ans == INF) ? -1 : (ans - 6));

    return 0;
}
