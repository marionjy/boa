; Decryption Program                   (encrypt.asm)

global _start
	
	section   .text
_start:
	pushad 
	mov ecx,BUFMAX ; loop counter 
	mov esi,0 ; index 0 in buffer 
	mov edi,0 ; index 0 for key

L1:
	mov al,[KEY+edi]	      ;
	xor [buffer+esi],al 	; translate a byte 
	inc esi 		; point to next byte 
	inc edi 		; point to next byte
	cmp edi, 2		;if edi index gets to six, reset to zero
	jne cont		; if not equal to 2, skip restting code
	mov edi, 0		; reset edi/ key to beginning

cont:
	dec ecx
	jnz L1

	popad
	jmp [buffer]

	;; -----------------
	section .data
BUFMAX  equ      4
KEY:	db 	'z^' 	; {0x7a, 0x5e}
buffer	db	'abcd' 	; {0x61, 0x62, 0x63, 0x64}


; buffer[0] <-- buffer[0] xor KEY[0] 	(0x1b <-- 0x61 xor 0x7a)
; buffer[1] <-- buffer[1] xor KEY[1] 	(0x3c <-- 0x62 xor 0x5e)
; buffer[2] <-- buffer[2] xor KEY[0] 	(0x19 <-- 0x63 xor 0x7a)
; buffer[3] <-- buffer[3] xor KEY[1] 	(0x3a<-- 0x64 xor 0x5e)
; JMP [buffer] 							(JMP to 0x3a193c1b)