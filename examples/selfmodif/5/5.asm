; Petit programme pour tester la détection d'auto-modification dans BOA

; On modifie l'adresse d'une instruction au milieu d'un BB qui est en cours d'exécution
; Au moment de l'exécution de la ligne C, on va exécuter un NOP et pas un RET

; To assemble (RAW binary) --> nasm -f bin xxxx.asm


bits 32
global main

        section   .text

main:

A       mov eax, 0x80000000
        xor ebx, ebx
        jmp B

B       inc ebx
        mov byte[C], 0x90
        dec ebx
C       ret

