; Petit programme pour tester la détection d'auto-modification dans BOA

; On modifie l'adresse d'une instruction au milieu d'un BB encore jamais exécuté
; Au moment de l'exécution de la ligne B, on ajoutera en réalité 0x50 à EAX, et pas 0x10. Donc on va sauter en 0x80000050 et pas en 0x80000010

; To assemble (RAW binary) --> nasm -f bin xxxx.asm


bits 32
global main

        section   .text

main:

A       mov eax,  0x80000000
        lea ebx, [C + 0x2]
        mov byte[ebx], 0x50
        jmp B


B       xor ebx, ebx
C       add eax, 0x10
        jmp eax




