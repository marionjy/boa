; Petit programme pour tester la détection d'auto-modification dans BOA

; On modifie la première adresse de la première instruction d'un BB jamais exécuté
; A la ligne C, au moment de l'exécution, c'est un NOP qui sera exécuté, et pas un RET (falsification à la ligne B)

; To assemble (RAW binary) --> nasm -f bin xxxx.asm


bits 32
global main

        section   .text

main:

A       mov eax,  0x88888888
		mov ecx, 0x99999999
        jmp B


B       mov byte[C], 0x90
        jmp C



C		RET



