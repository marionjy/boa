; Petit programme pour tester la détection d'auto-modification dans BOA

; On modifie la première adresse de la première instruction d'un BB déjà exécuté
; Au moment de la deuxième exécution de la ligne C, le RET est maintenant un NOP (falsification à la ligne B)

; To assemble (RAW binary) --> nasm -f bin xxxx.asm


bits 32
global main

        section   .text

main:

A       mov eax,  0x88888888
		mov ecx, 0x99999999
        push B
        jmp C


B       mov byte[C], 0x90
        jmp C



C		RET



