#!/usr/bin/env bash

export TIGRESS_HOME=../tigress-2.2
export PATH=$PATH:../tigress-2.2

#   --Functions=check_char_0,check_char_1,check_char_2,check_char_3,check_char_4,check_char_5,check_char_6,check_char_7,check_char_8,check_char_9,check_char_10 \


../tigress-2.2/tigress  \
   --Transform=Flatten \
   --Functions=check_char_0 \
   --out=crackme_flatten.c \
   ../crackme.c

gcc -m32 crackme_flatten.c -o crackme_flatten